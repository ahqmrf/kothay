package apps.ahqmrf.kothay;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.binjar.prefsdroid.Preference;

import apps.ahqmrf.kothay.auth.callbacks.LoginListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.auth.ui.LoginActivity;
import apps.ahqmrf.kothay.home.ui.HomeActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;

public class SplashActivity extends AuthActivity implements LoginListener {

    LoginResponse self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        self = Preference.getObject(PrefKeys.USER_INFO, LoginResponse.class);
    }

    @Override protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onViewCreated();
    }

    public void onViewCreated() {
        long splashDuration = AppUtils.isLoggedIn() ? Const.SPLASH_DURATION_SHORT : Const.SPLASH_DURATION;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(AppUtils.isLoggedIn()) {
                   checkLogin();
                } else {
                    openPage(LoginActivity.class);
                    finish();
                }
            }
        }, splashDuration);
    }

    private void checkLogin() {
        new AuthConnector(this).checkLoginStatus(self.getEmail(), self.getToken());
    }

    @Override public void onFailure(String message) {
        openPage(LoginActivity.class);
        AppUtils.clearUserCache();
        finish();
    }

    @Override public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override public void onLoginSuccess(LoginResponse response) {
        openPage(HomeActivity.class);
        finish();
    }

    @Override public void onActivationRequired(LoginResponse response) {

    }
}
