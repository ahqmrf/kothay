package apps.ahqmrf.kothay.account.request;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseRequest;

/**
 * Created by maruf on 10/17/2017.
 */

public class UpdateProfileRequest extends BaseRequest {
    @SerializedName("value") private String value;

    public void setValue(String value) {
        this.value = value;
    }
}
