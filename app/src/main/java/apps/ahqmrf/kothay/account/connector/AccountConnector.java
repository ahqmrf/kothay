package apps.ahqmrf.kothay.account.connector;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.account.callback.AccountUpdateListener;
import apps.ahqmrf.kothay.account.request.UpdateProfileRequest;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import apps.ahqmrf.kothay.account.service.AccountClient;
import apps.ahqmrf.kothay.account.service.AccountService;
import apps.ahqmrf.kothay.util.Const;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by maruf on 10/17/2017.
 */

public class AccountConnector {
    private ResponseListener listener;
    private AccountService   service;

    public AccountConnector(ResponseListener listener) {
        this.listener = listener;
        service = new AccountClient().createService();
    }

    public void uploadImage(File file) {
        if(listener != null) {
            listener.showLoader();
            Map<String, RequestBody> map         = new HashMap<>();
            RequestBody              requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
            Call<AccountUpdateResponse> call = service.uploadImage(map);
            call.enqueue(new ServiceCallback<AccountUpdateResponse>(listener) {
                @Override public void onResponse(Response<AccountUpdateResponse> response) {
                    AccountUpdateResponse res = response.body();
                    if(res != null) {
                        listener.onSuccess(res.getMessage());
                        ((AccountUpdateListener)listener).onImageUploaded(res);
                    }
                }
            });
        }
    }

    public void updateProfile(long userId, String value, final String type) {
        if(listener != null) {
            listener.showLoader();
            UpdateProfileRequest request = new UpdateProfileRequest();
            request.setUserId(userId);
            request.setValue(value);

            Call<AccountUpdateResponse> call;
            if(type.equals(Const.UPDATE_PROFILE_PICTURE)) call = service.updateProfilePicture(request);
            else if(type.equals(Const.UPDATE_BIO)) call = service.updateBio(request);
            else if(type.equals(Const.UPDATE_WORKPLACE)) call = service.updateWorkplace(request);
            else  call = service.updatePhone(request);

            call.enqueue(new ServiceCallback<AccountUpdateResponse>(listener) {
                @Override public void onResponse(Response<AccountUpdateResponse> response) {
                    AccountUpdateResponse res = response.body();
                    if(res != null) {
                        listener.onSuccess(res.getMessage());
                        AccountUpdateListener accountUpdateListener = (AccountUpdateListener) listener;
                        if(type.equals(Const.UPDATE_PROFILE_PICTURE)) accountUpdateListener.onProfilePictureUpdated(res);
                        else if(type.equals(Const.UPDATE_BIO)) accountUpdateListener.onBioUpdated(res);
                        else if(type.equals(Const.UPDATE_WORKPLACE)) accountUpdateListener.onWorkplaceUpdated(res);
                        else if(type.equals(Const.UPDATE_PHONE)) accountUpdateListener.onPhoneUpdated(res);
                    }
                }
            });
        }
    }
}
