package apps.ahqmrf.kothay.account.ui;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.home.ui.HomeActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;

/**
 * Created by bsse0 on 10/20/2017.
 */

public class UpdateAccountFragment extends DialogFragment {

    TextView updateTypeView;
    EditText inputView;
    TextView okBtn, cancelBtn;
    String updateType;
    long   id;

    public UpdateAccountFragment() {
    }

    public static UpdateAccountFragment getInstance(String updateType, String currentValue, long id) {
        UpdateAccountFragment frag = new UpdateAccountFragment();
        Bundle                args = new Bundle();
        args.putString(Const.UPDATE_TYPE, updateType);
        args.putString(Const.CURRENT_VALUE, currentValue);
        args.putLong(Const.ID, id);
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_update_account);

        inputView = (EditText) dialog.findViewById(R.id.input);
        updateTypeView = (TextView) dialog.findViewById(R.id.updateType);
        updateType = getArguments().getString(Const.UPDATE_TYPE);
        updateTypeView.setText(updateType);
        inputView.setText("");
        inputView.append(getArguments().getString(Const.CURRENT_VALUE));
        okBtn = (TextView) dialog.findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)getActivity()).hideSoftKeyboard();
                if (updateType.equals(Const.UPDATE_BIO))
                    ((AccountActivity) getActivity()).updateBio(inputView.getText().toString());
                else if (updateType.equals(Const.UPDATE_GENDER))
                    ((AccountActivity) getActivity()).updateGender(inputView.getText().toString());
                else if (updateType.equals(Const.UPDATE_PHONE))
                    ((AccountActivity) getActivity()).updatePhone(inputView.getText().toString());
                else if (updateType.equals(Const.UPDATE_WORKPLACE))
                    ((AccountActivity) getActivity()).updateWorkplace(inputView.getText().toString());
                else if (updateType.equals(Const.EDIT_STATUS)) {
                    if(AppUtils.isEmpty(inputView)) {
                        AppUtils.toast(R.string.update_hint);
                        return;
                    }
                    long id = getArguments().getLong(Const.ID);
                    ((HomeActivity) getActivity()).editStatus(id, inputView.getText().toString());
                }
                dismiss();
            }
        });

        cancelBtn = (TextView) dialog.findViewById(R.id.btn_cancel);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(AppUtils.getColor(android.R.color.transparent)));
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }

        return dialog;
    }
}
