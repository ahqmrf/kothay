package apps.ahqmrf.kothay.account.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by maruf on 10/17/2017.
 */

public class AccountClient extends Client<AccountService> {
    @Override public AccountService createService() {
        return getRetrofit().create(AccountService.class);
    }
}
