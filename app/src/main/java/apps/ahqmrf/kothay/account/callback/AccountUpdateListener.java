package apps.ahqmrf.kothay.account.callback;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;

/**
 * Created by maruf on 10/17/2017.
 */

public interface AccountUpdateListener extends ResponseListener{
    void onImageUploaded(AccountUpdateResponse response);

    void onProfilePictureUpdated(AccountUpdateResponse response);

    void onBioUpdated(AccountUpdateResponse response);

    void onWorkplaceUpdated(AccountUpdateResponse response);

    void onPhoneUpdated(AccountUpdateResponse response);
}
