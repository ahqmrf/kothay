package apps.ahqmrf.kothay.account.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.account.callback.AccountUpdateListener;
import apps.ahqmrf.kothay.account.connector.AccountConnector;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import apps.ahqmrf.kothay.friends.ui.FriendsActivity;
import apps.ahqmrf.kothay.friends.ui.ViewPostsActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.FullScreenImageFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountActivity extends BaseActivity implements AccountUpdateListener {

    @BindView(R.id.profile_picture) ImageView profilePictureView;
    @BindView(R.id.email)           TextView  emailView;
    @BindView(R.id.name)            TextView  nameView;
    @BindView(R.id.username)        TextView  usernameView;
    @BindView(R.id.phone)           TextView  phoneView;
    @BindView(R.id.bio)             TextView  bioView;
    @BindView(R.id.gender)          TextView  genderView;
    @BindView(R.id.workplace)       TextView  workplaceView;

    private AccountConnector connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        connector = new AccountConnector(this);
    }

    @Override
    public void onViewCreated() {
        setLabel(R.string.title_account);
        showBackArrow();
        int placeholder = R.drawable.man_placeholder;
        if (self.getGender().equals(Const.FEMALE)) placeholder = R.drawable.woman_placeholder;
        ImageLoader.getInstance().displayImage(self.getImageUrl(), profilePictureView, AppUtils.getImageConfig(placeholder));

        nameView.setText(self.getName());
        emailView.setText(self.getEmail());
        usernameView.setText(self.getUsername());
        if (TextUtils.isEmpty(self.getPhone())) phoneView.setText("No phone number set");
        else phoneView.setText(self.getPhone());
        String bio = self.getBio();
        if (TextUtils.isEmpty(bio)) bio = Const.DEFAULT_BIO;
        bioView.setText(bio);
        genderView.setText(self.getGender());
        workplaceView.setText(self.getWorkplace());
    }

    private void openImageGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (getPackageManager().resolveActivity(intent, 0) != null) {
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), Const.REQUEST_BROWSE_GALLERY);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openImageGallery();

                } else {
                    AppUtils.toast(R.string.error_permission);
                }
                break;
            }
        }
    }

    @OnClick(R.id.btnChangeProfilePicture)
    void checkReadExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Const.READ_EXTERNAL_STORAGE);
                }
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Const.READ_EXTERNAL_STORAGE);
            }
        } else {
            openImageGallery();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.REQUEST_BROWSE_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (null != uri) {
                File file = new File(AppUtils.getFileUrl(uri));
                if (connector != null) {
                    connector.uploadImage(file);
                }
            }
        }
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onImageUploaded(AccountUpdateResponse response) {
        connector.updateProfile(self.getId(), Const.BASE_URL + response.getValue(), Const.UPDATE_PROFILE_PICTURE);
    }

    @Override
    public void onProfilePictureUpdated(AccountUpdateResponse response) {
        self.setImageUrl(response.getValue());
        AppUtils.clearUserCache();
        AppUtils.cacheUserInfo(self);
        int placeholder = self.getGender().equals(Const.FEMALE)? R.drawable.woman_placeholder : R.drawable.man_placeholder;
        ImageLoader.getInstance().displayImage(self.getImageUrl(), profilePictureView, AppUtils.getImageConfig(placeholder));
    }

    @Override
    public void onBioUpdated(AccountUpdateResponse response) {
        AppUtils.clearUserCache();
        self.setBio(response.getValue());
        AppUtils.cacheUserInfo(self);
        bioView.setText(self.getBio());
    }

    @Override
    public void onWorkplaceUpdated(AccountUpdateResponse response) {
        AppUtils.clearUserCache();
        self.setWorkplace(response.getValue());
        AppUtils.cacheUserInfo(self);
        workplaceView.setText(self.getWorkplace());
    }

    @Override
    public void onPhoneUpdated(AccountUpdateResponse response) {
        AppUtils.clearUserCache();
        self.setPhone(response.getValue());
        AppUtils.cacheUserInfo(self);
        phoneView.setText(self.getPhone());
    }

    @OnClick(R.id.btn_bio_edit)
    void onBioEditClick() {
        UpdateAccountFragment frag = UpdateAccountFragment.getInstance(Const.UPDATE_BIO, self.getBio(), 0);
        frag.show(getSupportFragmentManager(), Const.UPDATE_BIO);
    }

    @OnClick(R.id.btn_phone_edit)
    void onPhoneEditClick() {
        UpdateAccountFragment frag = UpdateAccountFragment.getInstance(Const.UPDATE_PHONE, self.getPhone(), 0);
        frag.show(getSupportFragmentManager(), Const.UPDATE_PHONE);
    }

    @OnClick(R.id.btn_workplace_edit)
    void onWorkplaceEditClick() {
        UpdateAccountFragment frag = UpdateAccountFragment.getInstance(Const.UPDATE_WORKPLACE, self.getWorkplace(), 0);
        frag.show(getSupportFragmentManager(), Const.UPDATE_WORKPLACE);
    }

    void updateBio(String bio) {
        connector.updateProfile(self.getId(), bio, Const.UPDATE_BIO);
    }

    void updateGender(String gender) {
        connector.updateProfile(self.getId(), gender, Const.UPDATE_GENDER);
    }

    void updatePhone(String phone) {
        connector.updateProfile(self.getId(), phone, Const.UPDATE_PHONE);
    }

    void updateWorkplace(String workplace) {
        connector.updateProfile(self.getId(), workplace, Const.UPDATE_WORKPLACE);
    }

    @OnClick(R.id.profile_picture)
    void onProfilePictureClick() {
        if (!TextUtils.isEmpty(self.getImageUrl())) {
            FullScreenImageFragment frag = FullScreenImageFragment.getInstance(self.getImageUrl());
            frag.show(getSupportFragmentManager(), "Full screen image");
        }
    }

    @OnClick(R.id.friendListLayout)
    void onMyFriendsClick() {
        openPage(FriendsActivity.class);
    }

    @OnClick(R.id.postsLayout)
    void onMyPostsClick() {
        openPage(ViewPostsActivity.class);
    }
}
