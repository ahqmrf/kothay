package apps.ahqmrf.kothay.account.service;

import java.util.Map;

import apps.ahqmrf.kothay.account.request.UpdateProfileRequest;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by maruf on 10/17/2017.
 */

public interface AccountService {
    @Multipart
    @POST("upload_profile_picture.php")
    Call<AccountUpdateResponse> uploadImage(@PartMap Map<String, RequestBody> map);

    @POST("set_profile_picture.php")
    Call<AccountUpdateResponse> updateProfilePicture(@Body UpdateProfileRequest request);

    @POST("update_bio.php")
    Call<AccountUpdateResponse> updateBio(@Body UpdateProfileRequest request);

    @POST("update_gender.php")
    Call<AccountUpdateResponse> updateGender(@Body UpdateProfileRequest request);

    @POST("update_workplace.php")
    Call<AccountUpdateResponse> updateWorkplace(@Body UpdateProfileRequest request);

    @POST("update_phone.php")
    Call<AccountUpdateResponse> updatePhone(@Body UpdateProfileRequest request);
}
