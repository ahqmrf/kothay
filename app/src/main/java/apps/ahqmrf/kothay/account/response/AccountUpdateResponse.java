package apps.ahqmrf.kothay.account.response;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.util.Const;

/**
 * Created by maruf on 10/17/2017.
 */

public class AccountUpdateResponse extends BaseResponse {
    @SerializedName("value") private String value;

    public String getValue() {
        return value;
    }
}
