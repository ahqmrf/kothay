package apps.ahqmrf.kothay.alarm.callback;

import apps.ahqmrf.kothay.alarm.response.Alarm;

/**
 * Created by bsse0 on 11/20/2017.
 */

public interface OnUserNearCallback {
    void onUserInside(Alarm alarm);

    void onUserOutSide(Alarm alarm);
}
