package apps.ahqmrf.kothay.alarm.callback;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.alarm.response.SetAlarmResponse;

/**
 * Created by bsse0 on 11/12/2017.
 */

public interface AlarmServiceListener extends ResponseListener {
    void onAlarmSet(SetAlarmResponse response);

    void onAlarmsFetched(List<Alarm> alarms, int count);

    void onAlarmUpdated();
}
