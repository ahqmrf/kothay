package apps.ahqmrf.kothay.alarm.connector;


import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.alarm.callback.AlarmServiceListener;
import apps.ahqmrf.kothay.alarm.request.AlarmRequest;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.alarm.response.GetAlarmsResponse;
import apps.ahqmrf.kothay.alarm.response.SetAlarmResponse;
import apps.ahqmrf.kothay.alarm.service.AlarmClient;
import apps.ahqmrf.kothay.alarm.service.AlarmService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class AlarmConnector {
    private ResponseListener listener;
    private AlarmService     service;

    public AlarmConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = new AlarmClient().createService();
    }

    public void setAlarm(long selfId, long userId, String area, String place) {
        if (listener != null) {
            listener.showLoader();
            AlarmRequest request = new AlarmRequest();
            request.setSetBy(selfId);
            request.setSetAfter(userId);
            request.setArea(area);
            request.setPlace(place);

            Call<SetAlarmResponse> call = service.setAlarm(request);
            call.enqueue(new ServiceCallback<SetAlarmResponse>(listener) {
                @Override public void onResponse(Response<SetAlarmResponse> response) {
                    SetAlarmResponse result = response.body();
                    if (result != null) {
                        ((AlarmServiceListener) listener).onAlarmSet(result);
                        listener.onSuccess(result.getMessage());
                    }
                }
            });
        }
    }

    public void updateAlarm(long alarmId, boolean enabled, String area, String place) {
        if (listener != null) {
            listener.showLoader();
            AlarmRequest request = new AlarmRequest();
            request.setAlarmId(alarmId);
            request.setArea(area);
            request.setEnabled(enabled);
            request.setPlace(place);
            Call<BaseResponse> call = service.updateAlarm(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override public void onResponse(Response<BaseResponse> response) {
                    BaseResponse result = response.body();
                    if (result != null) {
                        ((AlarmServiceListener) listener).onAlarmUpdated();
                        listener.onSuccess(result.getMessage());
                    }
                }
            });
        }
    }

    public void getAlarms(long userId) {
        if (listener != null) {
            BaseRequest request = new BaseRequest();
            request.setUserId(userId);

            Call<GetAlarmsResponse> call = service.getAlarms(request);
            call.enqueue(new ServiceCallback<GetAlarmsResponse>(listener) {
                @Override public void onResponse(Response<GetAlarmsResponse> response) {
                    GetAlarmsResponse body = response.body();
                    if (body != null) {
                        ((AlarmServiceListener) listener).onAlarmsFetched(body.getAlarms(), body.getActiveAlarmCount());
                    }
                }
            });
        }
    }

    public void removeAlarm(long alarmId) {
        if (listener != null) {
            listener.showLoader();
            final AlarmRequest request = new AlarmRequest();
            request.setAlarmId(alarmId);

            Call<BaseResponse> call = service.removeAlarm(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                    }
                }
            });
        }
    }
}
