package apps.ahqmrf.kothay.alarm.response;

import android.os.Parcel;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 11/17/2017.
 */

public class Alarm extends UserInfoResponse {
    @SerializedName("alarmId") private      long    alarmId;
    @SerializedName("time") private         String  time;
    @SerializedName("area") private         String  area;
    @SerializedName("place") private        String  place;
    @SerializedName("enabled") private      boolean enabled;

    public Alarm() {
        super();
    }

    protected Alarm(Parcel in) {
        super(in);
        alarmId = in.readLong();
        time = in.readString();
        area = in.readString();
        place = in.readString();
        enabled = in.readByte() != 0;
    }

    public static final Creator<Alarm> CREATOR = new Creator<Alarm>() {
        @Override
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        @Override
        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(alarmId);
        parcel.writeString(time);
        parcel.writeString(area);
        parcel.writeString(place);
        parcel.writeByte((byte) (enabled ? 1 : 0));
    }

    public long getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    public Date getTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getAreaStr() {
        return area;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public ArrayList<LatLng> getArea() {
        ArrayList<LatLng> points = new ArrayList<>();
        if (area != null) {
            String arr[] = area.split(" ");
            for (String str : arr) {
                String[] latLngStr = str.split(",");
                LatLng   latLng    = new LatLng(Double.parseDouble(latLngStr[0]), Double.parseDouble(latLngStr[1]));
                points.add(latLng);
            }
        }
        return points;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
