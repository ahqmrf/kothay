package apps.ahqmrf.kothay.alarm.callback;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.alarm.response.Alarm;

/**
 * Created by bsse0 on 11/17/2017.
 */

public interface OnAlarmItemClickCallback extends OnItemClickCallback {

    void onSwitchClick(Alarm alarm, int position);
}
