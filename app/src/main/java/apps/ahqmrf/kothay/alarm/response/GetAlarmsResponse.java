package apps.ahqmrf.kothay.alarm.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by bsse0 on 11/17/2017.
 */

public class GetAlarmsResponse extends BaseResponse {
    @SerializedName("alarms") private           List<Alarm> alarms;
    @SerializedName("activeAlarmCount") private int         activeAlarmCount;

    public int getActiveAlarmCount() {
        return activeAlarmCount;
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }
}
