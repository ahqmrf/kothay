package apps.ahqmrf.kothay.alarm.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.rey.material.widget.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.alarm.callback.AlarmServiceListener;
import apps.ahqmrf.kothay.alarm.callback.OnAlarmItemClickCallback;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.friends.ui.FriendsActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAlarmsActivity extends BaseActivity implements AlarmServiceListener, OnAlarmItemClickCallback {

    AlarmListAdapter adapter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout   swipeRefreshLayout;
    @BindView(R.id.alarmsListView)     RecyclerView         alarmsView;
    @BindView(R.id.btn_add_alarm)      FloatingActionButton fab;
    private                            Animation            animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_alarms);
        ButterKnife.bind(this);

        animation = AnimationUtils.loadAnimation(this, R.anim.simple_grow);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        if (alarmView != null) {
            alarmView.setColorFilter(AppUtils.getColor(R.color.bottomIconFocused));
            alarmView.setBackgroundColor(AppUtils.getColor(R.color.colorPrimaryDark));
        }
        setLabel(R.string.title_my_alarms);
        alarms = new ArrayList<>();
        adapter = new AlarmListAdapter(alarms, this);
        alarmsView.setLayoutManager(new LinearLayoutManager(this));
        alarmsView.setAdapter(adapter);

        alarmsView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                alarmConnector.getAlarms(self.getId());
            }
        });

        fab.startAnimation(animation);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showLoader();
    }

    @Override
    public void onAlarmsFetched(List<Alarm> alarms, int count) {
        super.onAlarmsFetched(alarms, count);
        hideLoader();
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Object item) {
        Alarm  alarm  = (Alarm) item;
        Intent intent = new Intent(this, ViewAlarmActivity.class);
        intent.putExtra(Const.ALARM, alarm);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            showLoader();
            alarmConnector.getAlarms(self.getId());
        }
    }

    @Override
    public void onSwitchClick(Alarm alarm, int position) {
        if (!alarm.isEnabled() && activeAlarmCount == 3) {
            AppUtils.toast(R.string.error_max_alarm_count);
            adapter.notifyItemChanged(position);
            return;
        }
        alarmConnector.updateAlarm(alarm.getAlarmId(), !alarm.isEnabled(), alarm.getAreaStr(), alarm.getPlace());
    }

    @Override
    public void onFailure(String message) {
        super.onFailure(message);
        swipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.btn_add_alarm)
    void onAddClick() {
        Intent intent = new Intent(this, FriendsActivity.class);
        intent.putExtra(Const.ADD_ALARM, true);
        openPage(intent);
    }
}
