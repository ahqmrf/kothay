package apps.ahqmrf.kothay.alarm.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.alarm.callback.AlarmServiceListener;
import apps.ahqmrf.kothay.alarm.connector.AlarmConnector;
import apps.ahqmrf.kothay.alarm.response.SetAlarmResponse;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetAlarmActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, AlarmServiceListener {

    private GoogleMap           mGoogleMap;
    private ArrayList<LatLng>   points;
    private ArrayList<Circle>   circles;
    private ArrayList<Polyline> polylines;
    private boolean             isMapClickable;
    private UserInfoResponse    user;

    @BindView(R.id.label_freeze) TextView  freezeView;
    @BindView(R.id.ic_undo)      ImageView undoView;

    @BindView(R.id.place)        EditText  placeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);
        ButterKnife.bind(this);
    }

    @Override public void onViewCreated() {
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        showBackArrow();
        setLabel(user.getName());

        setUndoEnabled(false);
        setFreezeEnabled(false);
        isMapClickable = true;

        SupportMapFragment mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        points = new ArrayList<>();
        polylines = new ArrayList<>();
        circles = new ArrayList<>();
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    @Override public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        LatLng latLng = new LatLng(23.8103, 90.4125);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        mGoogleMap.setOnMapClickListener(this);
    }

    @Override public void onMapClick(LatLng latLng) {
        if (!isMapClickable) return;
        setUndoEnabled(true);
        points.add(latLng);
        int size = points.size();
        if (size > 1) {
            polylines.add(mGoogleMap.addPolyline(new PolylineOptions().add(points.get(size - 2), points.get(size - 1)).width(4).color(Color.RED)));
        }
        if (size > 2) {
            setFreezeEnabled(true);
        }
        circles.add(mGoogleMap.addCircle(new CircleOptions().center(latLng).radius(30).fillColor(Color.BLACK)));
    }

    @OnClick(R.id.label_freeze)
    void onFreezeClick() {
        if (points.size() < 3) {
            AppUtils.toast("Not enough points to make an area");
            return;
        }
        String place = placeView.getText().toString();
        if (TextUtils.isEmpty(place)) {
            AppUtils.toast(R.string.error_place);
            return;
        }
        isMapClickable = false;
        freezeView.setClickable(false);
        polylines.add(mGoogleMap.addPolyline(new PolylineOptions().add(points.get(points.size() - 1), points.get(0)).width(4).color(Color.RED)));
        onAreaSelected(place);
    }

    @OnClick(R.id.ic_undo)
    void onUndoClick() {
        isMapClickable = true;
        int size = polylines.size();
        int size2 = points.size();

        if (size == size2) {
            if (size > 0) {
                --size;
                Polyline polyline = polylines.get(size);
                polyline.remove();
                polylines.remove(size);
            }
        } else {
            if (size > 0) {
                --size;
                Polyline polyline = polylines.get(size);
                polyline.remove();
                polylines.remove(size);
            }
            if (size2 > 0) {
                --size2;
                Circle circle = circles.get(size2);
                circle.remove();
                circles.remove(size2);
                points.remove(size2);
            }
        }

        if (size2 == 0) {
            setUndoEnabled(false);
        }
        if (size2 < 3) {
            setFreezeEnabled(false);
        } else {
            setFreezeEnabled(true);
        }
    }

    @OnClick(R.id.ic_help)
    void onHelpClick() {
        AlarmHintFragment frag = AlarmHintFragment.getInstance();
        frag.show(getSupportFragmentManager(), "AlarmHint");
    }

    void onAreaSelected(String place) {
        StringBuilder builder = new StringBuilder();
        points.add(points.get(0));
        for (int i = 0, size = points.size(); i < size; i++) {
            LatLng latLng = points.get(i);
            if (i > 0) builder.append(" ");
            builder.append(latLng.latitude);
            builder.append(",");
            builder.append(latLng.longitude);
        }
        new AlarmConnector(this).setAlarm(self.getId(), user.getId(), builder.toString(), place);
    }

    @Override public void onAlarmSet(SetAlarmResponse response) {
        for (Polyline polyline : polylines) {
            polyline.remove();
        }
        polylines.clear();
        for (Circle circle : circles) {
            circle.remove();
        }
        circles.clear();
        points.clear();
        setUndoEnabled(false);
        setFreezeEnabled(false);
        isMapClickable = true;
    }

    @Override public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    private void setUndoEnabled(boolean enabled) {
        if (enabled) {
            undoView.setClickable(true);
        } else {
            undoView.setClickable(false);
        }
    }

    private void setFreezeEnabled(boolean enabled) {
        TypedValue outValue = new TypedValue();
        getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        freezeView.setBackgroundResource(outValue.resourceId);
        if (enabled) {
            freezeView.setClickable(true);
        } else {
            freezeView.setClickable(false);
        }
    }
}
