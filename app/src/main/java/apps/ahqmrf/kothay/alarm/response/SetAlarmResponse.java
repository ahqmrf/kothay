package apps.ahqmrf.kothay.alarm.response;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class SetAlarmResponse extends BaseResponse {
    @SerializedName("userInfo") private UserInfoResponse userInfo;
    @SerializedName("time") private     String           time;
    @SerializedName("area") private     String           area;

    public UserInfoResponse getUserInfo() {
        return userInfo;
    }

    public Date getTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<LatLng> getArea() {
        ArrayList<LatLng> points = new ArrayList<>();
        if (area != null) {
            String arr[] = area.split(" ");
            for (String str : arr) {
                String[] latLngStr = str.split(",");
                LatLng latLng = new LatLng(Double.parseDouble(latLngStr[0]), Double.parseDouble(latLngStr[1]));
                points.add(latLng);
            }
        }
        return points;
    }
}
