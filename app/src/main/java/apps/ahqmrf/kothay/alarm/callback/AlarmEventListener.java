package apps.ahqmrf.kothay.alarm.callback;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.PolyUtil;

import apps.ahqmrf.kothay.alarm.callback.OnUserNearCallback;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.util.Const;

/**
 * Created by bsse0 on 11/20/2017.
 */

public class AlarmEventListener implements ValueEventListener {
    private DatabaseReference  userRef;
    private OnUserNearCallback callback;
    private Alarm              alarm;


    public AlarmEventListener(Alarm alarm, OnUserNearCallback callback) {
        this.alarm = alarm;
        FirebaseDatabase rootRef = FirebaseDatabase.getInstance();
        DatabaseReference locationRef = rootRef.getReference().child(Const.LOCATION);
        this.userRef = locationRef.child(alarm.getUsername());
        this.callback = callback;
        userRef.addValueEventListener(this);
    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    public DatabaseReference getUserRef() {
        return userRef;
    }

    public void removeListener() {
        userRef.removeEventListener(this);
    }

    public void setUserRef(DatabaseReference userRef) {
        this.userRef = userRef;
    }

    @Override public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null && dataSnapshot.hasChild(Const.LATITUDE) && dataSnapshot.hasChild(Const.LONGITUDE)) {
            double lat = (double) dataSnapshot.child(Const.LATITUDE).getValue();
            double lng = (double) dataSnapshot.child(Const.LONGITUDE).getValue();
            String place = dataSnapshot.child(Const.PLACE).getValue(String.class);
            if (PolyUtil.containsLocation(new LatLng(lat, lng), alarm.getArea(), true)) {
                if (callback != null) {
                    callback.onUserInside(alarm);
                }
            } else {
                if (callback != null) {
                    callback.onUserOutSide(alarm);
                }
            }
        }
    }

    @Override public void onCancelled(DatabaseError databaseError) {

    }
}
