package apps.ahqmrf.kothay.alarm.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class AlarmClient extends Client<AlarmService> {
    @Override public AlarmService createService() {
        return getRetrofit().create(AlarmService.class);
    }
}
