package apps.ahqmrf.kothay.alarm.ui;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.util.AppUtils;

/**
 * Created by bsse0 on 11/10/2017.
 */

public class AlarmHintFragment extends DialogFragment {
    public AlarmHintFragment() {
    }

    public static AlarmHintFragment getInstance() {
        return new AlarmHintFragment();
    }

    @NonNull @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_alarm_hint);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(AppUtils.getColor(android.R.color.transparent)));
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }

        dialog.findViewById(R.id.btnGotIt).setOnClickListener(
                new View.OnClickListener() {
                    @Override public void onClick(View view) {
                        dismiss();
                    }
                }
        );

        return dialog;
    }
}
