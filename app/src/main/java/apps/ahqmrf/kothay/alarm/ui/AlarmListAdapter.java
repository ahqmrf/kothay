package apps.ahqmrf.kothay.alarm.ui;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.alarm.callback.OnAlarmItemClickCallback;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by bsse0 on 11/17/2017.
 */

public class AlarmListAdapter extends RecyclerView.Adapter {

    private ArrayList<Alarm>         items;
    private OnAlarmItemClickCallback callback;

    public AlarmListAdapter(ArrayList<Alarm> items, OnAlarmItemClickCallback callback) {
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alarm, parent, false);
        return new AlarmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AlarmViewHolder) holder).bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class AlarmViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)        TextView     nameView;
        @BindView(R.id.time)        TextView     timeView;
        @BindView(R.id.alarmSwitch) SwitchCompat alarmSwitch;
        @BindView(R.id.image)       ImageView    imageView;
        @BindView(R.id.place)       TextView     placeView;

        public AlarmViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Alarm alarm) {
            alarmSwitch.setChecked(alarm.isEnabled());
            nameView.setText(alarm.getName());
            timeView.setText(new AppUtils.TimeModel(alarm.getTime()).getTime(true));
            placeView.setText("Place: " + alarm.getPlace());
            int placeholder = R.drawable.man_placeholder;
            if (alarm.getGender().equals(Const.FEMALE)) placeholder = R.drawable.woman_placeholder;
            ImageLoader.getInstance().displayImage(
                    alarm.getImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(placeholder)
            );
        }

        @OnCheckedChanged(R.id.alarmSwitch)
        void onSwitchClick() {
            if (alarmSwitch.isPressed() && callback != null) {
                Alarm alarm = items.get(getAdapterPosition());
                callback.onSwitchClick(alarm, getAdapterPosition());
            }
        }

        @OnClick(R.id.alarmLayout)
        void onAlarmClick() {
            if (callback != null) {
                Alarm alarm = items.get(getAdapterPosition());
                callback.onItemClick(alarm);
            }
        }
    }
}
