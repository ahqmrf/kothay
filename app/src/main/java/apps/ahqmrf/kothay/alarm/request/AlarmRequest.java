package apps.ahqmrf.kothay.alarm.request;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.alarm.response.Alarm;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class AlarmRequest extends Alarm {

    @SerializedName("setBy") private    long setBy;
    @SerializedName("setAfter") private long setAfter;

    public long getSetBy() {
        return setBy;
    }

    public void setSetBy(long setBy) {
        this.setBy = setBy;
    }

    public long getSetAfter() {
        return setAfter;
    }

    public void setSetAfter(long setAfter) {
        this.setAfter = setAfter;
    }
}
