package apps.ahqmrf.kothay.alarm.service;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.alarm.request.AlarmRequest;
import apps.ahqmrf.kothay.alarm.response.GetAlarmsResponse;
import apps.ahqmrf.kothay.alarm.response.SetAlarmResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 11/12/2017.
 */

public interface AlarmService {
    @POST("set_alarm.php")
    Call<SetAlarmResponse> setAlarm(@Body AlarmRequest request);

    @POST("update_alarm.php")
    Call<BaseResponse> updateAlarm(@Body AlarmRequest request);

    @POST("get_alarms.php")
    Call<GetAlarmsResponse> getAlarms(@Body BaseRequest request);

    @POST("remove_alarm.php")
    Call<BaseResponse> removeAlarm(@Body AlarmRequest request);
}
