package apps.ahqmrf.kothay;

import android.support.annotation.NonNull;

import java.io.IOException;

import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by maruf on 8/19/2017.
 */

public abstract class ServiceCallback<Model> implements Callback<Model> {
    private ResponseListener listener;

    public ServiceCallback(ResponseListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResponse(@NonNull Call<Model> call, @NonNull Response<Model> response) {
        if(listener != null) {

            listener.hideLoader();

            Model model = response.body();
            if(model == null) {
                listener.onFailure(AppUtils.getString(R.string.error_something_wrong));
                return;
            }

            BaseResponse baseResponse = (BaseResponse) model;
            if(baseResponse.getStatusCode() != Const.OK) {
                listener.onFailure(baseResponse.getError());
                return;
            }

            onResponse(response);
        }
    }

    @Override
    public void onFailure(@NonNull Call<Model> call, @NonNull Throwable t) {
        if(listener != null) {
            listener.hideLoader();
            if (t instanceof IOException) {
                listener.onFailure(AppUtils.getString(R.string.error_network));
            }
        }
    }

    public abstract void onResponse(Response<Model> response);
}
