package apps.ahqmrf.kothay;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.binjar.prefsdroid.Preference;
import com.google.firebase.database.FirebaseDatabase;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.PermissionListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.account.ui.AccountActivity;
import apps.ahqmrf.kothay.alarm.callback.AlarmEventListener;
import apps.ahqmrf.kothay.alarm.callback.AlarmServiceListener;
import apps.ahqmrf.kothay.alarm.callback.OnUserNearCallback;
import apps.ahqmrf.kothay.alarm.connector.AlarmConnector;
import apps.ahqmrf.kothay.alarm.response.Alarm;
import apps.ahqmrf.kothay.alarm.response.SetAlarmResponse;
import apps.ahqmrf.kothay.alarm.ui.MyAlarmsActivity;
import apps.ahqmrf.kothay.auth.callbacks.LogoutListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.auth.ui.LoginActivity;
import apps.ahqmrf.kothay.friends.ui.FriendsActivity;
import apps.ahqmrf.kothay.home.ui.HomeActivity;
import apps.ahqmrf.kothay.meeting.ui.GroupsActivity;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.message.ui.MessageActivity;
import apps.ahqmrf.kothay.notification.callback.NotificationResponseListener;
import apps.ahqmrf.kothay.notification.connector.NotificationConnector;
import apps.ahqmrf.kothay.notification.response.MyNotification;
import apps.ahqmrf.kothay.notification.ui.NotificationActivity;
import apps.ahqmrf.kothay.search.ui.SearchActivity;
import apps.ahqmrf.kothay.settings.ui.SettingsActivity;
import apps.ahqmrf.kothay.tracker.ui.MyLocationActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

import static android.view.View.GONE;

/**
 * Created by bsse0 on 8/15/2017.
 */

public abstract class BaseActivity extends AuthActivity implements PopupMenu.OnMenuItemClickListener, LogoutListener, AlarmServiceListener, OnUserNearCallback, NotificationResponseListener {

    @Nullable @BindView(R.id.ic_home) public           ImageView homeView;
    @Nullable @BindView(R.id.ic_back) public           ImageView backView;
    @Nullable @BindView(R.id.blank_space) public       View      blankSpaceView;
    @Nullable @BindView(R.id.ic_menu) public           ImageView menuView;
    @Nullable @BindView(R.id.ic_alarm) public          ImageView alarmView;
    @Nullable @BindView(R.id.ic_chat) public           ImageView messagesView;
    @Nullable @BindView(R.id.title) public             TextView  titleView;
    @Nullable @BindView(R.id.notificationCount) public TextView  notificationView;

    protected PopupMenu                     popupMenu;
    public    LoginResponse                 self;
    public    DisplayImageOptions           displayImageOptions;
    public    ArrayList<Alarm>              alarms;
    public    AlarmConnector                alarmConnector;
    public    int                           activeAlarmCount;
    public    ArrayList<AlarmEventListener> alarmEventListeners;
    public    MediaPlayer                   mMediaPlayer;
    public    Vibrator                      vibrator;
    public    NotificationConnector         notificationConnector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = Preference.getObject(PrefKeys.USER_INFO, LoginResponse.class);
        displayImageOptions = AppUtils.getImageConfig();
        alarmConnector = new AlarmConnector(this);
        alarms = new ArrayList<>();
        alarmEventListeners = new ArrayList<>();

        notificationConnector = new NotificationConnector(this);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onViewCreated();
        if (menuView != null) {
            popupMenu = new PopupMenu(this, menuView);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.inflate(R.menu.popup_feature);
        }
        fetchAlarms();
        fetchNotifications();
        notificationConnector.listenToIncomingMessages();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (notificationConnector != null) {
            notificationConnector.stopListening();
            notificationConnector.listenToIncomingMessages();
        }
        FirebaseDatabase.getInstance().getReference().child("OnlineStatus").child(self.getUsername()).setValue(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseDatabase.getInstance().getReference().child("OnlineStatus").child(self.getUsername()).setValue(false);
    }

    @Override
    public void onIncomingMessage(ChatMessage message) {
        if (AppUtils.isChatOpen()) return;
        if (!message.isDelivered()) showNewMessageNotification(message);
        AppUtils.cacheMessage(message);
    }

    public void showNewMessageNotification(ChatMessage message) {
        if (AppUtils.isMessageNotificationEnabled()) {
            if (AppUtils.isMessageVibrationEnabled()) {
                if (vibrator != null) {
                    vibrator.vibrate(Const.VIBRATE_MESSAGE_DURATION);
                    vibrator.vibrate(Const.VIBRATE_MESSAGE_DURATION);
                }
            }
            createMessageNotification(message);
        }
    }

    public void createMessageNotification(final ChatMessage message) {
        final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(getApplicationContext(), MessageActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra(Const.MESSAGE, message);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);
        String content = message.getContent();
        if (message.getType().equals(Const.PHOTO)) content = "[Photo]";
        else if (message.getType().equals(Const.SMILEY)) content = "[Emoticon]";
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.notification);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setContentTitle(message.getFromName())
                .setContentText(content)
                .setSmallIcon(R.drawable.k)
                .setLargeIcon(bm)
                .setContentIntent(intent)
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        if (notificationManager != null)
            notificationManager.notify(1, notification);
    }

    public void fetchAlarms() {
        if (self != null) {
            alarmConnector.getAlarms(self.getId());
        }
    }

    public void fetchNotifications() {
        notificationConnector.fetchNotifications(self.getId());
    }

    @Override
    public void onAlarmSet(SetAlarmResponse response) {

    }

    @Override
    public void onAlarmsFetched(List<Alarm> alarms, int count) {
        this.alarms.clear();
        this.alarms.addAll(alarms);
        this.activeAlarmCount = count;
        trackUsersWithAlarmSet();
    }

    @Override
    public void onUserInside(Alarm alarm) {
        boolean wasNotified = Preference.getBoolean(alarm.getAlarmId() + "");
        if (!wasNotified) {
            Preference.putBoolean(alarm.getAlarmId() + "", true);
            triggerAlarm(alarm);
            if (AppUtils.isAlarmNotificationEnabled()) {
                showNotificationAlarm(alarm, true);
            }
        }
    }

    public void showNotificationAlarm(final Alarm alarm, final boolean isInside) {
        if (vibrator != null && AppUtils.isAlarmVibrationEnabled()) {
            vibrator.vibrate(Const.VIBRATE_ALARM_DURATION);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String content = alarm.getName() + " has left " + alarm.getPlace();
        if (isInside) {
            content = alarm.getName() + " is now at " + alarm.getPlace() + "!";
        }
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.notification);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setContentTitle("Alarm")
                .setContentText(content)
                .setSmallIcon(R.drawable.kk)
                .setLargeIcon(bm)
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        if (notificationManager != null)
            notificationManager.notify(0, notification);
    }

    public void triggerAlarm(Alarm alarm) {
        if (AppUtils.isAlarmSoundEnabled()) {
            playSound(this, getAlarmUri());
        }
        DialogInterface.OnClickListener alarmDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (mMediaPlayer != null) {
                            mMediaPlayer.stop();
                        }
                        if (vibrator != null) {
                            vibrator.cancel();
                        }
                        break;
                }
            }
        };
        AlertDialog.Builder alarmDialogBuilder = new AlertDialog.Builder(this);
        alarmDialogBuilder.setTitle("Alarm")
                .setMessage(alarm.getName() + " is now at " + alarm.getPlace() + "!")
                .setPositiveButton("Dismiss", alarmDialogClickListener);
        alarmDialogBuilder.show();
    }

    private void playSound(Context context, Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager != null && audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

    public Uri getAlarmUri() {
        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    @Override
    public void onUserOutSide(Alarm alarm) {
        String key = alarm.getAlarmId() + "";
        if (Preference.containsKey(key)) {
            if (Preference.getBoolean(key)) {
                if (AppUtils.isAlarmNotificationEnabled()) {
                    showNotificationAlarm(alarm, false);
                }
            }
            Preference.remove(key);
        }
    }

    public void trackUsersWithAlarmSet() {
        for (AlarmEventListener awl : alarmEventListeners) {
            awl.removeListener();
            Preference.remove(Const.LISTENER + "_" + awl.getAlarm().getAlarmId());
        }
        alarmEventListeners.clear();
        for (Alarm alarm : alarms) {
            boolean isListenerAttached = Preference.getBoolean(Const.LISTENER + "_" + alarm.getAlarmId());
            if (alarm.isEnabled() && alarm.sharesLocation()) {
                if (!isListenerAttached) {
                    AlarmEventListener awl = new AlarmEventListener(alarm, this);
                    alarmEventListeners.add(awl);
                    Preference.putBoolean(Const.LISTENER + "_" + alarm.getAlarmId(), true);
                }
            } else {
                Preference.remove(Const.LISTENER + "_" + alarm.getAlarmId());
            }
        }
    }

    @Override
    public void onAlarmUpdated() {
        fetchAlarms();
    }

    public void setCount(int count) {
        if (notificationView != null) {
            if (count > 0) {
                String txt = count + "";
                if (count > 9) txt = "9+";
                notificationView.setText(txt);
                notificationView.setVisibility(View.VISIBLE);
            } else {
                notificationView.setVisibility(View.GONE);
            }
        }
    }

    public void showBackArrow() {
        if (backView != null) {
            backView.setVisibility(View.VISIBLE);
            if (blankSpaceView != null) blankSpaceView.setVisibility(GONE);
        }
    }

    public abstract void onViewCreated();

    @Optional
    @OnClick(R.id.ic_menu)
    void onMenuClick() {
        // TODO open menu
        if (menuView != null) {
            Object menuHelper;
            Class[] argTypes;
            try {
                Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
                fMenuHelper.setAccessible(true);
                menuHelper = fMenuHelper.get(popupMenu);
                argTypes = new Class[]{boolean.class};
                menuHelper.getClass().getDeclaredMethod("setForceShowIcon", argTypes).invoke(menuHelper, true);
            } catch (Exception e) {
                // Possible exceptions are NoSuchMethodError and NoSuchFieldError
                //
                // In either case, an exception indicates something is wrong with the reflection code, or the
                // structure of the PopupMenu class or its dependencies has changed.
                //
                // These exceptions should never happen since we're shipping the AppCompat library in our own apk,
                // but in the case that they do, we simply can't force icons to display, so log the error and
                // show the menu normally.
            }
            popupMenu.show();
        }
    }

    @Optional
    @OnClick(R.id.ic_alarm)
    void onAlarmClick() {
        if (!(this instanceof MyAlarmsActivity)) {
            openPage(MyAlarmsActivity.class);
        }
    }

    void onSearchClick() {
        if (!(this instanceof SearchActivity)) {
            openPage(SearchActivity.class);
        }
    }

    @Optional
    @OnClick(R.id.ic_home)
    void onHomeClick() {
        if (!(this instanceof HomeActivity)) {
            openPage(HomeActivity.class);
        }
    }

    public void onFriendsClick() {
        if (!(this instanceof FriendsActivity)) {
            openPage(FriendsActivity.class);
        }
    }

    @Optional
    @OnClick(R.id.ic_chat)
    void onChatClick() {
        if (!(this instanceof MessageActivity)) {
            openPage(MessageActivity.class);
        }
    }

    void onSettingsClick() {
        if (!(this instanceof SettingsActivity)) {
            openPage(SettingsActivity.class);
        }
    }

    @Optional
    @OnClick(R.id.layout_notifications)
    void onNotificationsClick() {
        openPage(NotificationActivity.class);
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onLogoutSuccess() {
        AppUtils.clearUserCache();
        FirebaseDatabase.getInstance().getReference().child("OnlineStatus").child(self.getUsername()).setValue(false);
        finishAffinity();
        openPage(LoginActivity.class);
    }

    public void setLabel(String title) {
        if (titleView != null) titleView.setText(title);
    }

    public void setLabel(int resId) {
        setLabel(AppUtils.getString(resId));
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.menu_settings) onSettingsClick();
        else if (id == R.id.menu_location) openPage(MyLocationActivity.class);
        else if (id == R.id.menu_profile) openPage(AccountActivity.class);
        else if (id == R.id.menu_logout) logout();
        else if (id == R.id.menu_friends) onFriendsClick();
        else if (id == R.id.menu_search) onSearchClick();
        else if (id == R.id.menu_meet_friends) openPage(GroupsActivity.class);
        return false;
    }

    public void logout() {
        if (self != null) {
            new AuthConnector(this).logout(self.getEmail());
        } else onLogoutSuccess();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public boolean checkLocationPermission(PermissionListener permissionListener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Dexter.withActivity(this)
                        .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(permissionListener).check();
                return false;
            }
        }
        return true;
    }

    public void showGPSDisabledAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.alert_gps)
                .setCancelable(false)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onNotificationFetched(List<MyNotification> notifications) {

    }

    @Override
    public void onUnseenNotificationCountFetched(int count) {
        setCount(count);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        FirebaseDatabase.getInstance().getReference().child("OnlineStatus").child(self.getUsername()).setValue(false);

    }
}
