package apps.ahqmrf.kothay.home.request;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseRequest;

/**
 * Created by bsse0 on 11/26/2017.
 */

public class CheckInRequest extends BaseRequest {
    @SerializedName("status") private    String status;
    @SerializedName("latitude") private  double latitude;
    @SerializedName("longitude") private double longitude;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
