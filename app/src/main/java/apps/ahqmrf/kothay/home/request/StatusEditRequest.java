package apps.ahqmrf.kothay.home.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/14/2017.
 */

public class StatusEditRequest {
    @SerializedName("statusId") private long   statusId;
    @SerializedName("action") private   String action;
    @SerializedName("status") private   String status;

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
