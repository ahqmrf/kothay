package apps.ahqmrf.kothay.home.connector;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.account.request.UpdateProfileRequest;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import apps.ahqmrf.kothay.friends.request.FriendshipControlRequest;
import apps.ahqmrf.kothay.home.callback.HomeServiceListener;
import apps.ahqmrf.kothay.home.request.CheckInRequest;
import apps.ahqmrf.kothay.home.request.StatusEditRequest;
import apps.ahqmrf.kothay.home.response.StatusResponse;
import apps.ahqmrf.kothay.home.service.HomeClient;
import apps.ahqmrf.kothay.home.service.HomeService;
import apps.ahqmrf.kothay.util.Const;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 11/13/2017.
 */

public class HomeConnector {
    private ResponseListener listener;
    private HomeService      service;

    public HomeConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = new HomeClient().createService();
    }

    public void getStatus(long userId) {
        if (listener != null) {
            FriendshipControlRequest request = new FriendshipControlRequest();
            request.setUserId(userId);

            Call<StatusResponse> call = service.getStatus(request);
            call.enqueue(new ServiceCallback<StatusResponse>(listener) {
                @Override
                public void onResponse(Response<StatusResponse> response) {
                    StatusResponse body = response.body();
                    if (body != null) {
                        ((HomeServiceListener) listener).onStatusFetched(body.getPosts());
                    }
                }
            });
        }
    }


    public void checkIn(long userId, String status, double lat, double lng) {
        if (listener != null) {
            listener.showLoader();
            CheckInRequest request = new CheckInRequest();
            request.setUserId(userId);
            request.setStatus(status);
            request.setLatitude(lat);
            request.setLongitude(lng);

            Call<BaseResponse> call = service.checkIn(request);

            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse res = response.body();
                    if (res != null) {
                        listener.onSuccess(res.getMessage());
                        ((HomeServiceListener) listener).onStatusUpdate();
                    }
                }
            });
        }
    }

    public void updateStatus(long userId, String status) {
        if (listener != null) {
            listener.showLoader();
            UpdateProfileRequest request = new UpdateProfileRequest();
            request.setUserId(userId);
            request.setValue(status);
            Call<AccountUpdateResponse> call = service.updateStatus(request);

            call.enqueue(new ServiceCallback<AccountUpdateResponse>(listener) {
                @Override
                public void onResponse(Response<AccountUpdateResponse> response) {
                    AccountUpdateResponse res = response.body();
                    if (res != null) {
                        listener.onSuccess(res.getMessage());
                        ((HomeServiceListener) listener).onStatusUpdate();
                    }
                }
            });
        }
    }

    public void editStatus(long statusId, String status) {
        if (listener != null) {
            listener.showLoader();
            StatusEditRequest request = new StatusEditRequest();
            request.setAction(Const.EDIT_STATUS);
            request.setStatus(status);
            request.setStatusId(statusId);
            Call<BaseResponse> call = service.editStatus(request);

            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse res = response.body();
                    if (res != null) {
                        listener.onSuccess(res.getMessage());
                        ((HomeServiceListener) listener).onStatusEdited();
                    }
                }
            });
        }
    }

    public void removeStatus(long statusId) {
        if (listener != null) {
            listener.showLoader();
            StatusEditRequest request = new StatusEditRequest();
            request.setAction(Const.REMOVE_STATUS);
            request.setStatusId(statusId);
            Call<BaseResponse> call = service.editStatus(request);

            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse res = response.body();
                    if (res != null) {
                        listener.onSuccess(res.getMessage());
                        ((HomeServiceListener) listener).onStatusRemoved();
                    }
                }
            });
        }
    }
}
