package apps.ahqmrf.kothay.home.callback;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import apps.ahqmrf.kothay.home.response.Status;

/**
 * Created by bsse0 on 11/13/2017.
 */

public interface HomeServiceListener {

    void onStatusFetched(List<Status> posts);

    void onStatusUpdate();

    void onStatusEdited();

    void onStatusRemoved();
}
