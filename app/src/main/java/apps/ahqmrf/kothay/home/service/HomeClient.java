package apps.ahqmrf.kothay.home.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 11/13/2017.
 */

public class HomeClient extends Client<HomeService> {
    @Override
    public HomeService createService() {
        return getRetrofit().create(HomeService.class);
    }
}
