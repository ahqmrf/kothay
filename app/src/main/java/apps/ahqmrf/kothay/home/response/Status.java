package apps.ahqmrf.kothay.home.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 11/13/2017.
 */

public class Status extends UserInfoResponse implements Comparable<Status> {
    @SerializedName("time") private      String time;
    @SerializedName("statusId") private  long   statusId;
    @SerializedName("status") private    String status;
    @SerializedName("type") private      String type;
    @SerializedName("longitude") private double longitude;
    @SerializedName("latitude") private  double latitude;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int compareTo(@NonNull Status o) {
        return getTime().compareTo(o.getTime());
    }
}
