package apps.ahqmrf.kothay.home.service;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.account.request.UpdateProfileRequest;
import apps.ahqmrf.kothay.account.response.AccountUpdateResponse;
import apps.ahqmrf.kothay.friends.request.FriendshipControlRequest;
import apps.ahqmrf.kothay.home.request.CheckInRequest;
import apps.ahqmrf.kothay.home.request.StatusEditRequest;
import apps.ahqmrf.kothay.home.response.StatusResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 11/13/2017.
 */

public interface HomeService {
    @POST("get_feeds.php")
    Call<StatusResponse> getStatus(@Body FriendshipControlRequest request);

    @POST("update_status.php")
    Call<AccountUpdateResponse> updateStatus(@Body UpdateProfileRequest request);

    @POST("edit_status.php")
    Call<BaseResponse> editStatus(@Body StatusEditRequest request);

    @POST("check_in.php")
    Call<BaseResponse> checkIn(@Body CheckInRequest request);
}
