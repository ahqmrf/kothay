package apps.ahqmrf.kothay.home.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.LocationUpdateService;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.account.ui.AccountActivity;
import apps.ahqmrf.kothay.account.ui.UpdateAccountFragment;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.friends.ui.FriendProfileActivity;
import apps.ahqmrf.kothay.home.callback.HomeServiceListener;
import apps.ahqmrf.kothay.home.callback.StatusActionCallback;
import apps.ahqmrf.kothay.home.connector.HomeConnector;
import apps.ahqmrf.kothay.home.response.Status;
import apps.ahqmrf.kothay.tracker.ui.ViewLocationActivity;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.ui.ProfileActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeServiceListener, StatusActionCallback, UserListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.statusView)         RecyclerView       statusListView;
    private                            StatusListAdapter  adapter;
    private                            HomeConnector      connector;
    private                            String             status;
    private                            GoogleApiClient    mGoogleApiClient;
    private                            LocationManager    mLocationManager;
    boolean alertShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        connector = new HomeConnector(this);
    }

    @Override
    public void onViewCreated() {
        setLabel(R.string.title_home);
        if (homeView != null) {
            homeView.setColorFilter(AppUtils.getColor(R.color.bottomIconFocused));
            homeView.setBackgroundColor(AppUtils.getColor(R.color.colorPrimaryDark));
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                connector.getStatus(self.getId());
                fetchNotifications();
            }
        });

        adapter = new StatusListAdapter(new ArrayList<>(), this);
        statusListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        statusListView.setAdapter(adapter);
        showLoader();
        connector.getStatus(self.getId());
    }

    private void showAlertIfNeeded() {
        if (alertShown) return;
        alertShown = true;
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionListener permissionListener = new PermissionListener() {
                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {
                    AppUtils.toast(R.string.msg_permission_granted);
                    Intent intent = new Intent(getApplicationContext(), LocationUpdateService.class);
                    stopService(intent);
                    startService(intent);
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {
                    AppUtils.toast(R.string.error_permission);
                }

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
            };
            checkLocationPermission(permissionListener);
        } else {
            if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSDisabledAlert();
            }
        }

    }

    @Override
    public void onStatusFetched(List<Status> posts) {
        ArrayList<Object> items = new ArrayList<>();
        items.addAll(posts);
        items.add(0, self);
        adapter.filter(items);
        swipeRefreshLayout.setRefreshing(false);
        statusListView.scrollToPosition(0);
        hideLoader();
        showAlertIfNeeded();
    }

    @Override
    public void onItemClick(Object item) {
        if (item instanceof Status) {
            LoginResponse response = (LoginResponse) item;
            if (response.getId() == self.getId()) {
                openPage(AccountActivity.class);
            } else {
                UserInfoRequest request = new UserInfoRequest();
                request.setUserId(self.getId());
                request.setOtherUserId(response.getId());
                new UserConnector(this).fetchUser(request);
            }
        } else {
            updateStatus((String) item);
        }
    }

    private void updateStatus(String status) {
        if (!TextUtils.isEmpty(status)) {
            connector.updateStatus(self.getId(), status);
        } else {
            AppUtils.toast(R.string.update_hint);
        }
        hideSoftKeyboard();
    }

    @Override
    public void onStatusUpdate() {
        showLoader();
        connector.getStatus(self.getId());
    }

    @Override
    public void onStatusEdited() {
        showLoader();
        connector.getStatus(self.getId());
    }

    @Override
    public void onStatusRemoved() {
        showLoader();
        connector.getStatus(self.getId());
    }

    @Override
    public void onUserFetched(UserInfoResponse response) {
        Intent intent;
        if (response.isFriend()) {
            intent = new Intent(this, FriendProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
    }

    @Override
    public void onSuccess(String message) {
        super.onSuccess(message);
    }

    @Override
    public void onEditStatusClick(Status status) {
        UpdateAccountFragment frag = UpdateAccountFragment.getInstance(Const.EDIT_STATUS, status.getStatus(), status.getStatusId());
        frag.show(getSupportFragmentManager(), Const.EDIT_STATUS);
    }

    @Override
    public void onRemoveStatusClick(final Status status) {
        DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        connector.removeStatus(status.getStatusId());
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm")
                .setMessage("Are you sure you want to remove this post?")
                .setPositiveButton("Yes", mDialogClickListener)
                .setNegativeButton("No", mDialogClickListener);
        builder.show();
    }

    @Override
    public void onImageClick() {
        openPage(AccountActivity.class);
    }

    @Override
    public void onCopyClick(String status) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (clipboard != null) {
            ClipData clip = ClipData.newPlainText("Status", status);
            clipboard.setPrimaryClip(clip);
            AppUtils.toast("Status copied to clipboard");
        }
    }

    @Override
    public void onCheckInClick(String status) {
        hideSoftKeyboard();
        this.status = status;
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                proceed();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                AppUtils.toast(R.string.error_permission);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
        };

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (checkLocationPermission(permissionListener)) {
            proceed();
        }
    }

    @Override
    public void onViewLocationClick(double lat, double lng) {
        Intent intent = new Intent(this, ViewLocationActivity.class);
        intent.putExtra(Const.LATITUDE, lat);
        intent.putExtra(Const.LONGITUDE, lng);
        openPage(intent);
    }

    private void proceed() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlert();
        }

        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        } else {
            if (mGoogleApiClient.isConnected()) {
                requestLocation();
            } else {
                mGoogleApiClient.connect();
            }
        }
    }

    public void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void requestLocation() {
        final LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        connector.checkIn(self.getId(), status, location.getLatitude(), location.getLongitude());
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public void editStatus(long statusId, String status) {
        connector.editStatus(statusId, status);
    }

    @Override
    public void onFailure(String message) {
        super.onFailure(message);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        exitApp();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
