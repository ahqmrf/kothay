package apps.ahqmrf.kothay.home.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by bsse0 on 11/13/2017.
 */

public class StatusResponse extends BaseResponse {
    @SerializedName("posts") List<Status> posts;

    public List<Status> getPosts() {
        return posts;
    }
}
