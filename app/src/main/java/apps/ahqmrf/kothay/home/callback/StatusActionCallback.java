package apps.ahqmrf.kothay.home.callback;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.home.response.Status;

/**
 * Created by bsse0 on 11/14/2017.
 */

public interface StatusActionCallback extends OnItemClickCallback {
    void onEditStatusClick(Status status);

    void onRemoveStatusClick(Status status);

    void onImageClick();

    void onCopyClick(String status);

    void onCheckInClick(String status);

    void onViewLocationClick(double lat, double lng);
}
