package apps.ahqmrf.kothay.home.ui;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.binjar.prefsdroid.Preference;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.App;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.home.callback.StatusActionCallback;
import apps.ahqmrf.kothay.home.response.Status;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 11/13/2017.
 */

public class StatusListAdapter extends RecyclerView.Adapter {

    private static final int STATUS_TYPE = 1;
    private List<Object>         items;
    private StatusActionCallback callback;
    private String               email;

    public StatusListAdapter(List<Object> items, StatusActionCallback callback) {
        filter(items);
        this.callback = callback;
        LoginResponse self = Preference.getObject(PrefKeys.USER_INFO, LoginResponse.class);
        email = self.getEmail();
    }

    public void filter(List<Object> items) {
        this.items = new ArrayList<>();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == STATUS_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status, parent, false);
            return new StatusViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_input_layout, parent, false);
            return new StatusInputViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == STATUS_TYPE)
            ((StatusViewHolder) holder).bindTo((Status) items.get(position));
        else {
            ((StatusInputViewHolder) holder).bindTo((LoginResponse) items.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof Status) return STATUS_TYPE;
        return 2;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class StatusViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {

        @BindView(R.id.image)   ImageView imageView;
        @BindView(R.id.name)    TextView  nameView;
        @BindView(R.id.time)    TextView  timeView;
        @BindView(R.id.post)    TextView  postView;
        @BindView(R.id.checkIn) TextView  checkInView;
        @BindView(R.id.options) ImageView optionsView;

        PopupMenu popupMenu;

        public StatusViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindTo(Status status) {
            popupMenu = new PopupMenu(App.getContext(), optionsView);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.inflate(R.menu.status_action_menu);

            int placeholder = R.drawable.man_placeholder;
            if (status.getGender().equals(Const.FEMALE)) placeholder = R.drawable.woman_placeholder;
            ImageLoader.getInstance().displayImage(
                    status.getImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(placeholder)
            );

            nameView.setText(status.getName());

            if (status.getType().equals(Const.STATUS)) {
                checkInView.setVisibility(View.GONE);
                postView.setText(status.getStatus());
                postView.setVisibility(View.VISIBLE);
            } else {
                checkInView.setText("Checked in " + AppUtils.getPlace(status.getLatitude(), status.getLongitude()));
                if(TextUtils.isEmpty(status.getStatus())) {
                    postView.setVisibility(View.GONE);
                } else {
                    postView.setText(status.getStatus());
                    postView.setVisibility(View.VISIBLE);
                }
                checkInView.setVisibility(View.VISIBLE);
            }

            AppUtils.TimeModel model = new AppUtils.TimeModel(status.getTime());
            timeView.setText(model.getDateTime());

            if (!(!TextUtils.isEmpty(email) && email.equals(status.getEmail()))) {
                MenuItem item = popupMenu.getMenu().findItem(R.id.menu_edit);
                item.setVisible(false);
                item = popupMenu.getMenu().findItem(R.id.menu_remove);
                item.setVisible(false);
            } else {
                MenuItem item = popupMenu.getMenu().findItem(R.id.menu_edit);
                item.setVisible(true);
                item = popupMenu.getMenu().findItem(R.id.menu_remove);
                item.setVisible(true);
            }

            if (status.getType().equals(Const.STATUS)) {
                MenuItem item = popupMenu.getMenu().findItem(R.id.menu_location);
                item.setVisible(false);
                item = popupMenu.getMenu().findItem(R.id.menu_edit);
                item.setVisible(true);
            } else {
                MenuItem item = popupMenu.getMenu().findItem(R.id.menu_location);
                item.setVisible(true);
                item = popupMenu.getMenu().findItem(R.id.menu_edit);
                item.setVisible(false);
            }
        }

        @OnClick(R.id.options)
        void onOptionsClick() {
            popupMenu.show();
        }

        @OnClick({R.id.image, R.id.name})
        void onImageOrNameClick() {
            if (callback != null) {
                callback.onItemClick(items.get(getAdapterPosition()));
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.menu_edit) {
                if (callback != null) {
                    callback.onEditStatusClick((Status) items.get(getAdapterPosition()));
                }
            }
            if (id == R.id.menu_remove) {
                if (callback != null) {
                    callback.onRemoveStatusClick((Status) items.get(getAdapterPosition()));
                }
            }
            if (id == R.id.menu_copy) {
                if (callback != null) {
                    Status status = (Status) items.get(getAdapterPosition());
                    String text   = AppUtils.getPlace(status.getLatitude(), status.getLongitude());
                    if (status.getType().equals(Const.STATUS)) text = status.getStatus();
                    callback.onCopyClick(text);
                }
            }
            if(id == R.id.menu_location) {
                if(callback != null) {
                    Status status = (Status) items.get(getAdapterPosition());
                    callback.onViewLocationClick(status.getLatitude(), status.getLongitude());
                }
            }
            return false;
        }
    }

    class StatusInputViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)       ImageView imageView;
        @BindView(R.id.statusInput) EditText  inputView;

        public StatusInputViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindTo(LoginResponse response) {
            int placeholder = R.drawable.man_placeholder;
            if (response.getGender().equals(Const.FEMALE))
                placeholder = R.drawable.woman_placeholder;
            ImageLoader.getInstance().displayImage(
                    response.getImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(placeholder)
            );
            inputView.setText("");
        }

        @OnClick(R.id.btnShare)
        void onShareClick() {
            if (callback != null) {
                callback.onItemClick(inputView.getText().toString());
            }
        }

        @OnClick(R.id.image)
        void onImageClick() {
            if (callback != null) {
                callback.onImageClick();
            }
        }

        @OnClick(R.id.btnCheckIn)
        void onCheckInClick() {
            if (callback != null) {
                callback.onCheckInClick(inputView.getText().toString());
            }
        }
    }
}
