package apps.ahqmrf.kothay.auth.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by maruf on 8/20/2017.
 */

public class LoginResponse extends BaseResponse implements Parcelable {
    @SerializedName("id") private               long    id;
    @SerializedName("email") private            String  email;
    @SerializedName("username") private         String  username;
    @SerializedName("name") private             String  name;
    @SerializedName("verified") private         boolean verified;
    @SerializedName("confirmationCode") private String  confirmationCode;
    @SerializedName("workplace") private        String  workplace;
    @SerializedName("token") private            String  token;
    @SerializedName("imageUrl") private         String  imageUrl;
    @SerializedName("gender") private           String  gender;
    @SerializedName("phone") private            String  phone;
    @SerializedName("bio") private              String  bio;
    @SerializedName("alarmCount") private       int     alarmCount;
    @SerializedName("sharesLocation") private   boolean sharesLocation;

    public boolean sharesLocation() {
        return sharesLocation;
    }

    public void setSharesLocation(boolean sharesLocation) {
        this.sharesLocation = sharesLocation;
    }

    public LoginResponse() {

    }


    protected LoginResponse(Parcel in) {
        id = in.readLong();
        email = in.readString();
        username = in.readString();
        name = in.readString();
        verified = in.readByte() != 0;
        confirmationCode = in.readString();
        workplace = in.readString();
        token = in.readString();
        imageUrl = in.readString();
        gender = in.readString();
        phone = in.readString();
        bio = in.readString();
        alarmCount = in.readInt();
        sharesLocation = in.readByte() != 0;
    }

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel in) {
            return new LoginResponse(in);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public String getWorkplace() {
        return workplace;
    }

    public String getToken() {
        return token;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public String getPhone() {
        return phone;
    }

    public String getBio() {
        return bio;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(int alarmCount) {
        this.alarmCount = alarmCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(email);
        parcel.writeString(username);
        parcel.writeString(name);
        parcel.writeByte((byte) (verified ? 1 : 0));
        parcel.writeString(confirmationCode);
        parcel.writeString(workplace);
        parcel.writeString(token);
        parcel.writeString(imageUrl);
        parcel.writeString(gender);
        parcel.writeString(phone);
        parcel.writeString(bio);
        parcel.writeInt(alarmCount);
        parcel.writeByte((byte) (sharesLocation ? 1 : 0));
    }
}
