package apps.ahqmrf.kothay.auth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import apps.ahqmrf.kothay.AuthActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.callbacks.VerificationCodeListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.EmailSender;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AuthActivity implements VerificationCodeListener {

    @BindView(R.id.email_layout)  TextInputLayout emailLayout;
    @BindView(R.id.email)         EditText        emailView;
    @BindView(R.id.btn_send_code) Button          sendCodeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_send_code)
    void onSendCodeClick() {
        hideSoftKeyboard();
        emailLayout.setError(null);
        String email = emailView.getText().toString();
        if (TextUtils.isEmpty(email)) {
            emailLayout.setError(AppUtils.getString(R.string.error_field_required));
            return;
        }
        if (AppUtils.isValidEmail(email)) {
            sendCodeBtn.setClickable(false);
            sendCodeBtn.setAlpha(0.5f);
            sendCodeBtn.setText(R.string.btn_sending_code);
            new AuthConnector(this).sendVerificationCode(email);
        } else {
            emailLayout.setError(AppUtils.getString(R.string.error_invalid_email));
        }
    }

    @Override
    public void onEmailSent(String verificationCode) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra(Const.VERIFICATION_CODE, verificationCode);
        intent.putExtra(Const.EMAIL, emailView.getText().toString());
        openPage(intent);
        finish();
    }

    @Override
    public void onCodeGenerated(String verificationCode) {
        String      message = "Your email verification code is " + verificationCode;
        EmailSender es      = new EmailSender(this, emailView.getText().toString(), "Verify Email", message, verificationCode);
        es.execute();
        sendCodeBtn.setClickable(true);
        sendCodeBtn.setAlpha(1f);
        sendCodeBtn.setText(R.string.btn_send_code);
    }
}
