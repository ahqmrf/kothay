package apps.ahqmrf.kothay.auth.connector;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.auth.callbacks.LoginListener;
import apps.ahqmrf.kothay.auth.callbacks.LogoutListener;
import apps.ahqmrf.kothay.auth.callbacks.RegListener;
import apps.ahqmrf.kothay.auth.callbacks.VerificationCodeListener;
import apps.ahqmrf.kothay.auth.request.ChangePasswordRequest;
import apps.ahqmrf.kothay.auth.request.CheckLoginStatusRequest;
import apps.ahqmrf.kothay.auth.request.LoginRequest;
import apps.ahqmrf.kothay.auth.request.RegRequest;
import apps.ahqmrf.kothay.auth.request.ResetPasswordRequest;
import apps.ahqmrf.kothay.auth.request.VerificationCodeRequest;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.auth.response.RegResponse;
import apps.ahqmrf.kothay.auth.response.VerifyEmailResponse;
import apps.ahqmrf.kothay.auth.service.AuthClient;
import apps.ahqmrf.kothay.auth.service.AuthService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by maruf on 8/20/2017.
 */

public class AuthConnector {
    private ResponseListener listener;
    private AuthService      service;

    public AuthConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = new AuthClient().createService();
    }

    public void login(LoginRequest request) {
        if (listener != null) {
            Call<LoginResponse> call = service.login(request);
            call.enqueue(new ServiceCallback<LoginResponse>(listener) {
                @Override
                public void onResponse(Response<LoginResponse> response) {
                    LoginResponse body = response.body();
                    if (body != null) {
                        if (body.isVerified()) {
                            listener.onSuccess(body.getMessage());
                            ((LoginListener) listener).onLoginSuccess(body);
                        } else {
                            listener.onFailure(body.getMessage());
                            ((LoginListener) listener).onActivationRequired(body);

                        }
                    }
                }
            });
        }
    }

    public void checkLoginStatus(String email, String token) {
        if (listener != null) {
            CheckLoginStatusRequest request = new CheckLoginStatusRequest();
            request.setEmail(email);
            request.setToken(token);
            Call<LoginResponse> call = service.checkLogin(request);
            call.enqueue(new ServiceCallback<LoginResponse>(listener) {
                @Override
                public void onResponse(Response<LoginResponse> response) {
                    LoginResponse body = response.body();
                    if (body != null) {
                        ((LoginListener) listener).onLoginSuccess(body);
                    }
                }
            });
        }
    }

    public void register(RegRequest request) {
        if (listener != null) {
            Call<RegResponse> call = service.register(request);
            call.enqueue(new ServiceCallback<RegResponse>(listener) {
                @Override
                public void onResponse(Response<RegResponse> response) {
                    RegResponse body = response.body();
                    if (body != null) {
                        ((RegListener) listener).onCodeRetrieved(body.getConfirmationCode());
                        listener.onSuccess(body.getMessage());
                    }
                }
            });
        }
    }

    public void activateAccount(String email) {
        if (listener != null) {
            LoginRequest request = new LoginRequest();
            request.setEmail(email);
            Call<BaseResponse> call = service.activateAccount(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                    }
                }
            });
        }
    }

    public void logout(String email) {
        if (listener != null) {
            listener.showLoader();
            LoginRequest request = new LoginRequest();
            request.setEmail(email);
            Call<BaseResponse> call = service.logout(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                        ((LogoutListener) listener).onLogoutSuccess();
                    }
                }
            });
        }
    }

    public void sendVerificationCode(String email) {
        if (listener != null) {
            VerificationCodeRequest request = new VerificationCodeRequest();
            request.setEmail(email);
            Call<VerifyEmailResponse> call = service.sendVerificationCode(request);
            call.enqueue(new ServiceCallback<VerifyEmailResponse>(listener) {
                @Override
                public void onResponse(Response<VerifyEmailResponse> response) {
                    VerifyEmailResponse body = response.body();
                    if (body != null) {
                        ((VerificationCodeListener) listener).onCodeGenerated(body.getVerificationCode());
                    }
                }
            });
        }
    }

    public void resetPassword(String email, String password) {
        if (listener != null) {
            listener.showLoader();
            ResetPasswordRequest request = new ResetPasswordRequest();
            request.setEmail(email);
            request.setPassword(password);

            Call<BaseResponse> call = service.resetPassword(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                    }
                }
            });
        }
    }

    public void changePassword(String email, String password, String newPassword) {
        if (listener != null) {
            ChangePasswordRequest request = new ChangePasswordRequest();
            request.setEmail(email);
            request.setPassword(password);
            request.setNewPassword(newPassword);

            Call<BaseResponse> call = service.changePassword(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                    }
                }
            });
        }
    }
}
