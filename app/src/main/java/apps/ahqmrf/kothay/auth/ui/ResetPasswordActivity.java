package apps.ahqmrf.kothay.auth.ui;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import apps.ahqmrf.kothay.AuthActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordActivity extends AuthActivity {

    @BindView(R.id.instruction)             TextView        instructionView;
    @BindView(R.id.code)                    EditText        codeView;
    @BindView(R.id.code_layout)             TextInputLayout codeLayout;
    @BindView(R.id.password)                EditText        passwordView;
    @BindView(R.id.password_layout)         TextInputLayout passwordLayout;
    @BindView(R.id.confirm_password)        EditText        confirmPasswordView;
    @BindView(R.id.confirm_password_layout) TextInputLayout confirmPasswordLayout;
    @BindView(R.id.btnResetPassword)        Button          resetPassBtn;

    private String code;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        code = getIntent().getStringExtra(Const.VERIFICATION_CODE);
        email = getIntent().getStringExtra(Const.EMAIL);
        String promptText = "An email has been sent to " + email + " with a verification code. Please check your email.";
        instructionView.setText(promptText);
    }

    @OnClick(R.id.btnResetPassword)
    void onResetPasswordClick() {
        hideSoftKeyboard();
        if (formIsValid()) {
            resetPassBtn.setClickable(false);
            resetPassBtn.setAlpha(0.5f);
            resetPassBtn.setText(R.string.btn_resetting_password);
            new AuthConnector(this).resetPassword(email, passwordView.getText().toString());
        }
    }

    private boolean formIsValid() {
        clearErrors();
        boolean isValid = true;
        if (AppUtils.isEmpty(codeView)) {
            codeLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (!codeView.getText().toString().equals(code)) {
            codeLayout.setError(AppUtils.getString(R.string.error_invalid_code));
            isValid = false;
        }
        if (AppUtils.isEmpty(passwordView)) {
            passwordLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (passwordView.getText().toString().length() < 6) {
            passwordLayout.setError(AppUtils.getString(R.string.error_short_password));
            isValid = false;
        }
        if (!passwordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
            confirmPasswordLayout.setError(AppUtils.getString(R.string.error_password_mismatch));
            isValid = false;
        }
        return isValid;
    }

    private void clearErrors() {
        codeLayout.setError(null);
        passwordLayout.setError(null);
        confirmPasswordLayout.setError(null);
    }

    @Override
    public void onSuccess(String message) {
        super.onSuccess(message);
        resetPassBtn.setClickable(true);
        resetPassBtn.setAlpha(1f);
        resetPassBtn.setText(R.string.btn_reset_password);
        finishAffinity();
        openPage(LoginActivity.class);
    }
}
