package apps.ahqmrf.kothay.auth.callbacks;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by bsse0 on 10/20/2017.
 */

public interface LogoutListener extends ResponseListener {
    void onLogoutSuccess();
}
