package apps.ahqmrf.kothay.auth.callbacks;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by maruf on 8/19/2017.
 */

public interface RegListener extends ResponseListener {
    void onCodeRetrieved(String emailVerificationCode);
}
