package apps.ahqmrf.kothay.auth.service;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.auth.request.ChangePasswordRequest;
import apps.ahqmrf.kothay.auth.request.CheckLoginStatusRequest;
import apps.ahqmrf.kothay.auth.request.LoginRequest;
import apps.ahqmrf.kothay.auth.request.RegRequest;
import apps.ahqmrf.kothay.auth.request.ResetPasswordRequest;
import apps.ahqmrf.kothay.auth.request.VerificationCodeRequest;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.auth.response.RegResponse;
import apps.ahqmrf.kothay.auth.response.VerifyEmailResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by maruf on 8/19/2017.
 */

public interface AuthService {
    @POST("register/index.php")
    Call<RegResponse> register(@Body RegRequest request);

    @POST("activate_account.php")
    Call<BaseResponse> activateAccount(@Body LoginRequest request);

    @POST("login.php")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("check_login_status.php")
    Call<LoginResponse> checkLogin(@Body CheckLoginStatusRequest request);

    @POST("logout.php")
    Call<BaseResponse> logout(@Body LoginRequest request);

    @POST("send_verification_code.php")
    Call<VerifyEmailResponse> sendVerificationCode(@Body VerificationCodeRequest request);

    @POST("reset_password.php")
    Call<BaseResponse> resetPassword(@Body ResetPasswordRequest request);

    @POST("change_password.php")
    Call<BaseResponse> changePassword(@Body ChangePasswordRequest request);
}
