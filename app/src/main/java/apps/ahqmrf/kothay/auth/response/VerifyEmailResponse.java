package apps.ahqmrf.kothay.auth.response;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class VerifyEmailResponse extends BaseResponse {
    @SerializedName("verificationCode") private String verificationCode;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
