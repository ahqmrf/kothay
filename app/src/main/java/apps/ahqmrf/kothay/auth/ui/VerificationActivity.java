package apps.ahqmrf.kothay.auth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import apps.ahqmrf.kothay.AuthActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerificationActivity extends AuthActivity implements ResponseListener {

    @BindView(R.id.prompt)      TextView        promptView;
    @BindView(R.id.code)        EditText        codeView;
    @BindView(R.id.code_layout) TextInputLayout codeLayout;
    @BindView(R.id.btn_submit)  Button          submitBtn;

    private String        email;
    private String        verificationCode;
    private AuthConnector connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        connector = new AuthConnector(this);
    }

    public void onViewCreated() {
        email = getIntent().getStringExtra(Const.EMAIL);
        verificationCode = getIntent().getStringExtra(Const.VERIFICATION_CODE);
        if (email != null) {
            String promptText = "An email has been sent to " + email + " with a verification code. Please check your email.";
            promptView.setText(promptText);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onViewCreated();
    }

    @OnClick(R.id.btn_submit)
    void onSubmitClick() {
        hideSoftKeyboard();
        codeLayout.setError("");
        if (AppUtils.isEmpty(codeView)) {
            codeLayout.setError(AppUtils.getString(R.string.error_field_required));
        } else {
            String enteredCode = codeView.getText().toString();
            if (enteredCode.equals(verificationCode)) {
                submitBtn.setClickable(false);
                submitBtn.setAlpha(0.5f);
                submitBtn.setText(R.string.btn_submitting);
                connector.activateAccount(email);
            } else {
                codeLayout.setError(AppUtils.getString(R.string.error_invalid_code));
            }
        }
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
        submitBtn.setClickable(true);
        submitBtn.setAlpha(1f);
        submitBtn.setText(R.string.btn_submit);
        finishAffinity();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Const.EMAIL, email);
        openPage(intent);
    }
}
