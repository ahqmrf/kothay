package apps.ahqmrf.kothay.auth.callbacks;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by bsse0 on 10/31/2017.
 */

public interface VerificationCodeListener extends ResponseListener {
    void onEmailSent(String verificationCode);

    void onCodeGenerated(String verificationCode);
}
