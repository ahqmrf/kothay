package apps.ahqmrf.kothay.auth.callbacks;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.auth.response.LoginResponse;

/**
 * Created by maruf on 8/20/2017.
 */

public interface LoginListener extends ResponseListener{
    void onLoginSuccess(LoginResponse response);

    void onActivationRequired(LoginResponse response);
}
