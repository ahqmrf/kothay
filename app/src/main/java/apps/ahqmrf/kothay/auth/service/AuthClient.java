package apps.ahqmrf.kothay.auth.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by maruf on 8/19/2017.
 */

public class AuthClient extends Client<AuthService> {
    @Override
    public AuthService createService() {
        return getRetrofit().create(AuthService.class);
    }
}
