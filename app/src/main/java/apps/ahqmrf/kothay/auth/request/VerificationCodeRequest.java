package apps.ahqmrf.kothay.auth.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/12/2017.
 */

public class VerificationCodeRequest {
    @SerializedName("email") private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
