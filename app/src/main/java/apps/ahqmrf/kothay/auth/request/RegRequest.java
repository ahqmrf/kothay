package apps.ahqmrf.kothay.auth.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maruf on 8/19/2017.
 */

public class RegRequest extends LoginRequest {
    @SerializedName("username") private String username;
    @SerializedName("name") private String name;
    @SerializedName("workplace") private String workplace;
    @SerializedName("gender") private String gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
