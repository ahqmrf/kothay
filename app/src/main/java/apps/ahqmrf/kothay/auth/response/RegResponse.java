package apps.ahqmrf.kothay.auth.response;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by maruf on 8/19/2017.
 */

public class RegResponse extends BaseResponse {
    @SerializedName("confirmationCode") private String confirmationCode;

    public String getConfirmationCode() {
        return confirmationCode;
    }
}
