package apps.ahqmrf.kothay.auth.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/25/2017.
 */

public class ChangePasswordRequest extends ResetPasswordRequest {
    @SerializedName("newPassword") private String newPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
