package apps.ahqmrf.kothay.auth.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 10/20/2017.
 */

public class CheckLoginStatusRequest {
    @SerializedName("email") private String email;
    @SerializedName("token") private String token;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
