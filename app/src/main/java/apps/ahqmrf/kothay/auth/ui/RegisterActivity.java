package apps.ahqmrf.kothay.auth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import org.apache.commons.lang.WordUtils;

import apps.ahqmrf.kothay.AuthActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.callbacks.RegListener;
import apps.ahqmrf.kothay.auth.callbacks.VerificationCodeListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.auth.request.RegRequest;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.EmailSender;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AuthActivity implements RegListener, VerificationCodeListener {
    @BindView(R.id.username)                EditText        usernameView;
    @BindView(R.id.username_layout)         TextInputLayout usernameLayout;
    @BindView(R.id.email)                   EditText        emailView;
    @BindView(R.id.email_layout)            TextInputLayout emailLayout;
    @BindView(R.id.name)                    EditText        nameView;
    @BindView(R.id.name_layout)             TextInputLayout nameLayout;
    @BindView(R.id.workplace)               EditText        workplaceView;
    @BindView(R.id.workplace_layout)        TextInputLayout workplaceLayout;
    @BindView(R.id.password)                EditText        passwordView;
    @BindView(R.id.password_layout)         TextInputLayout passwordLayout;
    @BindView(R.id.confirm_password)        EditText        confirmPasswordView;
    @BindView(R.id.confirm_password_layout) TextInputLayout confirmPasswordLayout;
    @BindView(R.id.gender_male)             RadioButton     maleGenderView;
    @BindView(R.id.gender_female)           RadioButton     femaleGenderView;
    @BindView(R.id.btn_submit)              Button          submitBtn;

    private AuthConnector connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        connector = new AuthConnector(this);
    }

    @OnClick(R.id.btn_submit)
    void onSubmitClick() {
        hideSoftKeyboard();
        if (formIsValid()) {
            String gender = "Unspecified";
            if (maleGenderView.isChecked()) {
                gender = maleGenderView.getText().toString();
            } else if (femaleGenderView.isChecked()) {
                gender = femaleGenderView.getText().toString();
            }
            RegRequest request = new RegRequest();
            request.setUsername(usernameView.getText().toString());
            request.setEmail(emailView.getText().toString());
            request.setName(WordUtils.capitalize(nameView.getText().toString()));
            request.setGender(gender);
            request.setWorkplace(workplaceView.getText().toString());
            request.setPassword(passwordView.getText().toString());

            submitBtn.setClickable(false);
            submitBtn.setAlpha(0.5f);
            submitBtn.setText(R.string.btn_submitting);
            connector.register(request);
        }
    }

    private boolean formIsValid() {
        clearErrors();
        boolean isValid = true;
        if (AppUtils.isEmpty(usernameView)) {
            usernameLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (!AppUtils.isValidUsername(usernameView)) {
            usernameLayout.setError(AppUtils.getString(R.string.error_invalid_username));
            isValid = false;
        } else if (usernameView.getText().toString().length() < 5) {
            usernameLayout.setError(AppUtils.getString(R.string.error_short_username));
            isValid = false;
        }
        if (AppUtils.isEmpty(emailView)) {
            emailLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (!AppUtils.isValidEmail(emailView.getText().toString())) {
            emailLayout.setError(AppUtils.getString(R.string.error_invalid_email));
            isValid = false;
        }
        if (AppUtils.isEmpty(nameView)) {
            nameLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        }
        if (!maleGenderView.isChecked() && !femaleGenderView.isChecked()) {
            AppUtils.toast(R.string.error_gender);
        }
        if (AppUtils.isEmpty(workplaceView)) {
            workplaceLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        }
        if (AppUtils.isEmpty(passwordView)) {
            passwordLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (passwordView.getText().toString().length() < 6) {
            passwordLayout.setError(AppUtils.getString(R.string.error_short_password));
            isValid = false;
        }
        if (!passwordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
            confirmPasswordLayout.setError(AppUtils.getString(R.string.error_password_mismatch));
            isValid = false;
        }
        return isValid;
    }

    private void clearErrors() {
        usernameLayout.setError(null);
        emailLayout.setError(null);
        nameLayout.setError(null);
        workplaceLayout.setError(null);
        passwordLayout.setError(null);
        confirmPasswordLayout.setError(null);
    }

    @Override
    public void onCodeRetrieved(String emailVerificationCode) {
        String      message = "Thank you for registering in Kothay! Your email verification code is " + emailVerificationCode;
        EmailSender es      = new EmailSender(this, emailView.getText().toString(), "Verify Email", message, emailVerificationCode);
        es.execute();
        submitBtn.setClickable(true);
        submitBtn.setAlpha(1f);
        submitBtn.setText(R.string.btn_submit);
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onEmailSent(String emailVerificationCode) {
        Intent intent = new Intent(this, VerificationActivity.class);
        intent.putExtra(Const.EMAIL, emailView.getText().toString());
        intent.putExtra(Const.VERIFICATION_CODE, emailVerificationCode);
        openPage(intent);
    }

    @Override
    public void onCodeGenerated(String verificationCode) {

    }
}
