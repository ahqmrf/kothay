package apps.ahqmrf.kothay.auth.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/25/2017.
 */

public class ResetPasswordRequest {
    @SerializedName("email") private String email;
    @SerializedName("password") private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
