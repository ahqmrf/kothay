package apps.ahqmrf.kothay.auth.ui;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.util.AppUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity {

    @BindView(R.id.password)                EditText        passwordView;
    @BindView(R.id.password_layout)         TextInputLayout passwordLayout;
    @BindView(R.id.new_password)            EditText        newPasswordView;
    @BindView(R.id.new_password_layout)     TextInputLayout newPasswordLayout;
    @BindView(R.id.confirm_password)        EditText        confirmPasswordView;
    @BindView(R.id.confirm_password_layout) TextInputLayout confirmPasswordLayout;
    @BindView(R.id.btn_submit)              Button          submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
    }

    @OnClick(R.id.btn_submit)
    void onSubmitClick() {
        hideSoftKeyboard();
        if (formIsValid()) {
            submitBtn.setClickable(false);
            submitBtn.setAlpha(0.5f);
            submitBtn.setText(R.string.btn_changing_password);
            new AuthConnector(this).changePassword(
                    self.getEmail(),
                    passwordView.getText().toString(),
                    newPasswordView.getText().toString()
            );
        }
    }

    private boolean formIsValid() {
        clearErrors();
        boolean isValid = true;

        if (AppUtils.isEmpty(passwordView)) {
            passwordLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        }

        if (AppUtils.isEmpty(newPasswordView)) {
            newPasswordLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (newPasswordView.getText().toString().length() < 6) {
            newPasswordLayout.setError(AppUtils.getString(R.string.error_short_password));
            isValid = false;
        }
        if (!newPasswordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
            confirmPasswordLayout.setError(AppUtils.getString(R.string.error_password_mismatch));
            isValid = false;
        }
        return isValid;
    }

    private void clearErrors() {
        newPasswordLayout.setError(null);
        passwordLayout.setError(null);
        confirmPasswordLayout.setError(null);
    }

    @Override
    public void onSuccess(String message) {
        super.onSuccess(message);
        passwordView.setText(null);
        newPasswordView.setText(null);
        confirmPasswordView.setText(null);
        submitBtn.setClickable(true);
        submitBtn.setAlpha(1f);
        submitBtn.setText(R.string.btn_submit);
    }

    @Override public void onFailure(String message) {
        super.onFailure(message);
        submitBtn.setClickable(true);
        submitBtn.setAlpha(1f);
        submitBtn.setText(R.string.btn_submit);
    }
}
