package apps.ahqmrf.kothay.auth.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import apps.ahqmrf.kothay.AuthActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.callbacks.LoginListener;
import apps.ahqmrf.kothay.auth.connector.AuthConnector;
import apps.ahqmrf.kothay.auth.request.LoginRequest;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.home.ui.HomeActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AuthActivity implements LoginListener {

    @BindView(R.id.email)           EditText        emailView;
    @BindView(R.id.email_layout)    TextInputLayout emailLayout;
    @BindView(R.id.password)        EditText        passwordView;
    @BindView(R.id.password_layout) TextInputLayout passwordLayout;
    @BindView(R.id.checkbox)        CheckBox        rememberView;
    @BindView(R.id.btn_login)       Button          loginBtn;

    private AuthConnector       connector;
    private AlertDialog.Builder mDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        connector = new AuthConnector(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onViewCreated();
    }

    public void onViewCreated() {
        String email = getIntent().getStringExtra(Const.EMAIL);
        if (email == null) email = AppUtils.getCachedEmail();
        if (email != null) emailView.setText(email);

        DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        finishAffinity();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", mDialogClickListener)
                .setNegativeButton("No", mDialogClickListener);
    }

    @OnClick(R.id.layout_remember_me)
    void onRememberMeClick() {
        rememberView.toggle();
    }

    @OnClick(R.id.btn_login)
    void login() {
        hideSoftKeyboard();
        if (formIsValid()) {
            loginBtn.setText(R.string.btn_logging_in);
            loginBtn.setAlpha(0.5f);
            loginBtn.setClickable(false);
            LoginRequest request = new LoginRequest();
            request.setEmail(emailView.getText().toString());
            request.setPassword(passwordView.getText().toString());
            connector.login(request);
        }
    }

    private boolean formIsValid() {
        clearErrors();
        boolean isValid = true;

        if (AppUtils.isEmpty(emailView)) {
            emailLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        } else if (!AppUtils.isValidEmail(emailView.getText().toString()) && !AppUtils.isValidUsername(emailView)) {
            emailLayout.setError(AppUtils.getString(R.string.error_invalid_email_or_username));
            isValid = false;
        }

        if (AppUtils.isEmpty(passwordView)) {
            passwordLayout.setError(AppUtils.getString(R.string.error_field_required));
            isValid = false;
        }

        return isValid;
    }

    private void clearErrors() {
        emailLayout.setError(null);
        passwordLayout.setError(null);
    }

    @OnClick(R.id.btn_register)
    void openRegisterActivity() {
        openPage(RegisterActivity.class);
    }

    @OnClick(R.id.forgot_password)
    void openForgotPasswordActivity() {
        openPage(ForgotPasswordActivity.class);
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
        loginBtn.setText(R.string.btn_login);
        loginBtn.setAlpha(1f);
        loginBtn.setClickable(true);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onLoginSuccess(LoginResponse response) {
        if (rememberView.isChecked()) AppUtils.cacheUserInfo(response);
        AppUtils.cacheEmail(emailView.getText().toString());
        loginBtn.setText(R.string.btn_login);
        loginBtn.setAlpha(1f);
        loginBtn.setClickable(true);
        openPage(HomeActivity.class);
        finish();
    }

    @Override
    public void onActivationRequired(LoginResponse response) {
        Intent intent = new Intent(this, VerificationActivity.class);
        intent.putExtra(Const.EMAIL, response.getEmail());
        intent.putExtra(Const.VERIFICATION_CODE, response.getConfirmationCode());
        openPage(intent);
    }
}
