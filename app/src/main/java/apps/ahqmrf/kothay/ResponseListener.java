package apps.ahqmrf.kothay;

/**
 * Created by maruf on 8/19/2017.
 */

public interface ResponseListener {

    void hideLoader();

    void showLoader();

    void onFailure(String message);

    void onSuccess(String message);
}
