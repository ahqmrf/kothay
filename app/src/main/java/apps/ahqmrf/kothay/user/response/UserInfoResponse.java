package apps.ahqmrf.kothay.user.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;

/**
 * Created by maruf on 10/4/2017.
 */

public class UserInfoResponse extends LoginResponse implements Parcelable {
    @SerializedName("friendsCount") private     int     friendsCount;
    @SerializedName("checkinsCount") private    int     checkinsCount;
    @SerializedName("friendshipStatus") private String  friendshipStatus;
    @SerializedName("isFriend") private         boolean isFriend;

    public UserInfoResponse() {
        super();
    }

    protected UserInfoResponse(Parcel in) {
        super(in);
        friendsCount = in.readInt();
        checkinsCount = in.readInt();
        friendshipStatus = in.readString();
        isFriend = in.readByte() != 0;
    }

    public static final Creator<UserInfoResponse> CREATOR = new Creator<UserInfoResponse>() {
        @Override
        public UserInfoResponse createFromParcel(Parcel in) {
            return new UserInfoResponse(in);
        }

        @Override
        public UserInfoResponse[] newArray(int size) {
            return new UserInfoResponse[size];
        }
    };

    public int getFriendsCount() {
        return friendsCount;
    }

    public int getCheckinsCount() {
        return checkinsCount;
    }

    public String getFriendshipStatus() {
        return friendshipStatus;
    }

    public boolean isFriend() {
        return isFriend;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(friendsCount);
        parcel.writeInt(checkinsCount);
        parcel.writeString(friendshipStatus);
        parcel.writeByte((byte) (isFriend ? 1 : 0));
    }

    public void setFriendshipStatus(String friendshipStatus) {
        this.friendshipStatus = friendshipStatus;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public void setCheckinsCount(int checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public void setFriend(boolean friend) {
        isFriend = friend;
    }

    @Override public String toString() {
        return getName();
    }
}
