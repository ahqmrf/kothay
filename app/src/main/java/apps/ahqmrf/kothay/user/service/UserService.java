package apps.ahqmrf.kothay.user.service;

import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by maruf on 10/4/2017.
 */

public interface UserService {
    @POST("user_info.php") Call<UserInfoResponse> fetchUser(@Body UserInfoRequest request);
}
