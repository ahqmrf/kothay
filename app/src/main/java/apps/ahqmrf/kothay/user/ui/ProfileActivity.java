package apps.ahqmrf.kothay.user.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.friends.callback.FriendActivityListener;
import apps.ahqmrf.kothay.friends.connector.FriendActivityConnector;
import apps.ahqmrf.kothay.friends.ui.FriendProfileActivity;
import apps.ahqmrf.kothay.message.ui.ChatActivity;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity implements FriendActivityListener, UserListener {
    UserInfoResponse user;

    @BindView(R.id.full_name)       TextView   fullNameView;
    @BindView(R.id.bio)             TextView   bioView;
    @BindView(R.id.email)           TextView   emailView;
    @BindView(R.id.gender)          TextView   genderView;
    @BindView(R.id.friends_status)  TextView   friendStatusView;
    @BindView(R.id.workplace)       TextView   workplaceView;
    @BindView(R.id.profile_picture) ImageView  profilePictureView;
    @BindView(R.id.scrollView)      ScrollView rootLayout;
    @BindView(R.id.action_icon)     ImageView  actionIconView;
    @BindView(R.id.action_text)     TextView   actionTextView;

    private FriendActivityConnector connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        connector = new FriendActivityConnector(this);
    }

    @Override public void onViewCreated() {
        showBackArrow();
        if (user != null) {
            setLabel(user.getUsername());
            if (!TextUtils.isEmpty(user.getImageUrl())) {
                ImageLoader.getInstance().displayImage(user.getImageUrl(), profilePictureView, displayImageOptions);
            } else {
                if (user.getGender().equals(AppUtils.getString(R.string.label_female))) {
                    profilePictureView.setImageResource(R.drawable.woman_placeholder);
                } else {
                    profilePictureView.setImageResource(R.drawable.man_placeholder);
                }
            }
            fullNameView.setText(user.getName());
            if (TextUtils.isEmpty(user.getBio())) {
                bioView.setText(Const.DEFAULT_BIO);
            } else bioView.setText(user.getBio());
            genderView.setText(user.getGender());
            emailView.setText(user.getEmail());
            workplaceView.setText(user.getWorkplace());
            String text;
            if (user.getFriendshipStatus().equals(Const.CANCEL_REQUEST)) {
                text = "You have sent a friend request to " + user.getName();
                actionIconView.setImageResource(R.drawable.ic_cancel);
            } else {
                text = "You and " + user.getName() + " are not connected! Be friends to stay close!";
                actionIconView.setImageResource(R.drawable.ic_person_add);
            }
            actionTextView.setText(user.getFriendshipStatus());
            friendStatusView.setText(text);
            rootLayout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.action_layout) void onActionClick() {
        if (user.getFriendshipStatus().equals(Const.CANCEL_REQUEST)) {
            connector.controlFriendship(self.getId(), user.getId(), Const.CANCEL_REQUEST);
        } else if (user.getFriendshipStatus().equals(Const.ADD_AS_FRIEND)) {
            connector.controlFriendship(self.getId(), user.getId(), Const.ADD_AS_FRIEND);
        } else if (user.getFriendshipStatus().equals(Const.ACCEPT_REQUEST)) {
            connector.controlFriendship(self.getId(), user.getId(), Const.ACCEPT_REQUEST);
        }
    }

    @OnClick(R.id.message) void onMessageClick() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @Override public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override public void onRequestAccepted() {
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(self.getId());
        request.setOtherUserId(user.getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override public void onRequestSent() {
        user.setFriendshipStatus(Const.CANCEL_REQUEST);
        actionIconView.setImageResource(R.drawable.ic_cancel);
        actionTextView.setText(Const.CANCEL_REQUEST);
    }

    @Override public void onRequestCancelled() {
        user.setFriendshipStatus(Const.ADD_AS_FRIEND);
        actionIconView.setImageResource(R.drawable.ic_person_add);
        actionTextView.setText(Const.ADD_AS_FRIEND);
    }

    @Override public void onUserFetched(UserInfoResponse response) {
        Intent intent = new Intent(this, FriendProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
        finish();
    }
}
