package apps.ahqmrf.kothay.user.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maruf on 10/4/2017.
 */

public class UserInfoRequest {
    @SerializedName("userId") private long userId;
    @SerializedName("otherUserId") private long otherUserId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(long otherUserId) {
        this.otherUserId = otherUserId;
    }
}
