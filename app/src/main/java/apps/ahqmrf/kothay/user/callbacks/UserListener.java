package apps.ahqmrf.kothay.user.callbacks;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by maruf on 10/4/2017.
 */

public interface UserListener extends ResponseListener {
    void onUserFetched(UserInfoResponse response);
}
