package apps.ahqmrf.kothay.user.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by maruf on 10/4/2017.
 */

public class UserClient extends Client {
    @Override public UserService createService() {
        return getRetrofit().create(UserService.class);
    }
}
