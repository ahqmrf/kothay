package apps.ahqmrf.kothay.user.connector;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.service.UserClient;
import apps.ahqmrf.kothay.user.service.UserService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by maruf on 10/4/2017.
 */

public class UserConnector {
    private ResponseListener listener;
    private UserService      service;

    public UserConnector(ResponseListener listener) {
        this.listener = listener;
        service = new UserClient().createService();
    }

    public void fetchUser(UserInfoRequest request) {
        if(listener != null) {
            listener.showLoader();
            Call<UserInfoResponse> call = service.fetchUser(request);
            call.enqueue(new ServiceCallback<UserInfoResponse>(listener) {
                @Override public void onResponse(Response<UserInfoResponse> response) {
                    UserInfoResponse body = response.body();
                    if(body != null) {
                        ((UserListener)listener).onUserFetched(body);
                    }
                }
            });
        }
    }
}
