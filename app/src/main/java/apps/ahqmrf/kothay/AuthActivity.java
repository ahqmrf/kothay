package apps.ahqmrf.kothay;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.rey.material.widget.ProgressView;

import java.util.Random;

import apps.ahqmrf.kothay.util.AppUtils;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by bsse0 on 10/27/2017.
 */

public class AuthActivity extends AppCompatActivity implements ResponseListener {

    public AlertDialog.Builder mDialogBuilder;

    @Nullable @BindView(R.id.progress_layout) View         progressLayout;
    @Nullable @BindView(R.id.progress_view)   ProgressView progressView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        moveTaskToBack(true);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        mDialogBuilder = new AlertDialog.Builder(this);
        mDialogBuilder.setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", mDialogClickListener)
                .setNegativeButton("No", mDialogClickListener);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    @Override
    public void showLoader() {
        if (progressLayout != null && progressView != null) {
            progressLayout.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void hideLoader() {
        if (progressLayout != null && progressView != null) {
            progressView.setVisibility(View.GONE);
            progressLayout.setVisibility(View.GONE);
        }
    }

    public void openPage(Class<?> activity) {
        Intent intent = new Intent(this, activity);
        openPage(intent);
    }

    public void openPage(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void exitApp() {
        mDialogBuilder.show();
    }

    @Optional
    @OnClick(R.id.ic_back)
    void onBackClick() {
        onBackPressed();
    }
}
