package apps.ahqmrf.kothay.settings.ui;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;

import com.binjar.prefsdroid.Preference;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.ui.ChangePasswordActivity;
import apps.ahqmrf.kothay.settings.callback.SettingsUpdateListener;
import apps.ahqmrf.kothay.settings.request.SettingsUpdateConnector;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity implements SwitchCompat.OnCheckedChangeListener, SettingsUpdateListener {

    @BindView(R.id.scrollView)                View         rootLayout;
    @BindView(R.id.alarmNotificationSwitch)   SwitchCompat alarmNotificationSwitch;
    @BindView(R.id.alarmSoundSwitch)          SwitchCompat alarmSoundSwitch;
    @BindView(R.id.alarmVibrationSwitch)      SwitchCompat alarmVibrationSwitch;
    @BindView(R.id.messageNotificationSwitch) SwitchCompat messageNotificationSwitch;
    @BindView(R.id.messageVibrationSwitch)    SwitchCompat messageVibrationSwitch;
    @BindView(R.id.shareLocationSwitch)       SwitchCompat shareLocationSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        setLabel(R.string.title_settings);
        showBackArrow();

        bindViews();

        alarmNotificationSwitch.setOnCheckedChangeListener(this);
        alarmSoundSwitch.setOnCheckedChangeListener(this);
        alarmVibrationSwitch.setOnCheckedChangeListener(this);
        messageNotificationSwitch.setOnCheckedChangeListener(this);
        messageVibrationSwitch.setOnCheckedChangeListener(this);
        shareLocationSwitch.setOnCheckedChangeListener(this);
    }

    private void bindViews() {
        alarmNotificationSwitch.setChecked(AppUtils.isAlarmNotificationEnabled());
        if (AppUtils.isAlarmNotificationEnabled()) {
            alarmSoundSwitch.setClickable(true);
            alarmVibrationSwitch.setClickable(true);
            alarmSoundSwitch.setChecked(AppUtils.isAlarmSoundEnabled());
            alarmVibrationSwitch.setChecked(AppUtils.isAlarmVibrationEnabled());
        } else {
            alarmSoundSwitch.setClickable(false);
            alarmVibrationSwitch.setClickable(false);
            alarmSoundSwitch.setChecked(false);
            alarmVibrationSwitch.setChecked(false);
        }
        messageNotificationSwitch.setChecked(AppUtils.isMessageNotificationEnabled());
        if (AppUtils.isMessageNotificationEnabled()) {
            messageVibrationSwitch.setClickable(true);
            messageVibrationSwitch.setChecked(AppUtils.isMessageVibrationEnabled());
        } else {
            messageVibrationSwitch.setClickable(false);
            messageVibrationSwitch.setChecked(false);
        }
        shareLocationSwitch.setChecked(self.sharesLocation());
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(!compoundButton.isPressed()) return;
        int id = compoundButton.getId();
        if (id == R.id.alarmNotificationSwitch) {
            AppUtils.setAlarmNotificationEnabled(b);
            if (!b) {
                AppUtils.setAlarmSoundEnabled(false);
                AppUtils.setAlarmVibrationEnabled(false);
            }
        } else if (id == R.id.alarmSoundSwitch) {
            AppUtils.setAlarmSoundEnabled(b);
        } else if (id == R.id.alarmVibrationSwitch) {
            AppUtils.setAlarmVibrationEnabled(b);
        } else if (id == R.id.messageNotificationSwitch) {
            AppUtils.setMessageNotificationEnabled(b);
            if (!b) {
                AppUtils.setMessageVibrationEnabled(false);
            }
        } else if (id == R.id.messageVibrationSwitch) {
            AppUtils.setMessageVibrationEnabled(b);
        } else if (id == R.id.shareLocationSwitch) {
            new SettingsUpdateConnector(this).changeShareLocation(self.getId(), b);
        }

        bindViews();
    }

    @OnClick(R.id.layoutChangePassword)
    void onChangePasswordClick() {
        openPage(ChangePasswordActivity.class);
    }

    @Override
    public void onLocationShareChange(boolean sharesLocation) {
        shareLocationSwitch.setChecked(sharesLocation);
        self.setSharesLocation(sharesLocation);
        Preference.remove(Const.USER_INFO);
        Preference.putObject(Const.USER_INFO, self);
    }

    @Override
    public void onFailure(String message) {
        super.onFailure(message);
        shareLocationSwitch.setChecked(self.sharesLocation());
    }
}
