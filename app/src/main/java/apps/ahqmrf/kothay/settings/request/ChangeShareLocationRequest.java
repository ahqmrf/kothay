package apps.ahqmrf.kothay.settings.request;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseRequest;

/**
 * Created by bsse0 on 11/27/2017.
 */

public class ChangeShareLocationRequest extends BaseRequest {
    @SerializedName("sharesLocation") private boolean sharesLocation;

    public boolean isSharesLocation() {
        return sharesLocation;
    }

    public void setSharesLocation(boolean sharesLocation) {
        this.sharesLocation = sharesLocation;
    }
}
