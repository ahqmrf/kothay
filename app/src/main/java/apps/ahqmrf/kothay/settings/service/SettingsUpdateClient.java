package apps.ahqmrf.kothay.settings.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 11/27/2017.
 */

public class SettingsUpdateClient extends Client<SettingsUpdateService> {
    @Override
    public SettingsUpdateService createService() {
        return getRetrofit().create(SettingsUpdateService.class);
    }
}
