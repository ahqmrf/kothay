package apps.ahqmrf.kothay.settings.response;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by bsse0 on 11/27/2017.
 */

public class ChangeLocationShareResponse extends BaseResponse {
    @SerializedName("sharesLocation") private boolean sharesLocation;

    public boolean sharesLocation() {
        return sharesLocation;
    }

    public void setSharesLocation(boolean sharesLocation) {
        this.sharesLocation = sharesLocation;
    }
}
