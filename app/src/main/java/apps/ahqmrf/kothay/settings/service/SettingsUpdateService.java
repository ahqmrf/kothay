package apps.ahqmrf.kothay.settings.service;

import apps.ahqmrf.kothay.settings.response.ChangeLocationShareResponse;
import apps.ahqmrf.kothay.settings.request.ChangeShareLocationRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 11/27/2017.
 */

public interface SettingsUpdateService {
    @POST("change_location_share.php")
    Call<ChangeLocationShareResponse> changeLocationShareSettings(@Body ChangeShareLocationRequest request);
}
