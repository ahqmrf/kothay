package apps.ahqmrf.kothay.settings.callback;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by bsse0 on 11/27/2017.
 */

public interface SettingsUpdateListener extends ResponseListener{
    void onLocationShareChange(boolean sharesLocation);
}
