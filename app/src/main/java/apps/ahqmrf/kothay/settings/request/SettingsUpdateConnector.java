package apps.ahqmrf.kothay.settings.request;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.settings.response.ChangeLocationShareResponse;
import apps.ahqmrf.kothay.settings.callback.SettingsUpdateListener;
import apps.ahqmrf.kothay.settings.service.SettingsUpdateClient;
import apps.ahqmrf.kothay.settings.service.SettingsUpdateService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 11/27/2017.
 */

public class SettingsUpdateConnector {
    private ResponseListener      listener;
    private SettingsUpdateService service;

    public SettingsUpdateConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = new SettingsUpdateClient().createService();
    }

    public void changeShareLocation(long id, boolean sharesLocation) {
        if (listener != null) {
            listener.showLoader();
            ChangeShareLocationRequest request = new ChangeShareLocationRequest();
            request.setUserId(id);
            request.setSharesLocation(sharesLocation);
            Call<ChangeLocationShareResponse> call = service.changeLocationShareSettings(request);
            call.enqueue(new ServiceCallback<ChangeLocationShareResponse>(listener) {
                @Override
                public void onResponse(Response<ChangeLocationShareResponse> response) {
                    ChangeLocationShareResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                        ((SettingsUpdateListener) listener).onLocationShareChange(body.sharesLocation());
                    }
                }
            });
        }
    }
}
