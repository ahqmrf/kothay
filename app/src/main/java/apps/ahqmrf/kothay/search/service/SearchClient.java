package apps.ahqmrf.kothay.search.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 8/31/2017.
 */

public class SearchClient extends Client<SearchService> {
    @Override public SearchService createService() {
        return getRetrofit().create(SearchService.class);
    }
}
