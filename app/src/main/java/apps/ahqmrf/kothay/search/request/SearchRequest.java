package apps.ahqmrf.kothay.search.request;

/**
 * Created by bsse0 on 8/31/2017.
 */

public class SearchRequest {

    private String key;

    public SearchRequest(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
