package apps.ahqmrf.kothay.search.connector;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.search.callbacks.SearchListener;
import apps.ahqmrf.kothay.search.model.SearchItem;
import apps.ahqmrf.kothay.search.request.SearchRequest;
import apps.ahqmrf.kothay.search.response.SearchResponse;
import apps.ahqmrf.kothay.search.service.SearchClient;
import apps.ahqmrf.kothay.search.service.SearchService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 8/31/2017.
 */

public class SearchConnector {
    private ResponseListener listener;
    private SearchService    service;

    public SearchConnector(ResponseListener listener) {
        this.listener = listener;
        service = new SearchClient().createService();
    }

    public void search(String key) {
        if (listener != null) {
            listener.showLoader();
            SearchRequest request = new SearchRequest(key);
            Call<SearchResponse> call = service.search(request);
            call.enqueue(new ServiceCallback<SearchResponse>(listener) {
                @Override public void onResponse(Response<SearchResponse> response) {
                    SearchResponse body = response.body();
                    if (body != null) {
                        List<SearchItem> results = body.getResults();
                        if (results == null || results.isEmpty()) {
                            results = new ArrayList<>();
                            listener.onFailure(body.getError());
                        }
                        ((SearchListener) listener).onSearchCompleted(results);
                    }
                }
            });
        }
    }
}
