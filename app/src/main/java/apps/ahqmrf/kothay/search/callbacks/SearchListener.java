package apps.ahqmrf.kothay.search.callbacks;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.search.model.SearchItem;

/**
 * Created by bsse0 on 8/31/2017.
 */

public interface SearchListener extends ResponseListener {

    void onSearchCompleted(List<SearchItem> results);
}
