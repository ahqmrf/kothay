package apps.ahqmrf.kothay.search.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 10/4/2017.
 */

public class SearchItem {
    @SerializedName("id") private long id;
    @SerializedName("username") private String username;
    @SerializedName("name") private String name;
    @SerializedName("imageUrl") private String imageUrl;
    @SerializedName("workplace") private String workplace;
    @SerializedName("gender") private String gender;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getWorkplace() {
        return workplace;
    }

    public String getGender() {
        return gender;
    }
}
