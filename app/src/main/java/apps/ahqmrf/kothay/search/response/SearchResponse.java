package apps.ahqmrf.kothay.search.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.search.model.SearchItem;

/**
 * Created by bsse0 on 8/31/2017.
 */

public class SearchResponse extends BaseResponse {

    @SerializedName("key") private String              key;
    @SerializedName("result") private List<SearchItem> results;

    public String getKey() {
        return key;
    }

    public List<SearchItem> getResults() {
        return results;
    }
}
