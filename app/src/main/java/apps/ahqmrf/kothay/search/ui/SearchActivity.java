package apps.ahqmrf.kothay.search.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.binjar.prefsdroid.Preference;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.account.ui.AccountActivity;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.friends.ui.FriendProfileActivity;
import apps.ahqmrf.kothay.search.connector.SearchConnector;
import apps.ahqmrf.kothay.search.callbacks.SearchListener;
import apps.ahqmrf.kothay.search.model.SearchItem;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.ui.ProfileActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity implements SearchListener, UserListener, OnItemClickCallback {

    @BindView(R.id.input_search_key) EditText     searchInputView;
    @BindView(R.id.search_list)      RecyclerView searchResultListView;

    private SearchConnector   connector;
    private SearchListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        ButterKnife.bind(this);
        connector = new SearchConnector(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        searchInputView.requestFocus();
        searchInputView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearchClick();
                    return true;
                }
                return false;
            }
        });
        searchResultListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new SearchListAdapter(new ArrayList<SearchItem>(), this);
        searchResultListView.setAdapter(adapter);
    }

    @OnClick(R.id.btn_search)
    void onSearchClick() {
        hideSoftKeyboard();
        String key = searchInputView.getText().toString();
        if (!TextUtils.isEmpty(key)) {
            connector.search(key);
        }
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSearchCompleted(List<SearchItem> results) {
        adapter.filter(results);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Object item) {
        SearchItem user   = (SearchItem) item;
        long       userId = Preference.getObject(PrefKeys.USER_INFO, LoginResponse.class).getId();
        if (userId == user.getId()) {
            openPage(AccountActivity.class);
            return;
        }
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(userId);
        request.setOtherUserId(user.getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override
    public void onUserFetched(UserInfoResponse response) {
        Intent intent;
        if (response.isFriend()) {
            intent = new Intent(this, FriendProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
    }
}
