package apps.ahqmrf.kothay.search.service;

import apps.ahqmrf.kothay.search.request.SearchRequest;
import apps.ahqmrf.kothay.search.response.SearchResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 8/31/2017.
 */

public interface SearchService {
    @POST("search.php") Call<SearchResponse> search(@Body SearchRequest request);
}
