package apps.ahqmrf.kothay.search.ui;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.search.model.SearchItem;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 8/31/2017.
 */

public class SearchListAdapter extends RecyclerView.Adapter {

    private List<SearchItem>    items;
    private OnItemClickCallback callback;

    public SearchListAdapter(List<SearchItem> items, OnItemClickCallback callback) {
        filter(items);
        this.callback = callback;
    }

    public void filter(List<SearchItem> items) {
        this.items = new ArrayList<>();
        this.items.addAll(items);
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result, parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SearchResultViewHolder)holder).bindTo(items.get(position));
    }

    @Override public int getItemCount() {
        return items.size();
    }

    class SearchResultViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name) TextView nameView;
        @BindView(R.id.workplace) TextView workplaceView;
        @BindView(R.id.image) ImageView imageView;

        public SearchResultViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_search_result_layout) void onItemClick() {
            if(callback != null) callback.onItemClick(items.get(getAdapterPosition()));
        }

        void bindTo(SearchItem item) {
            nameView.setText(item.getName());
            workplaceView.setText(item.getWorkplace());
            int placeholder = R.drawable.man_placeholder;
            if (item.getGender().equals(Const.FEMALE)) placeholder = R.drawable.woman_placeholder;
            ImageLoader.getInstance().displayImage(
                    item.getImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(placeholder)
            );
        }
    }
}
