package apps.ahqmrf.kothay.notification.ui;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.notification.response.MyNotification;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 11/21/2017.
 */

public class NotificationListAdapter extends RecyclerView.Adapter {

    private ArrayList<MyNotification> items;
    private OnItemClickCallback       callback;

    public NotificationListAdapter(ArrayList<MyNotification> items, OnItemClickCallback callback) {
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_friend_request_accept, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NotificationViewHolder) holder).bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification) TextView  notificationView;
        @BindView(R.id.time)         TextView  timeView;
        @BindView(R.id.layout)       View      layout;
        @BindView(R.id.icon)         ImageView iconView;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(MyNotification item) {
            String first = "<b>" + item.getFrom().getName() + "</b>";
            if (item.getType().equals(Const.REQUEST_ACCEPT)) {
                String next = " accepted your friend request.";
                notificationView.setText(Html.fromHtml(first + next));
                iconView.setImageResource(R.drawable.ic_friendship);
            } else if (item.getType().equals(Const.ALARM)) {
                String next = " has set an alarm after you.";
                notificationView.setText(Html.fromHtml(first + next));
                iconView.setImageResource(R.drawable.ic_notification_alarm);
            } else if (item.getType().equals(Const.FRIEND_REQUEST)) {
                String next = " has sent you a friend request. Click here to visit profile.";
                notificationView.setText(Html.fromHtml(first + next));
                iconView.setImageResource(R.drawable.ic_request);
            } else if (item.getType().equals("Location")) {
                String next = " has shared a location with you. Click here to view.";
                notificationView.setText(Html.fromHtml(first + next));
                iconView.setImageResource(R.drawable.ic_share_location);
            }
            AppUtils.TimeModel model = new AppUtils.TimeModel(item.getTime());
            timeView.setText(model.getDateTime());
            if (item.isSeen()) {
                layout.setBackgroundColor(AppUtils.getColor(R.color.bg));
            } else {
                layout.setBackgroundColor(AppUtils.getColor(R.color.darkBg));
            }
        }

        @OnClick(R.id.notification_layout)
        void onNotificationClick() {
            if (callback != null) {
                callback.onItemClick(
                        items.get(getAdapterPosition())
                );
            }
        }
    }
}
