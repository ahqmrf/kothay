package apps.ahqmrf.kothay.notification.service;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.meeting.request.NotifyRequest;
import apps.ahqmrf.kothay.notification.response.GetNotificationsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 11/21/2017.
 */

public interface NotificationService {
    @POST("get_notifications.php")
    Call<GetNotificationsResponse> getNotifications(@Body BaseRequest request);

    @POST("make_notification_seen.php")
    Call<BaseResponse> makeNotificationSeen(@Body BaseRequest request);

    @POST("notify_place.php")
    Call<BaseResponse> notifyMembers(@Body NotifyRequest request);
}
