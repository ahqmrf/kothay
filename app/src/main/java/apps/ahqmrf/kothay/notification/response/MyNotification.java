package apps.ahqmrf.kothay.notification.response;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 11/21/2017.
 */

public class MyNotification {
    @SerializedName("id") private      long             id;
    @SerializedName("from") private    UserInfoResponse from;
    @SerializedName("type") private    String           type;
    @SerializedName("content") private String           content;
    @SerializedName("isSeen") private  boolean          seen;
    @SerializedName("time") private    String           time;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserInfoResponse getFrom() {
        return from;
    }

    public void setFrom(UserInfoResponse from) {
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public Date getTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
