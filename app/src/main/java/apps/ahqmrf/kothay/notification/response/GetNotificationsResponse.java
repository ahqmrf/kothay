package apps.ahqmrf.kothay.notification.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by bsse0 on 11/21/2017.
 */

public class GetNotificationsResponse extends BaseResponse {
    @SerializedName("notifications") private List<MyNotification> notifications;
    @SerializedName("unseen") private int unseen;

    public List<MyNotification> getNotifications() {
        return notifications;
    }

    public int getUnseen() {
        return unseen;
    }
}
