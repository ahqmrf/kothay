package apps.ahqmrf.kothay.notification.callback;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.notification.response.MyNotification;

/**
 * Created by bsse0 on 11/21/2017.
 */

public interface NotificationResponseListener extends ResponseListener {
    void onNotificationFetched(List<MyNotification> notifications);

    void onUnseenNotificationCountFetched(int count);

    void onIncomingMessage(ChatMessage message);
}
