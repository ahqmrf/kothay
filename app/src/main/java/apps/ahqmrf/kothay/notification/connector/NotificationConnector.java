package apps.ahqmrf.kothay.notification.connector;


import com.binjar.prefsdroid.Preference;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.notification.callback.NotificationResponseListener;
import apps.ahqmrf.kothay.notification.response.GetNotificationsResponse;
import apps.ahqmrf.kothay.notification.service.NotificationClient;
import apps.ahqmrf.kothay.notification.service.NotificationService;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 11/21/2017.
 */

public class NotificationConnector {
    private NotificationService          service;
    private NotificationResponseListener listener;
    private DatabaseReference            ref;
    private ValueEventListener incomingMessageListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot != null) {
                        ChatMessage message = snapshot.getValue(ChatMessage.class);
                        LoginResponse self = Preference.getObject(Const.USER_INFO, LoginResponse.class);
                        if (listener != null && self != null && !message.isSeen()) {
                            if (self.getUsername().equals(message.getTo())) {
                                listener.onIncomingMessage(message);
                                message.setDelivered(true);
                                snapshot.getRef().setValue(message);
                                FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION)
                                        .child(self.getUsername()).child(message.getFrom()).setValue(message);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public NotificationConnector(NotificationResponseListener listener) {
        this.listener = listener;
        this.service = new NotificationClient().createService();
        LoginResponse self = Preference.getObject(PrefKeys.USER_INFO, LoginResponse.class);
        if (self != null) {
            ref = FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION).child(self.getUsername());
        }
    }

    public void fetchNotifications(long userId) {
        if (listener != null) {
            BaseRequest request = new BaseRequest();
            request.setUserId(userId);

            Call<GetNotificationsResponse> call = service.getNotifications(request);
            call.enqueue(new ServiceCallback<GetNotificationsResponse>(listener) {
                @Override
                public void onResponse(Response<GetNotificationsResponse> response) {
                    GetNotificationsResponse body = response.body();
                    if (body != null) {
                        listener.onNotificationFetched(body.getNotifications());
                        listener.onUnseenNotificationCountFetched(body.getUnseen());
                    }
                }
            });
        }
    }

    public void makeNotificationSeen(long userId) {
        if (listener != null) {
            BaseRequest request = new BaseRequest();
            request.setUserId(userId);

            Call<BaseResponse> call = service.makeNotificationSeen(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override
                public void onResponse(Response<BaseResponse> response) {

                }
            });
        }
    }

    public void listenToIncomingMessages() {
        if (ref != null) ref.addValueEventListener(incomingMessageListener);
    }

    public void stopListening() {
        if (ref != null) ref.removeEventListener(incomingMessageListener);
    }
}
