package apps.ahqmrf.kothay.notification.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.binjar.prefsdroid.Preference;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.account.ui.AccountActivity;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.friends.ui.FriendProfileActivity;
import apps.ahqmrf.kothay.notification.callback.OnNotificationClickCallback;
import apps.ahqmrf.kothay.notification.connector.NotificationConnector;
import apps.ahqmrf.kothay.notification.response.MyNotification;
import apps.ahqmrf.kothay.search.model.SearchItem;
import apps.ahqmrf.kothay.tracker.ui.ViewLocationActivity;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.ui.ProfileActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.PrefKeys;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity implements OnItemClickCallback, UserListener {

    @BindView(R.id.notificationList) RecyclerView notificationListView;

    private NotificationListAdapter   adapter;
    private ArrayList<MyNotification> items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        setLabel(R.string.title_notifications);
        showBackArrow();
        items = new ArrayList<>();
        adapter = new NotificationListAdapter(items, this);
        notificationListView.setLayoutManager(new LinearLayoutManager(this));
        notificationListView.setAdapter(adapter);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showLoader();
    }

    @Override
    public void onNotificationFetched(List<MyNotification> notifications) {
        items.clear();
        items.addAll(notifications);
        adapter.notifyDataSetChanged();
        notificationConnector.makeNotificationSeen(self.getId());
        hideLoader();
    }

    @Override
    public void onItemClick(Object item) {
        MyNotification notification = (MyNotification) item;
        if (notification.getType().equals("Location")) {
            String tokens[] = notification.getContent().split(",");
            if (tokens.length == 3) {
                double lat = Double.parseDouble(tokens[0]);
                double lng = Double.parseDouble(tokens[1]);
                Intent intent = new Intent(this, ViewLocationActivity.class);
                intent.putExtra(Const.PLACE, tokens[2]);
                intent.putExtra(Const.LATITUDE, lat);
                intent.putExtra(Const.LONGITUDE, lng);
                openPage(intent);
                return;
            }
        }
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(self.getId());
        request.setOtherUserId(notification.getFrom().getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override
    public void onUserFetched(UserInfoResponse response) {
        Intent intent;
        if (response.isFriend()) {
            intent = new Intent(this, FriendProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
    }
}
