package apps.ahqmrf.kothay.notification.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 11/21/2017.
 */

public class NotificationClient extends Client<NotificationService> {
    @Override public NotificationService createService() {
        return getRetrofit().create(NotificationService.class);
    }
}
