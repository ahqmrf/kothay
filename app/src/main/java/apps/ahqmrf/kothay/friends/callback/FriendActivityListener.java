package apps.ahqmrf.kothay.friends.callback;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by maruf on 10/7/2017.
 */

public interface FriendActivityListener extends ResponseListener {
    void onRequestAccepted();

    void onRequestSent();

    void onRequestCancelled();
}
