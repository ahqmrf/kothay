package apps.ahqmrf.kothay.friends.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.auth.response.LoginResponse;

/**
 * Created by bsse0 on 10/21/2017.
 */

public class FriendListResponse extends BaseResponse {
    @SerializedName("friends") private List<LoginResponse> friends;

    public List<LoginResponse> getFriends() {
        return friends;
    }
}
