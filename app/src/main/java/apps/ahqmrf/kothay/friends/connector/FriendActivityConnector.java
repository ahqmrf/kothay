package apps.ahqmrf.kothay.friends.connector;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.friends.callback.PostsListener;
import apps.ahqmrf.kothay.friends.callback.FriendListRetriveListener;
import apps.ahqmrf.kothay.friends.callback.FriendActivityListener;
import apps.ahqmrf.kothay.friends.request.FriendshipControlRequest;
import apps.ahqmrf.kothay.friends.response.FriendListResponse;
import apps.ahqmrf.kothay.friends.response.FriendshipControlResponse;
import apps.ahqmrf.kothay.friends.service.FriendActivityClient;
import apps.ahqmrf.kothay.friends.service.FriendActivityService;
import apps.ahqmrf.kothay.home.response.StatusResponse;
import apps.ahqmrf.kothay.util.Const;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by maruf on 10/7/2017.
 */

public class FriendActivityConnector {
    private ResponseListener      listener;
    private FriendActivityService service;

    public FriendActivityConnector(ResponseListener listener) {
        this.listener = listener;
        service = new FriendActivityClient().createService();
    }

    public void controlFriendship(long userId, long otherUserId, String actionType) {
        if(listener != null) {
            listener.showLoader();
            FriendshipControlRequest request = new FriendshipControlRequest();
            request.setUserId(userId);
            request.setOtherUserId(otherUserId);
            request.setActionType(actionType);

            Call<FriendshipControlResponse> call = service.controlFriendship(request);
            call.enqueue(new ServiceCallback<FriendshipControlResponse>(listener) {
                @Override public void onResponse(Response<FriendshipControlResponse> response) {
                    FriendshipControlResponse body = response.body();
                    if(body != null) {
                        listener.onSuccess(body.getMessage());
                        FriendActivityListener controlListener = (FriendActivityListener)listener;
                        String                 type            = body.getActionType();
                        if(type.equals(Const.ACCEPT_REQUEST)) controlListener.onRequestAccepted();
                        else if(type.equals(Const.ADD_AS_FRIEND)) controlListener.onRequestSent();
                        else controlListener.onRequestCancelled();
                    }
                }
            });
        }
    }

    public void getFriendList(long userId) {
        if(listener != null) {
            FriendshipControlRequest request = new FriendshipControlRequest();
            request.setUserId(userId);
            Call<FriendListResponse> call = service.getFriends(request);
            call.enqueue(new ServiceCallback<FriendListResponse>(listener) {
                @Override public void onResponse(Response<FriendListResponse> response) {
                    FriendListResponse res = response.body();
                    if(res != null) {
                        ((FriendListRetriveListener) listener).onFriendListRetrieved(res.getFriends());
                    }
                }
            });
        }
    }

    public void getPosts(long userId) {
        BaseRequest request = new BaseRequest();
        request.setUserId(userId);
        Call<StatusResponse> call = service.getPosts(request);
        call.enqueue(new ServiceCallback<StatusResponse>(listener) {
            @Override public void onResponse(Response<StatusResponse> response) {
                StatusResponse res = response.body();
                if(res != null) {
                    ((PostsListener) listener).onPostsFetched(res.getPosts());
                }
            }
        });
    }
}
