package apps.ahqmrf.kothay.friends.ui;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.lang.WordUtils;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.meeting.model.FriendItemForGroup;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by bsse0 on 10/21/2017.
 */

public class FriendListAdapter extends RecyclerView.Adapter {

    private List<Object>        items;
    private OnItemClickCallback callback;
    private int                 layout;

    public FriendListAdapter(OnItemClickCallback callback, int layout) {
        items = new ArrayList<>();
        this.callback = callback;
        this.layout = layout;
    }

    public void filter(List<Object> list) {
        items = new ArrayList<>();
        items.addAll(list);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((FriendViewHolder) holder).bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class FriendViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)               TextView  nameView;
        @BindView(R.id.workplace)          TextView  workplaceView;
        @BindView(R.id.image)              ImageView imageView;
        @Nullable @BindView(R.id.checkbox) CheckBox  checkBox;

        public FriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Object object) {
            LoginResponse item = (LoginResponse) object;
            nameView.setText(WordUtils.capitalize(item.getName()));
            workplaceView.setText(item.getWorkplace());
            int placeholder = R.drawable.man_placeholder;
            if (item.getGender().equals(Const.FEMALE)) placeholder = R.drawable.woman_placeholder;
            ImageLoader.getInstance().displayImage(
                    item.getImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(placeholder)
            );
            if (checkBox != null) {
                checkBox.setChecked(((FriendItemForGroup) object).isSelected());
            }
        }

        @OnClick(R.id.friend_layout)
        void onFriendLayoutClick() {
            if (layout == R.layout.item_friend && callback != null) {
                callback.onItemClick(items.get(getAdapterPosition()));
            }
        }

        @Optional
        @OnCheckedChanged(R.id.checkbox)
        void onCheckBoxClick() {
            if (checkBox != null && checkBox.isPressed() && callback != null) {
                int position = getAdapterPosition();
                FriendItemForGroup item = (FriendItemForGroup) items.get(position);
                callback.onItemClick(item.getId());
                item.setSelected(!item.isSelected());
                items.set(position, item);
                notifyItemChanged(position);
            }
        }
    }
}
