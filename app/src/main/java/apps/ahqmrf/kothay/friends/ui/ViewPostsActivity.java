package apps.ahqmrf.kothay.friends.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.friends.callback.PostsListener;
import apps.ahqmrf.kothay.friends.connector.FriendActivityConnector;
import apps.ahqmrf.kothay.home.callback.StatusActionCallback;
import apps.ahqmrf.kothay.home.response.Status;
import apps.ahqmrf.kothay.home.ui.StatusListAdapter;
import apps.ahqmrf.kothay.tracker.ui.ViewLocationActivity;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewPostsActivity extends BaseActivity implements PostsListener, StatusActionCallback{

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.checkInsList)       RecyclerView       postsListView;

    private FriendActivityConnector connector;
    private StatusListAdapter       adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_posts);
        ButterKnife.bind(this);

        connector = new FriendActivityConnector(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        final UserInfoResponse user = getIntent().getParcelableExtra(Const.USER_INFO);
        if (user != null) {
            setLabel("Posts by " + user.getName());
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    connector.getPosts(user.getId());
                    fetchNotifications();
                }
            });

            adapter = new StatusListAdapter(new ArrayList<>(), this);
            postsListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            postsListView.setAdapter(adapter);
            showLoader();
            connector.getPosts(user.getId());
        } else {
            setLabel("Posts by " + self.getName());
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    connector.getPosts(self.getId());
                    fetchNotifications();
                }
            });

            adapter = new StatusListAdapter(new ArrayList<>(), this);
            postsListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            postsListView.setAdapter(adapter);
            showLoader();
            connector.getPosts(self.getId());
        }
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    @Override
    public void onPostsFetched(List<Status> posts) {
        if(posts == null || posts.isEmpty()) {
            AppUtils.toast("No check-ins to show");
        }
        ArrayList<Object> items = new ArrayList<>();
        items.addAll(posts);
        adapter.filter(items);
        swipeRefreshLayout.setRefreshing(false);
        postsListView.scrollToPosition(0);
        hideLoader();
    }

    @Override
    public void onItemClick(Object item) {

    }

    @Override
    public void onEditStatusClick(Status status) {

    }

    @Override
    public void onRemoveStatusClick(Status status) {

    }

    @Override
    public void onImageClick() {

    }

    @Override
    public void onCopyClick(String status) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (clipboard != null) {
            ClipData clip = ClipData.newPlainText("Status", status);
            clipboard.setPrimaryClip(clip);
            AppUtils.toast("Status copied to clipboard");
        }
    }

    @Override
    public void onCheckInClick(String status) {

    }

    @Override
    public void onViewLocationClick(double lat, double lng) {
        Intent intent = new Intent(this, ViewLocationActivity.class);
        intent.putExtra(Const.LATITUDE, lat);
        intent.putExtra(Const.LONGITUDE, lng);
        openPage(intent);
    }
}
