package apps.ahqmrf.kothay.friends.callback;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.home.response.Status;

/**
 * Created by bsse0 on 11/29/2017.
 */

public interface PostsListener extends ResponseListener {
    void onPostsFetched(List<Status> statuses);
}
