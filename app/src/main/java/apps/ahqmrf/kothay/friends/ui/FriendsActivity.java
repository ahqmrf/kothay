package apps.ahqmrf.kothay.friends.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.rey.material.widget.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.account.ui.AccountActivity;
import apps.ahqmrf.kothay.alarm.ui.MyRecyclerScroll;
import apps.ahqmrf.kothay.alarm.ui.SetAlarmActivity;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.friends.callback.FriendListRetriveListener;
import apps.ahqmrf.kothay.friends.connector.FriendActivityConnector;
import apps.ahqmrf.kothay.meeting.ui.CreateGroupActivity;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.ui.ProfileActivity;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FriendsActivity extends BaseActivity implements UserListener, FriendListRetriveListener, OnItemClickCallback {

    @BindView(R.id.friendList)         RecyclerView         friendListView;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout   swipeRefreshLayout;
    @BindView(R.id.btnCreateGroup)     FloatingActionButton fab;

    private FriendListAdapter       adapter;
    private FriendActivityConnector connector;
    private long                    id;
    private boolean                 addAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        ButterKnife.bind(this);

        connector = new FriendActivityConnector(this);
    }

    @Override
    public void onViewCreated() {
        id = self.getId();
        UserInfoResponse user = getIntent().getParcelableExtra(Const.USER_INFO);
        addAlarm = getIntent().getBooleanExtra(Const.ADD_ALARM, false);
        if (addAlarm) {
            setLabel("Set alarm");
            fab.setVisibility(View.GONE);
        } else if (user != null) {
            id = user.getId();
            setLabel("Friends of " + user.getName());
            fab.setVisibility(View.GONE);
        } else {
            setLabel(R.string.title_friends);
            fab.setVisibility(View.VISIBLE);
        }
        showBackArrow();
        adapter = new FriendListAdapter(this, R.layout.item_friend);
        friendListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        friendListView.setAdapter(adapter);
        friendListView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        showLoader();
        connector.getFriendList(id);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                connector.getFriendList(id);
                fetchNotifications();
            }
        });
    }

    @Override
    public void onFriendListRetrieved(List<LoginResponse> friends) {
        List<Object> list = new ArrayList<>();
        list.addAll(friends);
        adapter.filter(list);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        hideLoader();
    }

    @Override
    public void onItemClick(Object item) {
        LoginResponse response = (LoginResponse) item;
        if (response.getId() == self.getId()) {
            openPage(AccountActivity.class);
            return;
        }
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(self.getId());
        request.setOtherUserId(response.getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override
    public void onUserFetched(UserInfoResponse response) {
        if (addAlarm) {
            Intent intent = new Intent(this, SetAlarmActivity.class);
            intent.putExtra(Const.USER_INFO, response);
            openPage(intent);
            return;
        }
        Intent intent;
        if (response.isFriend()) {
            intent = new Intent(this, FriendProfileActivity.class);
        } else intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
    }

    @Override
    public void onFriendsClick() {
        if (addAlarm) {
            openPage(FriendsActivity.class);
        }
    }

    @OnClick(R.id.btnCreateGroup)
    void onCreateGroupClick() {
        openPage(CreateGroupActivity.class);
    }
}
