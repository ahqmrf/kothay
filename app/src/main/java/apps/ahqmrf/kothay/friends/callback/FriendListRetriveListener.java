package apps.ahqmrf.kothay.friends.callback;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.auth.response.LoginResponse;

/**
 * Created by bsse0 on 10/21/2017.
 */

public interface FriendListRetriveListener extends ResponseListener {
    void onFriendListRetrieved(List<LoginResponse> friends);
}
