package apps.ahqmrf.kothay.friends.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by maruf on 10/7/2017.
 */

public class FriendActivityClient extends Client<FriendActivityService> {
    @Override public FriendActivityService createService() {
        return getRetrofit().create(FriendActivityService.class);
    }
}
