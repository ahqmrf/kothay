package apps.ahqmrf.kothay.friends.response;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.BaseResponse;

/**
 * Created by maruf on 10/7/2017.
 */

public class FriendshipControlResponse extends BaseResponse {
    @SerializedName("actionType") private String actionType;

    public String getActionType() {
        return actionType;
    }
}
