package apps.ahqmrf.kothay.friends.service;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.friends.request.FriendshipControlRequest;
import apps.ahqmrf.kothay.friends.response.FriendListResponse;
import apps.ahqmrf.kothay.friends.response.FriendshipControlResponse;
import apps.ahqmrf.kothay.home.response.StatusResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by maruf on 10/7/2017.
 */

public interface FriendActivityService {
    @POST("control_friendship.php")
    Call<FriendshipControlResponse> controlFriendship(@Body FriendshipControlRequest request);

    @POST("get_friends.php")
    Call<FriendListResponse> getFriends(@Body FriendshipControlRequest request);

    @POST("get_posts.php")
    Call<StatusResponse> getPosts(@Body BaseRequest request);
}
