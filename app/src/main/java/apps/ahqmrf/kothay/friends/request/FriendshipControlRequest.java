package apps.ahqmrf.kothay.friends.request;

import com.google.gson.annotations.SerializedName;

import apps.ahqmrf.kothay.user.request.UserInfoRequest;

/**
 * Created by maruf on 10/7/2017.
 */

public class FriendshipControlRequest extends UserInfoRequest {
    @SerializedName("actionType") private String actionType;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
