package apps.ahqmrf.kothay.friends.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.alarm.ui.SetAlarmActivity;
import apps.ahqmrf.kothay.friends.callback.FriendActivityListener;
import apps.ahqmrf.kothay.friends.connector.FriendActivityConnector;
import apps.ahqmrf.kothay.message.ui.ChatActivity;
import apps.ahqmrf.kothay.tracker.ui.RouteActivity;
import apps.ahqmrf.kothay.tracker.ui.TrackUserActivity;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.user.ui.ProfileActivity;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.FullScreenImageFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FriendProfileActivity extends BaseActivity implements FriendActivityListener, UserListener {
    UserInfoResponse user;
    @BindView(R.id.profile_picture) ImageView  profilePictureView;
    @BindView(R.id.full_name)       TextView   fullNameView;
    @BindView(R.id.bio)             TextView   bioView;
    @BindView(R.id.friendsCount)    TextView   friendsCountView;
    @BindView(R.id.gender)          TextView   genderView;
    @BindView(R.id.workplace)       TextView   workplaceView;
    @BindView(R.id.email)           TextView   emailView;
    @BindView(R.id.phone)           TextView   phoneView;
    @BindView(R.id.scrollView)      ScrollView rootLayout;
    @BindView(R.id.callIcon)        ImageView  callIconView;
    @BindView(R.id.callText)        TextView   callTextView;
    @BindView(R.id.callLayout)      View       callLayout;
    @BindView(R.id.ic_track)        ImageView  trackIconView;
    @BindView(R.id.trackText)       TextView   trackTextView;
    @BindView(R.id.trackLayout)     View       trackLayout;

    private AlertDialog.Builder mDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        if (user != null) {
            setLabel(user.getUsername());
            int placeholder = R.drawable.man_placeholder;
            if (user.getGender().equals(AppUtils.getString(R.string.label_female))) {
                placeholder = R.drawable.woman_placeholder;
            }
            ImageLoader.getInstance().displayImage(
                    user.getImageUrl(),
                    profilePictureView,
                    AppUtils.getImageConfig(placeholder)
            );
            fullNameView.setText(user.getName());

            if (TextUtils.isEmpty(user.getBio())) {
                bioView.setText(Const.DEFAULT_BIO);
            } else bioView.setText(user.getBio());

            genderView.setText(user.getGender());
            emailView.setText(user.getEmail());
            workplaceView.setText(user.getWorkplace());
            friendsCountView.setText("See all friends(" + user.getFriendsCount() + ")");
            if (TextUtils.isEmpty(user.getPhone())) {
                phoneView.setText("Not available");
                callIconView.setColorFilter(AppUtils.getColor(R.color.bottomIconUnfocused));
                callTextView.setTextColor(AppUtils.getColor(R.color.bottomIconUnfocused));
                callLayout.setClickable(false);
            } else {
                phoneView.setText(user.getPhone());
            }

            if (!user.sharesLocation()) {
                trackIconView.setColorFilter(AppUtils.getColor(R.color.bottomIconUnfocused));
                trackTextView.setTextColor(AppUtils.getColor(R.color.bottomIconUnfocused));
                trackLayout.setClickable(false);
            }
            rootLayout.setVisibility(View.VISIBLE);

            DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            unfriend();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            mDialogBuilder = new AlertDialog.Builder(this);
            mDialogBuilder.setTitle("Confirm unfriend")
                    .setMessage("Are you sure you want to unfriend " + user.getName() + "?")
                    .setPositiveButton("Yes", mDialogClickListener)
                    .setNegativeButton("No", mDialogClickListener);
        }
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    private void unfriend() {
        new FriendActivityConnector(this).controlFriendship(self.getId(), user.getId(), Const.UNFRIEND);
    }

    @OnClick(R.id.messageLayout)
    void onMessageClick() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @OnClick(R.id.checkInLayout)
    void onViewCheckInClick() {
        Intent intent = new Intent(this, ViewPostsActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @OnClick(R.id.callLayout)
    void onCallClick() {
        if (!TextUtils.isEmpty(user.getPhone())) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + user.getPhone()));
            startActivity(intent);
        }
    }

    @OnClick(R.id.trackLayout)
    void onTrackClick() {
        Intent intent = new Intent(this, TrackUserActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @OnClick(R.id.btn_unfriend)
    void onUnfriendClick() {
        mDialogBuilder.show();
    }

    @Override
    public void onFailure(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onSuccess(String message) {
        AppUtils.toast(message);
    }

    @Override
    public void onRequestAccepted() {

    }

    @Override
    public void onRequestSent() {

    }

    @Override
    public void onRequestCancelled() {
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(self.getId());
        request.setOtherUserId(user.getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override
    public void onUserFetched(UserInfoResponse response) {
        Intent intent;
        if (response.isFriend()) {
            intent = new Intent(this, FriendProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Const.USER_INFO, response);
        openPage(intent);
        finish();
    }

    @OnClick(R.id.profile_picture)
    void onProfilePictureClick() {
        if (!TextUtils.isEmpty(user.getImageUrl())) {
            FullScreenImageFragment frag = FullScreenImageFragment.getInstance(user.getImageUrl());
            frag.show(getSupportFragmentManager(), "Full screen image");
        }
    }

    @OnClick(R.id.shortestRoute)
    void onShortestRouteClick() {
        Intent intent = new Intent(this, RouteActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @OnClick(R.id.setAlarmLayout)
    void onSetAlarmClick() {
        Intent intent = new Intent(this, SetAlarmActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }

    @OnClick(R.id.friendListLayout)
    void onViewFriendsClick() {
        Intent intent = new Intent(this, FriendsActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }
}
