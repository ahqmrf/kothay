package apps.ahqmrf.kothay.meeting.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class GroupListResponse extends BaseResponse {
    @SerializedName("groups") List<FriendsGroup> groups;

    public List<FriendsGroup> getGroups() {
        return groups;
    }
}
