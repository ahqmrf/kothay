package apps.ahqmrf.kothay.meeting.model;

import apps.ahqmrf.kothay.auth.response.LoginResponse;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class FriendItemForGroup extends LoginResponse {
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
