package apps.ahqmrf.kothay.meeting.connector;

import java.util.List;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.meeting.callback.FourSquareListener;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.meeting.model.MeetingPlace;
import apps.ahqmrf.kothay.meeting.request.NotifyRequest;
import apps.ahqmrf.kothay.meeting.response.FourSquareResponse;
import apps.ahqmrf.kothay.meeting.service.FourSquareService;
import apps.ahqmrf.kothay.notification.service.NotificationClient;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class FourSquareConnector {
    private FourSquareService service;
    private ResponseListener  listener;

    private final String CLIENT_ID     = "312322VQIWGNYNYVFXF5TCADBCXPTL1ZEWROKKMQUUSEN1WR";
    private final String CLIENT_SECRET = "QNOEWCCR4F2H3ZP4HLRKEJEL52DT2UGOMMAXOIRKJDILNRAW";
    private final String API_VERSION   = "20161010";

    public FourSquareConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = FourSquareService.retrofit.create(FourSquareService.class);
    }

    public void getSuitablePlaces(String query, double lat, double lng) {
        if (listener != null) {
            listener.showLoader();
            String geoLocation = lat + "," + lng;
            Call<FourSquareResponse> call = service.requestExplore(CLIENT_ID, CLIENT_SECRET, API_VERSION, geoLocation, query);
            call.enqueue(new Callback<FourSquareResponse>() {
                @Override public void onResponse(Call<FourSquareResponse> call, Response<FourSquareResponse> response) {
                    listener.hideLoader();
                    FourSquareResponse body = response.body();
                    if (body != null) {
                        ((FourSquareListener) listener).onPlacesFetched(body);
                    }
                }

                @Override public void onFailure(Call<FourSquareResponse> call, Throwable t) {
                    listener.hideLoader();
                }
            });
        }
    }

    public void notifyMembers(long userId, List<UserInfoResponse> members, MeetingPlace place) {
        if (listener != null) {
            NotifyRequest request = new NotifyRequest();
            request.setUserId(userId);
            request.setContent(place.getLat() + "," + place.getLng() + "," + place.getName());
            String to = "";

            for (int i = 0, size = members.size(); i < size; i++) {
                if (members.get(i).getId() == userId) {
                    members.remove(i);
                    break;
                }
            }

            for (int i = 0, size = members.size(); i < size; i++) {
                if (i > 0) to = to + ",";
                to = to + members.get(i).getId();
            }

            request.setTo(to);
            Call<BaseResponse> call = new NotificationClient().createService().notifyMembers(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override public void onResponse(Response<BaseResponse> response) {
                    BaseResponse body = response.body();
                    if (body != null) listener.onSuccess(body.getMessage());
                }
            });
        }
    }
}
