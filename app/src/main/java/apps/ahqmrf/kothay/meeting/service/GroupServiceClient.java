package apps.ahqmrf.kothay.meeting.service;

import apps.ahqmrf.kothay.networking.Client;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class GroupServiceClient extends Client<GroupService> {

    @Override
    public GroupService createService() {
        return getRetrofit().create(GroupService.class);
    }
}
