package apps.ahqmrf.kothay.meeting.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.meeting.callback.FourSquareListener;
import apps.ahqmrf.kothay.meeting.callback.OnPlaceOptionsClickCallback;
import apps.ahqmrf.kothay.meeting.connector.FourSquareConnector;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.meeting.model.MeetingPlace;
import apps.ahqmrf.kothay.meeting.response.FourSquareResponse;
import apps.ahqmrf.kothay.meeting.response.Group;
import apps.ahqmrf.kothay.meeting.response.Item;
import apps.ahqmrf.kothay.meeting.response.Location;
import apps.ahqmrf.kothay.meeting.response.Venue;
import apps.ahqmrf.kothay.tracker.callback.TrackerListener;
import apps.ahqmrf.kothay.tracker.connector.TrackerConnector;
import apps.ahqmrf.kothay.tracker.ui.RouteActivity;
import apps.ahqmrf.kothay.tracker.ui.ViewLocationActivity;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetPlacesActivity extends BaseActivity implements TrackerListener, FourSquareListener, OnPlaceOptionsClickCallback {

    @BindView(R.id.namesSpinner)         AppCompatSpinner namesSpinner;
    @BindView(R.id.locationTypesSpinner) AppCompatSpinner locationTypesSpinner;
    @BindView(R.id.placesList)           RecyclerView     placesListView;

    private FriendsGroup            group;
    private PlaceAdapter            placeAdapter;
    private ArrayList<MeetingPlace> places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_places);
        ButterKnife.bind(this);
    }

    @Override public void onViewCreated() {
        showBackArrow();
        setLabel("Meeting places");
        group = getIntent().getParcelableExtra(Const.GROUP_INFO);
        if (group != null) {
            ArrayAdapter<UserInfoResponse> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            adapter.addAll(group.getMembers());
            namesSpinner.setAdapter(adapter);
            places = new ArrayList<>();
            placesListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            placeAdapter = new PlaceAdapter(places, this);
            placesListView.setAdapter(placeAdapter);
        }
    }

    @OnClick(R.id.btnSearchPlaces)
    void onSearchClick() {
        new TrackerConnector(this).getUserLocation((UserInfoResponse) namesSpinner.getSelectedItem());
    }

    @Override
    public void onSelfLocationRetrieved(LatLng latLng) {

    }

    @Override
    public void onUserLocationRetrieved(LatLng latLng) {
        String query = (String) locationTypesSpinner.getSelectedItem();
        new FourSquareConnector(this).getSuitablePlaces(query, latLng.latitude, latLng.longitude);
    }

    @Override
    public void onPlacesFetched(FourSquareResponse response) {
        ArrayList<MeetingPlace> places = parse(response);
        this.places.clear();
        this.places.addAll(places);
        placeAdapter.notifyDataSetChanged();
    }

    private ArrayList<MeetingPlace> parse(FourSquareResponse response) {
        ArrayList<MeetingPlace> places = new ArrayList<>();
        List<Group> groups = response.getResponse().getGroups();
        for (Group g : groups) {
            List<Item> items = g.getItems();
            for (Item i : items) {
                Venue venue = i.getVenue();
                MeetingPlace place = new MeetingPlace();
                Location location = venue.getLocation();
                place.setAddress(location.getAddress());
                place.setCity(location.getCity());
                place.setCountry(location.getCountry());
                place.setState(location.getState());
                place.setLat(location.getLat());
                place.setLng(location.getLng());
                place.setName(venue.getName());
                if (venue.getRating() != null) place.setRating(venue.getRating());
                places.add(place);
            }
        }

        return places;
    }

    @Override
    public void onViewLocation(MeetingPlace place) {
        Intent intent = new Intent(this, ViewLocationActivity.class);
        intent.putExtra(Const.PLACE, place.getName());
        intent.putExtra(Const.LATITUDE, place.getLat());
        intent.putExtra(Const.LONGITUDE, place.getLng());
        openPage(intent);
    }

    @Override
    public void onShortestRoute(MeetingPlace place) {
        Intent intent = new Intent(this, RouteActivity.class);
        UserInfoResponse wink = new UserInfoResponse();
        wink.setName(place.getName());
        intent.putExtra(Const.USER_INFO, wink);
        intent.putExtra(Const.LATITUDE, place.getLat());
        intent.putExtra(Const.LONGITUDE, place.getLng());
        openPage(intent);
    }

    @Override
    public void onShare(MeetingPlace place) {
        new FourSquareConnector(this).notifyMembers(self.getId(), group.getMembers(), place);
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }
}
