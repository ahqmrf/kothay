package apps.ahqmrf.kothay.meeting.callback;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.meeting.response.FourSquareResponse;

/**
 * Created by bsse0 on 12/21/2017.
 */

public interface FourSquareListener extends ResponseListener {
    void onPlacesFetched(FourSquareResponse response);
}
