package apps.ahqmrf.kothay.meeting.callback;

import java.util.List;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.meeting.response.GroupResponse;

/**
 * Created by bsse0 on 12/20/2017.
 */

public interface GroupServiceListener extends ResponseListener {
    void onGroupCreated(GroupResponse response);

    void onGroupsFetched(List<FriendsGroup> groups);

    void onGroupDeleted();
}
