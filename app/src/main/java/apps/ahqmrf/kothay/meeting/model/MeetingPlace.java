package apps.ahqmrf.kothay.meeting.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bsse0 on 12/21/2017.
 */

public class MeetingPlace implements Parcelable {
    private String name;
    private String address;
    private String city;
    private String state;
    private String country;
    private double lat;
    private double lng;
    private double rating;

    public MeetingPlace() {
    }

    protected MeetingPlace(Parcel in) {
        name = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        rating = in.readDouble();
    }

    public static final Creator<MeetingPlace> CREATOR = new Creator<MeetingPlace>() {
        @Override
        public MeetingPlace createFromParcel(Parcel in) {
            return new MeetingPlace(in);
        }

        @Override
        public MeetingPlace[] newArray(int size) {
            return new MeetingPlace[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(address);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(country);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeDouble(rating);
    }
}
