package apps.ahqmrf.kothay.meeting.callback;

import apps.ahqmrf.kothay.meeting.model.MeetingPlace;

/**
 * Created by bsse0 on 12/21/2017.
 */

public interface OnPlaceOptionsClickCallback {
    void onViewLocation(MeetingPlace place);

    void onShortestRoute(MeetingPlace place);

    void onShare(MeetingPlace place);
}
