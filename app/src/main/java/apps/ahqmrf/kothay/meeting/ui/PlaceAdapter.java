package apps.ahqmrf.kothay.meeting.ui;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.meeting.callback.OnPlaceOptionsClickCallback;
import apps.ahqmrf.kothay.meeting.model.MeetingPlace;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 12/21/2017.
 */

public class PlaceAdapter extends RecyclerView.Adapter {

    private ArrayList<MeetingPlace>     items;
    private OnPlaceOptionsClickCallback callback;

    public PlaceAdapter(ArrayList<MeetingPlace> items, OnPlaceOptionsClickCallback callback) {
        this.items = items;
        this.callback = callback;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_meeting_place, parent, false);
        return new PlaceViewHolder(view);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PlaceViewHolder) holder).bind(items.get(position));
    }

    @Override public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)    TextView nameView;
        @BindView(R.id.rating)  TextView ratingView;
        @BindView(R.id.address) TextView addressView;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(MeetingPlace place) {
            nameView.setText(place.getName());
            if (place.getRating() != 0) {
                ratingView.setText("Rating " + place.getRating());
                ratingView.setVisibility(View.VISIBLE);
            } else {
                ratingView.setVisibility(View.GONE);
            }
            String address = place.getCountry();
            if (!TextUtils.isEmpty(place.getCity())) {
                address = place.getCity() + ", " + address;
            }
            if (!TextUtils.isEmpty(place.getAddress())) {
                address = place.getAddress() + ", " + address;
            }
            addressView.setText(address);
        }

        @OnClick(R.id.route)
        void onRouteClick() {
            if (callback != null) callback.onShortestRoute(items.get(getAdapterPosition()));
        }

        @OnClick(R.id.showOnMap)
        void onShowClick() {
            if (callback != null) callback.onViewLocation(items.get(getAdapterPosition()));
        }

        @OnClick(R.id.share)
        void onShareClick() {
            if (callback != null) callback.onShare(items.get(getAdapterPosition()));
        }
    }
}
