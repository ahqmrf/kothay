package apps.ahqmrf.kothay.meeting.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class FriendsGroup implements Parcelable {
    @SerializedName("groupId") private     long                   groupId;
    @SerializedName("groupName") private   String                 groupName;
    @SerializedName("createdById") private long                   createdById;
    @SerializedName("members") private     List<UserInfoResponse> members;
    @SerializedName("createdAt") private   String                 createdAt;

    public Date getCreatedTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return format.parse(createdAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected FriendsGroup(Parcel in) {
        groupId = in.readLong();
        groupName = in.readString();
        createdById = in.readLong();
        members = in.createTypedArrayList(UserInfoResponse.CREATOR);
        createdAt = in.readString();
    }

    public static final Creator<FriendsGroup> CREATOR = new Creator<FriendsGroup>() {
        @Override
        public FriendsGroup createFromParcel(Parcel in) {
            return new FriendsGroup(in);
        }

        @Override
        public FriendsGroup[] newArray(int size) {
            return new FriendsGroup[size];
        }
    };

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(long createdById) {
        this.createdById = createdById;
    }

    public List<UserInfoResponse> getMembers() {
        return members;
    }

    public void setMembers(List<UserInfoResponse> members) {
        this.members = members;
    }


    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(groupId);
        parcel.writeString(groupName);
        parcel.writeLong(createdById);
        parcel.writeTypedList(members);
        parcel.writeString(createdAt);
    }
}
