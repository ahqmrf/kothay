package apps.ahqmrf.kothay.meeting.callback;

import apps.ahqmrf.kothay.meeting.model.FriendsGroup;

/**
 * Created by bsse0 on 12/21/2017.
 */

public interface GroupItemClickCallback {
    void onDeleteClick(FriendsGroup group);

    void onMeetingPlacesClick(FriendsGroup group);
}
