package apps.ahqmrf.kothay.meeting.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class GroupRequest {
    @SerializedName("groupName") private   String groupName;
    @SerializedName("createdById") private long   createdById;
    @SerializedName("members") private     String members;

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setCreatedById(long createdById) {
        this.createdById = createdById;
    }

    public void addMember(long id) {
        if (members == null) {
            members = id + "";
        } else {
            members = members + "," + id;
        }
    }
}
