package apps.ahqmrf.kothay.meeting.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.rey.material.widget.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.alarm.ui.MyRecyclerScroll;
import apps.ahqmrf.kothay.meeting.callback.GroupItemClickCallback;
import apps.ahqmrf.kothay.meeting.callback.GroupServiceListener;
import apps.ahqmrf.kothay.meeting.connector.GroupServiceConnector;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.meeting.response.GroupResponse;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GroupsActivity extends BaseActivity implements GroupServiceListener, GroupItemClickCallback {

    @BindView(R.id.groupList)      RecyclerView         groupsListView;
    @BindView(R.id.btnCreateGroup) FloatingActionButton fab;

    private GroupListAdapter        adapter;
    private ArrayList<FriendsGroup> items;
    private GroupServiceConnector   connector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        connector = new GroupServiceConnector(this);
        showBackArrow();
        setLabel(R.string.title_my_groups);
        items = new ArrayList<>();
        adapter = new GroupListAdapter(items, this);

        groupsListView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        groupsListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        groupsListView.setAdapter(adapter);
        connector.getGroups(self.getId());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    @Override
    public void onGroupCreated(GroupResponse response) {

    }

    @Override
    public void onGroupsFetched(List<FriendsGroup> groups) {
        items.clear();
        items.addAll(groups);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGroupDeleted() {
        connector.getGroups(self.getId());
    }

    @OnClick(R.id.btnCreateGroup)
    void onCreateGroupClick() {
        openPage(CreateGroupActivity.class);
    }

    @Override
    public void onDeleteClick(final FriendsGroup group) {
        DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteGroup(group);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm")
                .setMessage("Are you sure you want to delete this group?")
                .setPositiveButton("Yes", mDialogClickListener)
                .setNegativeButton("No", mDialogClickListener);
        builder.show();
    }

    private void deleteGroup(FriendsGroup group) {
        connector.deleteGroup(group.getGroupId());
    }

    @Override
    public void onMeetingPlacesClick(FriendsGroup group) {
        Intent intent = new Intent(this, GetPlacesActivity.class);
        intent.putExtra(Const.GROUP_INFO, group);
        openPage(intent);
    }
}
