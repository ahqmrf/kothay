package apps.ahqmrf.kothay.meeting.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import apps.ahqmrf.kothay.App;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.home.response.Status;
import apps.ahqmrf.kothay.meeting.callback.GroupItemClickCallback;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 12/21/2017.
 */

public class GroupListAdapter extends RecyclerView.Adapter {

    private List<FriendsGroup>     items;
    private GroupItemClickCallback callback;

    public GroupListAdapter(List<FriendsGroup> items, GroupItemClickCallback callback) {
        this.items = items;
        this.callback = callback;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false);
        return new GroupViewHolder(view);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((GroupViewHolder) holder).bind(items.get(position));
    }

    @Override public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {

        @BindView(R.id.big)     ImageView bigImageView;
        @BindView(R.id.medium)  ImageView mediumImageView;
        @BindView(R.id.small)   ImageView smallImageView;
        @BindView(R.id.name)    TextView  groupNameView;
        @BindView(R.id.time)    TextView  timeView;
        @BindView(R.id.members) TextView  membersView;
        @BindView(R.id.more)    ImageView optionsView;

        PopupMenu popupMenu;


        public GroupViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(FriendsGroup item) {

            popupMenu = new PopupMenu(App.getContext(), optionsView);
            popupMenu.setOnMenuItemClickListener(this);
            popupMenu.inflate(R.menu.group_popup);

            if (item.getMembers().size() > 2) {
                int placeholder = R.drawable.man_placeholder;
                UserInfoResponse big = item.getMembers().get(0);
                if (big.getGender().equals(Const.FEMALE))
                    placeholder = R.drawable.woman_placeholder;
                ImageLoader.getInstance().displayImage(
                        big.getImageUrl(),
                        bigImageView,
                        AppUtils.getImageConfig(placeholder)
                );

                placeholder = R.drawable.man_placeholder;
                UserInfoResponse medium = item.getMembers().get(1);
                if (medium.getGender().equals(Const.FEMALE))
                    placeholder = R.drawable.woman_placeholder;
                ImageLoader.getInstance().displayImage(
                        medium.getImageUrl(),
                        mediumImageView,
                        AppUtils.getImageConfig(placeholder)
                );

                placeholder = R.drawable.man_placeholder;
                UserInfoResponse small = item.getMembers().get(2);
                if (small.getGender().equals(Const.FEMALE))
                    placeholder = R.drawable.woman_placeholder;
                ImageLoader.getInstance().displayImage(
                        small.getImageUrl(),
                        smallImageView,
                        AppUtils.getImageConfig(placeholder)
                );

                groupNameView.setText(item.getGroupName());
                String members = big.getName() + ",\n" + medium.getName() + ",\n" + small.getName();
                if (item.getMembers().size() > 3) {
                    members = members + ",\n" + "and " + (item.getMembers().size() - 3) + " others";
                }
                membersView.setText(members);
                AppUtils.TimeModel model = new AppUtils.TimeModel(item.getCreatedTime());
                timeView.setText(model.getDateTime());
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.menu_delete) {
                if (callback != null) {
                    callback.onDeleteClick(items.get(getAdapterPosition()));
                }
            }
            if (id == R.id.menu_get_meeting_places) {
                if (callback != null) {
                    callback.onMeetingPlacesClick((items.get(getAdapterPosition())));
                }
            }
            return false;
        }

        @OnClick(R.id.more)
        void onOptionsClick() {
            if (popupMenu != null) {
                popupMenu.show();
            }
        }
    }
}
