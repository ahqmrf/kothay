package apps.ahqmrf.kothay.meeting.connector;

import java.util.HashSet;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.ServiceCallback;
import apps.ahqmrf.kothay.meeting.callback.GroupServiceListener;
import apps.ahqmrf.kothay.meeting.request.DeleteGroupRequest;
import apps.ahqmrf.kothay.meeting.request.GroupRequest;
import apps.ahqmrf.kothay.meeting.response.GroupListResponse;
import apps.ahqmrf.kothay.meeting.response.GroupResponse;
import apps.ahqmrf.kothay.meeting.service.GroupService;
import apps.ahqmrf.kothay.meeting.service.GroupServiceClient;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class GroupServiceConnector {
    private ResponseListener listener;
    private GroupService     service;

    public GroupServiceConnector(ResponseListener listener) {
        this.listener = listener;
        this.service = new GroupServiceClient().createService();
    }

    public void createGroup(long createdById, String groupName, HashSet<Long> memberIds) {
        if (listener != null) {
            listener.showLoader();
            GroupRequest request = new GroupRequest();
            request.setCreatedById(createdById);
            request.setGroupName(groupName);
            for (Long id : memberIds) {
                request.addMember(id);
            }

            Call<GroupResponse> call = service.createGroup(request);
            call.enqueue(new ServiceCallback<GroupResponse>(listener) {
                @Override public void onResponse(Response<GroupResponse> response) {
                    GroupResponse body = response.body();
                    if (body != null) {
                        listener.onSuccess(body.getMessage());
                        ((GroupServiceListener) listener).onGroupCreated(body);
                    }
                }
            });
        }
    }

    public void getGroups(long userId) {
        if (listener != null) {
            listener.showLoader();
            BaseRequest request = new BaseRequest();
            request.setUserId(userId);
            Call<GroupListResponse> call = service.getGroups(request);
            call.enqueue(new ServiceCallback<GroupListResponse>(listener) {
                @Override public void onResponse(Response<GroupListResponse> response) {
                    GroupListResponse body = response.body();
                    if (body != null) {
                        ((GroupServiceListener) listener).onGroupsFetched(body.getGroups());
                    }
                }
            });
        }
    }

    public void deleteGroup(long groupId) {
        if (listener != null) {
            listener.showLoader();
            DeleteGroupRequest request = new DeleteGroupRequest();
            request.setGroupId(groupId);
            Call<BaseResponse> call = service.deleteGroup(request);
            call.enqueue(new ServiceCallback<BaseResponse>(listener) {
                @Override public void onResponse(Response<BaseResponse> response) {
                    BaseResponse out = response.body();
                    if (out != null) {
                        listener.onSuccess(out.getMessage());
                        ((GroupServiceListener) listener).onGroupDeleted();
                    }
                }
            });
        }
    }
}
