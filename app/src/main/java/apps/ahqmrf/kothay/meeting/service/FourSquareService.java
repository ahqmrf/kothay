package apps.ahqmrf.kothay.meeting.service;

/**
 * Created by bsse0 on 12/20/2017.
 */

import apps.ahqmrf.kothay.meeting.response.FourSquareResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FourSquareService {

    @GET("venues/explore/")
    Call<FourSquareResponse> requestExplore(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") String v,
            @Query("ll") String ll,
            @Query("query") String query);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.foursquare.com/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}