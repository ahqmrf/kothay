package apps.ahqmrf.kothay.meeting.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.friends.callback.FriendListRetriveListener;
import apps.ahqmrf.kothay.friends.connector.FriendActivityConnector;
import apps.ahqmrf.kothay.friends.ui.FriendListAdapter;
import apps.ahqmrf.kothay.meeting.callback.GroupServiceListener;
import apps.ahqmrf.kothay.meeting.connector.GroupServiceConnector;
import apps.ahqmrf.kothay.meeting.model.FriendItemForGroup;
import apps.ahqmrf.kothay.meeting.model.FriendsGroup;
import apps.ahqmrf.kothay.meeting.response.GroupResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateGroupActivity extends BaseActivity implements OnItemClickCallback, FriendListRetriveListener, GroupServiceListener {

    @BindView(R.id.friendList)     RecyclerView            friendListView;
    @BindView(R.id.inputGroupName) EditText                groupNameInputView;
    private                        FriendListAdapter       adapter;
    private                        FriendActivityConnector connector;
    private                        HashSet<Long>           selectedFriendIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);

        connector = new FriendActivityConnector(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        setLabel(R.string.title_create_group);
        adapter = new FriendListAdapter(this, R.layout.item_friend_group);
        friendListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        friendListView.setAdapter(adapter);

        selectedFriendIds = new HashSet<>();

        showLoader();
        connector.getFriendList(self.getId());
    }

    @Override
    public void onItemClick(Object item) {
        long id = (long) item;
        if (selectedFriendIds.contains(id)) selectedFriendIds.remove(id);
        else selectedFriendIds.add(id);
    }

    @Override
    public void onFriendListRetrieved(List<LoginResponse> friends) {
        List<Object> list = new ArrayList<>();
        for (LoginResponse i : friends) {
            FriendItemForGroup item = new FriendItemForGroup();
            item.setId(i.getId());
            item.setUsername(i.getUsername());
            item.setName(i.getName());
            item.setImageUrl(i.getImageUrl());
            item.setWorkplace(i.getWorkplace());
            item.setSelected(false);
            item.setGender(i.getGender());
            list.add(item);
        }
        adapter.filter(list);
        adapter.notifyDataSetChanged();
        hideLoader();
    }

    @OnClick(R.id.btnCreateGroup)
    void onCreateGroupClick() {
        hideSoftKeyboard();
        String groupName = groupNameInputView.getText().toString();
        if (TextUtils.isEmpty(groupName)) {
            AppUtils.toast("Group name is required");
            return;
        }
        if (selectedFriendIds.size() < 1) {
            AppUtils.toast("Please select at least one friend to create a group!");
            return;
        }
        new GroupServiceConnector(this).createGroup(self.getId(), groupName, selectedFriendIds);
    }

    @Override
    public void onGroupCreated(GroupResponse response) {
        Intent intent = new Intent(this, GroupsActivity.class);
        intent.putExtra("BOOO!!!", ":)");
        openPage(intent);
        finish();
    }

    @Override
    public void onGroupsFetched(List<FriendsGroup> groups) {

    }

    @Override
    public void onGroupDeleted() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }
}
