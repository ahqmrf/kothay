package apps.ahqmrf.kothay.meeting.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 12/21/2017.
 */

public class NotifyRequest {
    @SerializedName("userId") private  long   userId;
    @SerializedName("content") private String content;
    @SerializedName("to") private      String to;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
