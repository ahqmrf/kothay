package apps.ahqmrf.kothay.meeting.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;

/**
 * Created by bsse0 on 12/20/2017.
 */

public class GroupResponse extends BaseResponse {
    @SerializedName("groupName") private   String                 groupName;
    @SerializedName("createdById") private long                   createdById;
    @SerializedName("members") private     List<UserInfoResponse> members;
    @SerializedName("createdAt") private   String                 createdAt;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(long createdById) {
        this.createdById = createdById;
    }

    public List<UserInfoResponse> getMembers() {
        return members;
    }

    public void setMembers(List<UserInfoResponse> members) {
        this.members = members;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
