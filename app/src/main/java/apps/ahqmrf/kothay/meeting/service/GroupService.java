package apps.ahqmrf.kothay.meeting.service;

import apps.ahqmrf.kothay.BaseRequest;
import apps.ahqmrf.kothay.BaseResponse;
import apps.ahqmrf.kothay.meeting.request.DeleteGroupRequest;
import apps.ahqmrf.kothay.meeting.request.GroupRequest;
import apps.ahqmrf.kothay.meeting.response.GroupListResponse;
import apps.ahqmrf.kothay.meeting.response.GroupResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by bsse0 on 12/20/2017.
 */

public interface GroupService {
    @POST("create_group.php")
    Call<GroupResponse> createGroup(@Body GroupRequest request);

    @POST("get_groups.php")
    Call<GroupListResponse> getGroups(@Body BaseRequest request);

    @POST("delete_group.php")
    Call<BaseResponse> deleteGroup(@Body DeleteGroupRequest request);
}
