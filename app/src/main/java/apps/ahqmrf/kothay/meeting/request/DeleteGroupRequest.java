package apps.ahqmrf.kothay.meeting.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 12/21/2017.
 */

public class DeleteGroupRequest {
    @SerializedName("groupId") private long groupId;

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }
}
