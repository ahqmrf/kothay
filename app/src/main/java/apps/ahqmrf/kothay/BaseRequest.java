package apps.ahqmrf.kothay;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/17/2017.
 */

public class BaseRequest {
    @SerializedName("userId") private long userId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
