package apps.ahqmrf.kothay;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.binjar.prefsdroid.Preference;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import apps.ahqmrf.kothay.util.PrefKeys;

/**
 * Created by bsse0 on 7/22/2017.
 */

public class App extends Application {
    private static App context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        Intent intent = new Intent(getApplicationContext(), LocationUpdateService.class);
        stopService(intent);
        startService(intent);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);

        Preference.load().using(this).with(PrefKeys.PREF_NAME).prepare();
    }

    public static Context getContext() {
        return context;
    }
}
