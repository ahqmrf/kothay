package apps.ahqmrf.kothay.util;

/**
 * Created by bsse0 on 7/22/2017.
 */

public final class PrefKeys {
    public static final String PREF_NAME       = "kothay.pref";
    public static final String TOKEN           = "token";
    public static final String USER_INFO       = "UserInfo";
    public static final String EMAIL           = "cachedEmail";
    public static final String SERVICE_STARTED = "serviceStarted";
    public static final String LOADER          = "Loader";
}
