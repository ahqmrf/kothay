package apps.ahqmrf.kothay.util;

/**
 * Created by bsse0 on 7/22/2017.
 */

public final class Const {
    public static final String BASE_URL                     = "http://ahqmrf.000webhostapp.com/";
    public static final long   SPLASH_DURATION              = 3500;
    public static final int    REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final int    OK                           = 200;
    public static final String EMAIL                        = "email";
    public static final String VERIFICATION_CODE            = "VerificationCode";
    public static final long   SPLASH_DURATION_SHORT        = 1000;
    public static final String USER_INFO                    = "UserInfo";
    public static final String CANCEL_REQUEST               = "Cancel request";
    public static final String ADD_AS_FRIEND                = "Add as friend";
    public static final String ACCEPT_REQUEST               = "Accept request";
    public static final String DEFAULT_BIO                  = "Hey there! I am new to Kothay!";
    public static final String UNFRIEND                     = "Unfriend";
    public static final String UPDATE_TYPE                  = "UpdateType";
    public static final String CURRENT_VALUE                = "CurrentValue";
    public static final String UPDATE_BIO                   = "Update bio";
    public static final String UPDATE_PROFILE_PICTURE       = "Update profile picture";
    public static final String UPDATE_WORKPLACE             = "Update workplace";
    public static final String UPDATE_GENDER                = "Update gender";
    public static final String UPDATE_PHONE                 = "Update phone";
    public static final String IMAGE_URL                    = "ImageURL";
    public static final String MALE                         = "Male";
    public static final String FEMALE                       = "Female";
    public static final String LOCATION                     = "location";
    public static final String LATITUDE                     = "latitude";
    public static final String LONGITUDE                    = "longitude";
    public static final String PLACE                        = "place";
    public static final String WOMAN                        = "Woman";
    public static final String EDIT_STATUS                  = "Edit";
    public static final String REMOVE_STATUS                = "Remove";
    public static final String ID                           = "id";
    public static final String ALARM                        = "Alarm";
    public static final String NAME                         = "Name";
    public static final String LISTENER                     = "Listener";
    public static final long   VIBRATE_ALARM_DURATION       = 1000;
    public static final long   VIBRATE_MESSAGE_DURATION     = 100;
    public static final String ALARM_SOUND_ENABLED          = "AlarmSoundEnabled";
    public static final String ALARM_VIBRATION_ENABLED      = "AlarmVibrationEnabled";
    public static final String ALARM_NOTIFICATION_ENABLED   = "AlarmNotificationEnabled";
    public static final String MESSAGE_NOTIFICATION_ENABLED = "MessageNotificationEnabled";
    public static final String MESSAGE_VIBRATION_ENABLED    = "MessageVibrationEnabled";
    public static final String REQUEST_ACCEPT               = "RequestAccept";
    public static final String CHAT                         = "chat";
    public static final String UNDEFINED                    = "undefined";
    public static final String TYPE_TEXT                    = "text";
    public static final String CHAT_STATE                   = "ChatState";
    public static final String MESSAGES                     = "messages";
    public static final String LAST_MESSAGE                 = "lastMessage";
    public static final String TYPING_STATUS                = "typingStatus";
    public static final String CONVERSATION                 = "conversation";
    public static final String FRIEND_REQUEST               = "FriendRequest";
    public static final String ACTION_TYPE                  = "ActionType";
    public static final String RESET_PASSWORD               = "ResetPassword";
    public static final String STATUS                       = "Status";
    public static final String ADD_ALARM                    = "AddAlarm";
    public static final String SMILEY                       = "smiley";
    public static final String CHAT_OPEN                    = "ChatOpen";
    public static final int    EMO_LIST_MAX_ROW             = 4;
    public static final String PHOTO                        = "Photo";
    public static final int    REQUEST_BROWSE_GALLERY       = 21;
    public static final int    READ_EXTERNAL_STORAGE        = 22;
    public static final String TYPE_IMAGE                   = "image";
    public static final String MESSAGE                      = "message";
    public static final String GROUP_INFO                   = "GroupInfo";
    public static       String UNKNOWN_PLACE                = "Unknown place";
}
