package apps.ahqmrf.kothay.util;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import apps.ahqmrf.kothay.R;

/**
 * Created by bsse0 on 10/21/2017.
 */

public class FullScreenImageFragment extends DialogFragment {

    View      progressLayout;
    ImageView imageView;

    public FullScreenImageFragment() {
    }

    public static FullScreenImageFragment getInstance(String imageUrl) {
        Bundle args = new Bundle();
        args.putString(Const.IMAGE_URL, imageUrl);
        FullScreenImageFragment frag = new FullScreenImageFragment();
        frag.setArguments(args);
        return frag;
    }

    @NonNull @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_full_screen_image);

        progressLayout = dialog.findViewById(R.id.progress_layout);
        imageView = (ImageView) dialog.findViewById(R.id.image);
        String imageUrl = getArguments().getString(Const.IMAGE_URL);

        ImageLoader.getInstance().displayImage(
                imageUrl,
                imageView,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        progressLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        progressLayout.setVisibility(View.GONE);
                        AppUtils.toast(failReason.toString());
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        progressLayout.setVisibility(View.GONE);
                    }
                });

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(AppUtils.getColor(android.R.color.transparent)));
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }

        return dialog;
    }
}
