package apps.ahqmrf.kothay.tracker.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.LocationUpdateService;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.tracker.callback.DirectionFinderListener;
import apps.ahqmrf.kothay.tracker.callback.TrackerListener;
import apps.ahqmrf.kothay.tracker.connector.TrackerConnector;
import apps.ahqmrf.kothay.tracker.module.DirectionFinder;
import apps.ahqmrf.kothay.tracker.module.Route;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RouteActivity extends BaseActivity implements OnMapReadyCallback, TrackerListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, DirectionFinderListener {

    private static final String TAG = "Route Activity";
    UserInfoResponse user;
    private GoogleMap        mGoogleMap;
    private TrackerConnector connector;
    private LatLng           selfLocation;
    private GoogleApiClient  mGoogleApiClient;
    private LocationManager  mLocationManager;
    private Marker           selfMarker, userMarker;
    private List<Marker>   originMarkers      = new ArrayList<>();
    private List<Marker>   destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths      = new ArrayList<>();

    private double lat, lng;

    @BindView(R.id.routeInfo) View     routeInfoView;
    @BindView(R.id.duration)  TextView durationView;
    @BindView(R.id.distance)  TextView distanceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        ButterKnife.bind(this);

        connector = new TrackerConnector(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        lat = getIntent().getDoubleExtra(Const.LATITUDE, 10000000);
        lng = getIntent().getDoubleExtra(Const.LONGITUDE, 10000000);
        setLabel(user.getName());
        SupportMapFragment mMapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFrag.getMapAsync(this);
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setPadding(0, AppUtils.dpToPx(48 * 2), 0, AppUtils.dpToPx(48));
        mGoogleMap.setTrafficEnabled(true);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        showRoute();
    }

    @OnClick(R.id.ic_refresh)
    public void showRoute() {
        if (mGoogleMap != null) mGoogleMap.clear();
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                proceed();
                Intent intent = new Intent(getApplicationContext(), LocationUpdateService.class);
                stopService(intent);
                startService(intent);
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                AppUtils.toast(R.string.error_permission);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
        };

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission(permissionListener);
        } else {
            proceed();
        }
    }

    private void proceed() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlert();
        }

        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        } else {
            if (mGoogleApiClient.isConnected()) {
                requestLocation();
            } else {
                mGoogleApiClient.connect();
            }
        }
    }

    public void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocation();
    }

    @Override public void onConnectionSuspended(int i) {

    }

    private void requestLocation() {
        final LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        selfLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (lat != 10000000 && lng != 10000000) {
            onUserLocationRetrieved(new LatLng(lat, lng));
        } else {
            connector.getUserLocation(user);
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override public void onSelfLocationRetrieved(LatLng latLng) {

    }

    @Override
    public void onUserLocationRetrieved(final LatLng latLng) {
        MarkerOptions options = new MarkerOptions();
        options.position(selfLocation);
        options.title(self.getName());
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        selfMarker = mGoogleMap.addMarker(options);

        options = new MarkerOptions();
        options.position(latLng);
        options.title(user.getName());
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        userMarker = mGoogleMap.addMarker(options);
        userMarker.showInfoWindow();


        String origin = selfLocation.latitude + "," + selfLocation.longitude;
        String destination = latLng.latitude + "," + latLng.longitude;

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDirectionFinderStart() {
        showLoader();
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        hideLoader();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (routes.size() > 0) {
            Route route = routes.get(0);
            for (int i = 1, size = routes.size(); i < size; i++) {
                if (routes.get(i).distance.value < route.distance.value) {
                    route = routes.get(i);
                }
            }
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            durationView.setText(route.duration.text);
            distanceView.setText(route.distance.text);
            routeInfoView.setVisibility(View.VISIBLE);

            builder.include(route.startLocation).include(route.endLocation);

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(AppUtils.getColor(R.color.routeColor)).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mGoogleMap.addPolyline(polylineOptions));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 36);
            mGoogleMap.animateCamera(cameraUpdate);
        } else {
            AppUtils.toast("Oops! Could not find route!");
            routeInfoView.setVisibility(View.GONE);
        }
    }

    @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
