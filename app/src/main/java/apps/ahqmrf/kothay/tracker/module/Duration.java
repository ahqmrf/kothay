package apps.ahqmrf.kothay.tracker.module;

/**
 * Created by bsse0 on 12/15/2017.
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
