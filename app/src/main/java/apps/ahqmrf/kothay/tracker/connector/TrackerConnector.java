package apps.ahqmrf.kothay.tracker.connector;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.tracker.callback.TrackerListener;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;

/**
 * Created by bsse0 on 11/9/2017.
 */

public class TrackerConnector {
    private ResponseListener listener;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference locationRef;

    public TrackerConnector(ResponseListener listener) {
        this.listener = listener;
    }

    public void getUserLocation(UserInfoResponse userInfo) {
        if (listener != null) {
            listener.showLoader();
            locationRef = database.getReference(Const.LOCATION).child(userInfo.getUsername());
            locationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                    listener.hideLoader();
                    if (dataSnapshot != null && dataSnapshot.hasChild(Const.LATITUDE) && dataSnapshot.hasChild(Const.LONGITUDE)) {
                        String latStr = dataSnapshot.child(Const.LATITUDE).getValue().toString();
                        String lngStr = dataSnapshot.child(Const.LONGITUDE).getValue().toString();
                        if (!TextUtils.isEmpty(latStr) && !TextUtils.isEmpty(lngStr)) {
                            LatLng latLng = new LatLng(Double.parseDouble(latStr), Double.parseDouble(lngStr));
                            ((TrackerListener) listener).onUserLocationRetrieved(latLng);
                        } else {
                            listener.onFailure(AppUtils.getString(R.string.error_something_wrong));
                        }
                    }
                }

                @Override public void onCancelled(DatabaseError databaseError) {
                    listener.hideLoader();
                }
            });
        }
    }

    public void getSelfLocation(String username) {
        if (listener != null) {
            listener.showLoader();
            locationRef = database.getReference(Const.LOCATION).child(username);
            locationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                    listener.hideLoader();
                    if (dataSnapshot.hasChild(Const.LATITUDE) && dataSnapshot.hasChild(Const.LONGITUDE)) {
                        String latStr = dataSnapshot.child(Const.LATITUDE).getValue().toString();
                        String lngStr = dataSnapshot.child(Const.LONGITUDE).getValue().toString();
                        if (!TextUtils.isEmpty(latStr) && !TextUtils.isEmpty(lngStr)) {
                            LatLng latLng = new LatLng(Double.parseDouble(latStr), Double.parseDouble(lngStr));
                            ((TrackerListener) listener).onSelfLocationRetrieved(latLng);
                        } else {
                            listener.onFailure(AppUtils.getString(R.string.error_something_wrong));
                        }
                    }
                }

                @Override public void onCancelled(DatabaseError databaseError) {
                    listener.hideLoader();
                }
            });
        }
    }
}
