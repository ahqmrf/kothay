package apps.ahqmrf.kothay.tracker.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.Const;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrackUserActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String TAG = "Track User Activity";
    private UserInfoResponse user;
    private DatabaseReference ref;
    private GoogleMap         mGoogleMap;
    private Marker            mCurrLocationMarker;
    private ValueEventListener eventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot != null && dataSnapshot.hasChild(Const.LATITUDE) && dataSnapshot.hasChild(Const.LONGITUDE)) {
                double lat = (double) dataSnapshot.child(Const.LATITUDE).getValue();
                double lng = (double) dataSnapshot.child(Const.LONGITUDE).getValue();
                updateMarker(new LatLng(lat, lng));
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_user);
        ButterKnife.bind(this);
    }

    @Override public void onViewCreated() {
        showBackArrow();
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        if (user != null) {
            setLabel(user.getName());
            ref = FirebaseDatabase.getInstance().getReference().child(Const.LOCATION).child(user.getUsername());
        }
        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ref.addValueEventListener(eventListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ref.removeEventListener(eventListener);
    }

    @Override public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setTrafficEnabled(true);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void updateMarker(final LatLng latLng) {
        if (mCurrLocationMarker != null) {
            // Change marker position
            mCurrLocationMarker.setPosition(latLng);
            return;
        }

        //No marker added yet, add a marker and set current location to the marker

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(user.getName());
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
        mCurrLocationMarker.showInfoWindow();

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
    }

    @OnClick(R.id.route)
    void onRouteClick() {
        Intent intent = new Intent(this, RouteActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }
}
