package apps.ahqmrf.kothay.tracker.callback;

import java.util.List;

import apps.ahqmrf.kothay.tracker.module.Route;

/**
 * Created by bsse0 on 12/15/2017.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();

    void onDirectionFinderSuccess(List<Route> route);
}
