package apps.ahqmrf.kothay.tracker.callback;

import com.google.android.gms.maps.model.LatLng;

import apps.ahqmrf.kothay.ResponseListener;

/**
 * Created by bsse0 on 11/9/2017.
 */

public interface TrackerListener extends ResponseListener{
    void onSelfLocationRetrieved(LatLng latLng);

    void onUserLocationRetrieved(LatLng latLng);
}
