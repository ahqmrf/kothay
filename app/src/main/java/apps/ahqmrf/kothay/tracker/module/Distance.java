package apps.ahqmrf.kothay.tracker.module;

/**
 * Created by bsse0 on 12/15/2017.
 */
public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
