package apps.ahqmrf.kothay;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maruf on 8/19/2017.
 */

public class BaseResponse {
    @SerializedName("statusCode") private long   statusCode;
    @SerializedName("error") private      String error;
    @SerializedName("message") private    String message;

    public long getStatusCode() {
        return statusCode;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
