package apps.ahqmrf.kothay;

/**
 * Created by bsse0 on 9/30/2017.
 */

public interface OnItemClickCallback {
    void onItemClick(Object item);
}
