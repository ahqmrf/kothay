package apps.ahqmrf.kothay.message.callback;

/**
 * Created by bsse0 on 11/29/2017.
 */

public interface OnSmileyClickCallback {
    void onSmileyClick(int smileyId);
}
