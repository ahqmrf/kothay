package apps.ahqmrf.kothay.message.connector;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Date;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.message.callback.MessageExchangeListener;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.message.model.ChatMessageBuilder;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;

/**
 * Created by bsse0 on 11/23/2017.
 */

public class MessageServiceConnector {
    private MessageExchangeListener listener;
    private LoginResponse           self;
    private UserInfoResponse        other;
    private DatabaseReference       chatRef;
    private ValueEventListener      newMessageListener;
    private ValueEventListener      typingListener;
    private DatabaseReference       messageRef;
    private DatabaseReference       typingRef;
    private DatabaseReference       conversationRef;
    private DatabaseReference       onlineRef;
    private ValueEventListener      onlineListener;

    public MessageServiceConnector(MessageExchangeListener cbk, LoginResponse slf, UserInfoResponse other) {
        this.listener = cbk;
        this.self = slf;
        this.other = other;
        if (other != null) {
            conversationRef = FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION);
            DatabaseReference ref = FirebaseDatabase.getInstance()
                    .getReference()
                    .child(Const.CHAT);
            onlineRef = FirebaseDatabase.getInstance().getReference().child("OnlineStatus").child(other.getUsername());
            chatRef = ref.child(AppUtils.getMessagingRoute(self.getUsername(), other.getUsername())).child(Const.LAST_MESSAGE);
            messageRef = FirebaseDatabase.getInstance().getReference().child(Const.MESSAGES).child(AppUtils.getMessagingRoute(self.getUsername(), other.getUsername()));
            typingRef = ref.child(AppUtils.getMessagingRoute(self.getUsername(), other.getUsername())).child(Const.TYPING_STATUS).child(other.getUsername());

            newMessageListener = new ValueEventListener() {
                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                        ChatMessage message = dataSnapshot.getValue(ChatMessage.class);
                        if (listener != null) {
                            if (message.getTo().equals(self.getUsername())) {
                                if (!message.isSeen()) {
                                    message.setSeen(true);
                                    dataSnapshot.getRef().setValue(message);
                                    listener.onNewMessage(message);
                                    FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION)
                                            .child(self.getUsername()).child(message.getFrom()).child("seen").setValue(true);
                                    FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION)
                                            .child(message.getFrom()).child(self.getUsername()).child("seen").setValue(true);
                                }
                            } else {
                                listener.onNewMessage(message);
                            }
                        }
                    }
                }

                @Override public void onCancelled(DatabaseError databaseError) {

                }
            };

            typingListener = new ValueEventListener() {
                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                        boolean isTyping = dataSnapshot.getValue(Boolean.class);
                        if (listener != null) listener.onTypingStatusChanged(isTyping);
                    } else {
                        if (listener != null) listener.onTypingStatusChanged(false);
                    }
                }

                @Override public void onCancelled(DatabaseError databaseError) {
                    if (listener != null) listener.onTypingStatusChanged(false);
                }
            };

            onlineListener = new ValueEventListener() {
                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                        boolean online = dataSnapshot.getValue(Boolean.class);
                        if (listener != null) listener.onOnlineStatusChanged(online);
                    } else {
                        if (listener != null) listener.onOnlineStatusChanged(false);
                    }
                }

                @Override public void onCancelled(DatabaseError databaseError) {
                    if (listener != null) listener.onOnlineStatusChanged(false);
                }
            };
        }

    }

    public void sendMessage(String type, String content) {
        ChatMessageBuilder builder = new ChatMessageBuilder();
        builder.setFrom(self.getUsername())
                .setTo(other.getUsername())
                .setFromName(self.getName())
                .setId((int) self.getId())
                .setToId((int) other.getId())
                .setFromDisplayImageUrl(self.getImageUrl())
                .setToDisplayImageUrl(other.getImageUrl())
                .setType(type)
                .setTime(new Date().getTime());
        if (type.equals(Const.TYPE_TEXT) || type.equals(Const.SMILEY)) {
            builder.setContent(content);
        } else {
            builder.setImageUrl(content);
        }
        ChatMessage msg = builder.build();
        chatRef.setValue(msg);
        messageRef.push().setValue(msg);
        msg.setFromName(other.getName());
        msg.setFromDisplayImageUrl(other.getImageUrl());
        msg.setId((int) other.getId());
        conversationRef.child(self.getUsername()).child(other.getUsername()).setValue(msg);
        msg.setFromName(self.getName());
        msg.setId((int) self.getId());
        msg.setFromDisplayImageUrl(self.getImageUrl());
        conversationRef.child(other.getUsername()).child(self.getUsername()).setValue(msg);
    }

    public void attachListener() {
        chatRef.addValueEventListener(newMessageListener);
        typingRef.addValueEventListener(typingListener);
        onlineRef.addValueEventListener(onlineListener);
    }

    public void detachListener() {
        chatRef.removeEventListener(newMessageListener);
        typingRef.removeEventListener(typingListener);
        onlineRef.removeEventListener(onlineListener);
    }

    public void updateTypingStatus(boolean isTyping) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(Const.CHAT)
                .child(AppUtils.getMessagingRoute(self.getUsername(), other.getUsername()))
                .child(Const.TYPING_STATUS)
                .child(self.getUsername())
                .setValue(isTyping);

    }

    public void fetchPreviousMessages() {
        if (listener != null) {
            Query previousMessageRef = messageRef.limitToLast(1000);
            if (previousMessageRef != null) {
                previousMessageRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override public void onDataChange(DataSnapshot dataSnapshot) {
                        respondToActivity(dataSnapshot, 1);
                    }

                    @Override public void onCancelled(DatabaseError databaseError) {
                        respondToActivity(null, 1);
                    }
                });
            } else {
                respondToActivity(null, 1);
            }
        }
    }

    private void respondToActivity(DataSnapshot dataSnapshot, int type) {
        ArrayList<ChatMessage> messages = new ArrayList<>();
        if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            for (DataSnapshot item : children) {
                ChatMessage msg = item.getValue(ChatMessage.class);
                if (type == 1 && msg.getTo().equals(self.getUsername())) {
                    msg.setSeen(true);
                    item.getRef().setValue(msg);
                }
                messages.add(msg);
            }
        }
        if (listener != null) {
            listener.onPreviousMessagesFetched(messages);
        }
    }

    public void getConversationList() {
        if (listener != null) {
            FirebaseDatabase.getInstance().getReference().child(Const.CONVERSATION).child(self.getUsername())
                    .addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override public void onDataChange(DataSnapshot dataSnapshot) {
                                    respondToActivity(dataSnapshot, 0);
                                }

                                @Override public void onCancelled(DatabaseError databaseError) {
                                    respondToActivity(null, 0);
                                }
                            }
                    );
        }
    }

    public void sendImage(Uri uri) {
        if (listener != null) {
            listener.showLoader();
            String root = AppUtils.getMessagingRoute(self.getUsername(), other.getUsername());
            final StorageReference photoStorage = FirebaseStorage.getInstance().getReference().child(root).child(uri.getLastPathSegment());
            photoStorage.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    photoStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            listener.hideLoader();
                            AppUtils.toast("Uploaded successfully");
                            listener.onImageUrlFetched(uri.toString());
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            listener.hideLoader();
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    listener.hideLoader();
                    AppUtils.toast("Failed to upload photo");
                }
            });
        }
    }
}
