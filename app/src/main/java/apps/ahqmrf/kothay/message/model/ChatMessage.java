package apps.ahqmrf.kothay.message.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by bsse0 on 11/23/2017.
 */

public class ChatMessage implements Parcelable, Comparable<ChatMessage> {
    @SerializedName("id") private                  int     id;
    @SerializedName("toId") private                int     toId;
    @SerializedName("from") private                String  from;
    @SerializedName("fromName") private            String  fromName;
    @SerializedName("to") private                  String  to;
    @SerializedName("type") private                String  type;
    @SerializedName("time") private                long    time;
    @SerializedName("content") private             String  content;
    @SerializedName("imageUrl") private            String  imageUrl;
    @SerializedName("seen") private                boolean seen;
    @SerializedName("fromDisplayImageUrl") private String  fromDisplayImageUrl;
    @SerializedName("toDisplayImageUrl") private   String  toDisplayImageUrl;
    @SerializedName("delivered") private           boolean delivered;

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public ChatMessage() {
    }

    public ChatMessage(int id, String from, String to, String type, long time, String content, String imageUrl, boolean seen, String fromDisplayImageUrl, String fromName, String toDisplayImageUrl, int toId, boolean delivered) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.type = type;
        this.time = time;
        this.content = content;
        this.imageUrl = imageUrl;
        this.seen = seen;
        this.fromDisplayImageUrl = fromDisplayImageUrl;
        this.fromName = fromName;
        this.toDisplayImageUrl = toDisplayImageUrl;
        this.toId = toId;
        this.delivered = delivered;
    }

    protected ChatMessage(Parcel in) {
        id = in.readInt();
        from = in.readString();
        to = in.readString();
        type = in.readString();
        time = in.readLong();
        content = in.readString();
        imageUrl = in.readString();
        seen = in.readByte() != 0;
        fromDisplayImageUrl = in.readString();
        fromName = in.readString();
        toDisplayImageUrl = in.readString();
        toId = in.readInt();
        delivered = in.readByte() != 0;
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public int getId() {
        return id;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getType() {
        return type;
    }

    public long getTime() {
        return time;
    }

    public String getContent() {
        return content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isSeen() {
        return seen;
    }

    public String getFromDisplayImageUrl() {
        return fromDisplayImageUrl;
    }

    public void setFromDisplayImageUrl(String fromDisplayImageUrl) {
        this.fromDisplayImageUrl = fromDisplayImageUrl;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToDisplayImageUrl() {
        return toDisplayImageUrl;
    }

    public void setToDisplayImageUrl(String toDisplayImageUrl) {
        this.toDisplayImageUrl = toDisplayImageUrl;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(from);
        parcel.writeString(to);
        parcel.writeString(type);
        parcel.writeLong(time);
        parcel.writeString(content);
        parcel.writeString(imageUrl);
        parcel.writeByte((byte) (seen ? 1 : 0));
        parcel.writeString(fromDisplayImageUrl);
        parcel.writeString(fromName);
        parcel.writeString(toDisplayImageUrl);
        parcel.writeInt(toId);
        parcel.writeByte((byte) (delivered ? 1 : 0));
    }

    @Override public int compareTo(@NonNull ChatMessage message) {
        Date date = new Date(getTime());
        Date other = new Date(message.getTime());
        return date.compareTo(other);
    }
}
