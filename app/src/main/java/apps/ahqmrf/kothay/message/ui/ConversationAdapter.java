package apps.ahqmrf.kothay.message.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binjar.prefsdroid.Preference;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Date;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.auth.response.LoginResponse;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 11/25/2017.
 */

public class ConversationAdapter extends RecyclerView.Adapter {
    private ArrayList<ChatMessage> items;
    private OnItemClickCallback    callback;
    private String self = Preference.getObject(Const.USER_INFO, LoginResponse.class).getUsername();

    public ConversationAdapter(ArrayList<ChatMessage> items, OnItemClickCallback callback) {
        this.items = items;
        this.callback = callback;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversation, parent, false);
        return new ConversationViewHolder(view);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ConversationViewHolder) holder).bind(items.get(position));
    }

    @Override public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class ConversationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)      ImageView imageView;
        @BindView(R.id.time)       TextView  timeView;
        @BindView(R.id.text)       TextView  textView;
        @BindView(R.id.name)       TextView  nameView;
        @BindView(R.id.smiley)     ImageView smileyView;
        @BindView(R.id.itemLayout) View      layout;

        public ConversationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(ChatMessage item) {
            ImageLoader.getInstance().displayImage(
                    item.getFromDisplayImageUrl(),
                    imageView,
                    AppUtils.getImageConfig(R.drawable.man_placeholder)
            );
            timeView.setText(
                    new AppUtils.TimeModel(new Date(item.getTime())).getTime(false));
            if (item.getType().equals(Const.TYPE_TEXT)) {
                textView.setText(item.getContent());
                textView.setVisibility(View.VISIBLE);
                smileyView.setVisibility(View.GONE);
            } else if (item.getType().equals(Const.SMILEY)) {
                smileyView.setImageResource(Integer.parseInt(item.getContent()));
                smileyView.setVisibility(View.VISIBLE);
                textView.setVisibility(View.GONE);
            } else if (item.getType().equals(Const.PHOTO)) {
                textView.setText("Photo");
                textView.setVisibility(View.VISIBLE);
                smileyView.setVisibility(View.GONE);
            }
            nameView.setText(item.getFromName());
            if (!item.isSeen() && self.equals(item.getTo())) {
                layout.setBackgroundColor(AppUtils.getColor(R.color.darkBg));
            } else {
                layout.setBackgroundColor(AppUtils.getColor(R.color.bg));
            }
        }

        @OnClick(R.id.layout)
        void onItemClick() {
            if (callback != null) callback.onItemClick(items.get(getAdapterPosition()));
        }
    }
}
