package apps.ahqmrf.kothay.message.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.message.callback.OnSmileyClickCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bsse0 on 11/29/2017.
 */

public class SmileyAdapter extends RecyclerView.Adapter {
    private int[] items = {
            R.drawable.emo_cool,
            R.drawable.emo_angry,
            R.drawable.emo_bored,
            R.drawable.emo_wink,
            R.drawable.emo_confused,
            R.drawable.emo_cry,
            R.drawable.emo_happy,
            R.drawable.emo_happy_b,
            R.drawable.emo_happy_c,
            R.drawable.emo_in_love,
            R.drawable.emo_laugh,
            R.drawable.emo_love,
            R.drawable.emo_ninja,
            R.drawable.emo_sad,
            R.drawable.emo_sad_b,
            R.drawable.emo_smart,
            R.drawable.emo_smiling,
            R.drawable.emo_thinking,
            R.drawable.emo_wow,
            R.drawable.emo_mad,
            R.drawable.emo_suspicious,
            R.drawable.emo_bored_normal,
            R.drawable.emo_nerd,
            R.drawable.emo_embarrassed
    };

    private OnSmileyClickCallback callback;

    public SmileyAdapter(OnSmileyClickCallback callback) {
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_smiley, parent, false);
        return new SmileyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SmileyViewHolder) holder).imageView.setImageResource(items[position]);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    class SmileyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.smiley) ImageView imageView;

        public SmileyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.smiley)
        void onSmileyClick() {
            if (callback != null) {
                callback.onSmileyClick(items[getAdapterPosition()]);
            }
        }
    }
}
