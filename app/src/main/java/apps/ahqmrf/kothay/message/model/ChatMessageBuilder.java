package apps.ahqmrf.kothay.message.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import apps.ahqmrf.kothay.util.Const;

/**
 * Created by bsse0 on 11/23/2017.
 */

public class ChatMessageBuilder {

    @SerializedName("id") private                  int     id;
    @SerializedName("toId") private                int     toId;
    @SerializedName("from") private                String  from;
    @SerializedName("to") private                  String  to;
    @SerializedName("type") private                String  type;
    @SerializedName("time") private                long    time;
    @SerializedName("content") private             String  content;
    @SerializedName("imageUrl") private            String  imageUrl;
    @SerializedName("seen") private                boolean seen;
    @SerializedName("fromDisplayImageUrl") private String  fromDisplayImageUrl;
    @SerializedName("toDisplayImageUrl") private   String  toDisplayImageUrl;
    @SerializedName("fromName") private            String  fromName;
    @SerializedName("delivered") private           boolean delivered;

    public ChatMessageBuilder() {
        type = Const.TYPE_TEXT;
        content = Const.UNDEFINED;
        imageUrl = Const.UNDEFINED;
        seen = false;
        delivered = false;
    }

    public ChatMessageBuilder setDelivered(boolean delivered) {
        this.delivered = delivered;
        return this;
    }

    public ChatMessageBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public ChatMessageBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public ChatMessageBuilder setToId(int toId) {
        this.toId = toId;
        return this;
    }

    public ChatMessageBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public ChatMessageBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public ChatMessageBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    public ChatMessageBuilder setTime(long time) {
        this.time = time;
        return this;
    }

    public ChatMessageBuilder setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public ChatMessageBuilder setSeen(boolean seen) {
        this.seen = seen;
        return this;
    }

    public ChatMessageBuilder setFromName(String fromName) {
        this.fromName = fromName;
        return this;
    }

    public ChatMessageBuilder setFromDisplayImageUrl(String fromDisplayImageUrl) {
        if (TextUtils.isEmpty(fromDisplayImageUrl)) {
            fromDisplayImageUrl = Const.UNDEFINED;
        }
        this.fromDisplayImageUrl = fromDisplayImageUrl;
        return this;
    }

    public ChatMessageBuilder setToDisplayImageUrl(String toDisplayImageUrl) {
        if (TextUtils.isEmpty(toDisplayImageUrl)) {
            toDisplayImageUrl = Const.UNDEFINED;
        }
        this.toDisplayImageUrl = toDisplayImageUrl;
        return this;
    }

    public ChatMessage build() {
        if (time == 0) time = new Date().getTime();
        return new ChatMessage(id, from, to, type, time, content, imageUrl, seen, fromDisplayImageUrl, fromName, toDisplayImageUrl, toId, delivered);
    }
}
