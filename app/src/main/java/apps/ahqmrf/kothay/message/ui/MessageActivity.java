package apps.ahqmrf.kothay.message.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.message.callback.MessageExchangeListener;
import apps.ahqmrf.kothay.message.connector.MessageServiceConnector;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.user.callbacks.UserListener;
import apps.ahqmrf.kothay.user.connector.UserConnector;
import apps.ahqmrf.kothay.user.request.UserInfoRequest;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageActivity extends BaseActivity implements MessageExchangeListener, OnItemClickCallback, UserListener {

    @BindView(R.id.conversationList)   RecyclerView       conversationListView;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private ConversationAdapter     adapter;
    private ArrayList<ChatMessage>  items;
    private MessageServiceConnector messageServiceConnector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        setLabel(R.string.title_messages);
        showBackArrow();
        messageServiceConnector = new MessageServiceConnector(this, self, null);
        if (messagesView != null) {
            messagesView.setColorFilter(AppUtils.getColor(R.color.bottomIconFocused));
            messagesView.setBackgroundColor(AppUtils.getColor(R.color.colorPrimaryDark));
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                messageServiceConnector.getConversationList();
            }
        });

        conversationListView.setLayoutManager(new LinearLayoutManager(this));
        items = new ArrayList<>();
        adapter = new ConversationAdapter(items, this);
        conversationListView.setAdapter(adapter);
        showLoader();
        messageServiceConnector.getConversationList();
    }

    @Override public void onNewMessage(ChatMessage message) {

    }

    @Override protected void onResume() {
        super.onResume();
        messageServiceConnector.getConversationList();
    }

    @Override public void onPreviousMessagesFetched(ArrayList<ChatMessage> previousMessages) {
        hideLoader();
        items.clear();
        swipeRefreshLayout.setRefreshing(false);
        Collections.sort(previousMessages);
        Collections.reverse(previousMessages);
        items.addAll(previousMessages);
        adapter.notifyDataSetChanged();
        ChatMessage message = getIntent().getParcelableExtra(Const.MESSAGE);
        if(message != null) {
            onItemClick(message);
            getIntent().removeExtra(Const.MESSAGE);
        }
    }

    @Override public void onTypingStatusChanged(boolean isTyping) {

    }

    @Override public void onImageUrlFetched(String imageUrl) {

    }

    @Override public void onOnlineStatusChanged(boolean online) {

    }

    @Override public void onItemClick(Object item) {
        ChatMessage message = (ChatMessage) item;
        UserInfoRequest request = new UserInfoRequest();
        request.setUserId(self.getId());
        request.setOtherUserId(message.getId());
        new UserConnector(this).fetchUser(request);
    }

    @Override public void onUserFetched(UserInfoResponse user) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Const.USER_INFO, user);
        openPage(intent);
    }
}
