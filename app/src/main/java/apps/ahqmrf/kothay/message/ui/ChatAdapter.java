package apps.ahqmrf.kothay.message.ui;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.message.model.ItemChat;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by bsse0 on 11/24/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter {

    private ArrayList<ItemChat> items;

    private final int SELF         = 0;
    private final int OTHER        = 1;
    private final int SELF_SMILEY  = 2;
    private final int OTHER_SMILEY = 3;
    private final int SELF_PHOTO   = 4;
    private final int OTHER_PHOTO  = 5;
    private OnItemClickCallback callback;
    private int                 placeholder;

    public ChatAdapter(ArrayList<ItemChat> items, OnItemClickCallback callback, int placeholder) {
        this.items = items;
        this.callback = callback;
        this.placeholder = placeholder;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = R.layout.item_message_self;
        if (viewType == SELF_SMILEY) layout = R.layout.item_emo_self;
        else if (viewType == OTHER) layout = R.layout.item_message_other;
        else if (viewType == OTHER_SMILEY) layout = R.layout.item_emo_other;
        else if (viewType == SELF_PHOTO) layout = R.layout.item_photo_self;
        else if (viewType == OTHER_PHOTO) layout = R.layout.item_photo_other;

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        if (viewType == SELF_SMILEY || viewType == OTHER_SMILEY) return new SmileyViewHolder(view);
        return new ChatTextViewHolder(view);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemChat item = items.get(position);
        if (item.getType().equals(Const.SMILEY)) {
            ((SmileyViewHolder) holder).bind(item);
        } else {
            ((ChatTextViewHolder) holder).bind(item);
        }
    }

    @Override public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override public int getItemViewType(int position) {
        ItemChat item = items.get(position);
        if (item.isIncoming()) {
            if (item.getType().equals(Const.SMILEY)) return OTHER_SMILEY;
            if (item.getType().equals(Const.PHOTO)) return OTHER_PHOTO;
            return OTHER;
        }
        if (item.getType().equals(Const.SMILEY)) return SELF_SMILEY;
        if (item.getType().equals(Const.PHOTO)) return SELF_PHOTO;
        return SELF;
    }

    class ChatTextViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.image) ImageView imageView;

        @Nullable
        @BindView(R.id.photo) ImageView photoView;

        @Nullable
        @BindView(R.id.text) TextView textView;

        public ChatTextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(ItemChat item) {
            if (item.isIncoming() && imageView != null) {
                if (item.isLast()) {
                    ImageLoader.getInstance().displayImage(
                            item.getFromDisplayImageUrl(),
                            imageView,
                            AppUtils.getImageConfig(placeholder));
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
            if (textView != null) {
                textView.setText(item.getContent());
                textView.setBackgroundResource(item.getBackground());
            }

            if (photoView != null && item.getType().equals(Const.PHOTO)) {
                ImageLoader.getInstance().displayImage(
                        item.getImageUrl(),
                        photoView,
                        AppUtils.getImageConfig(R.drawable.loading));
            }
        }

        @Optional
        @OnClick(R.id.photo)
        void onImageClick() {
            if (callback != null)
                callback.onItemClick(items.get(getAdapterPosition()).getImageUrl());
        }
    }

    class SmileyViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.image)  ImageView imageView;
        @BindView(R.id.smiley) ImageView smileyView;

        public SmileyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ItemChat item) {
            if (item.isIncoming() && imageView != null) {
                if (item.isLast()) {
                    ImageLoader.getInstance().displayImage(
                            item.getFromDisplayImageUrl(),
                            imageView,
                            AppUtils.getImageConfig(placeholder));
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
            smileyView.setImageResource(Integer.parseInt(item.getContent()));
        }
    }
}
