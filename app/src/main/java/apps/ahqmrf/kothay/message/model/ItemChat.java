package apps.ahqmrf.kothay.message.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bsse0 on 11/24/2017.
 */

public class ItemChat extends ChatMessage {
    @SerializedName("last") private            boolean last;
    @SerializedName("clicked") private         boolean clicked;
    @SerializedName("incoming") private        boolean incoming;
    @SerializedName("background") private      int     background;

    public ItemChat(ChatMessage message) {
        setId(message.getId());
        setContent(message.getContent());
        setFrom(message.getFrom());
        setImageUrl(message.getImageUrl());
        setSeen(message.isSeen());
        setTime(message.getTime());
        setTo(message.getTo());
        setType(message.getType());
        last = false;
        clicked = false;
    }

    protected ItemChat(Parcel in) {
        last = in.readByte() != 0;
        clicked = in.readByte() != 0;
        incoming = in.readByte() != 0;
        background = in.readInt();
    }

    public static final Creator<ItemChat> CREATOR = new Creator<ItemChat>() {
        @Override
        public ItemChat createFromParcel(Parcel in) {
            return new ItemChat(in);
        }

        @Override
        public ItemChat[] newArray(int size) {
            return new ItemChat[size];
        }
    };

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (last ? 1 : 0));
        parcel.writeByte((byte) (incoming ? 1 : 0));
        parcel.writeByte((byte) (clicked ? 1 : 0));
        parcel.writeInt(background);
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public boolean isIncoming() {
        return incoming;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
