package apps.ahqmrf.kothay.message.callback;

import java.util.ArrayList;

import apps.ahqmrf.kothay.ResponseListener;
import apps.ahqmrf.kothay.message.model.ChatMessage;

/**
 * Created by bsse0 on 11/23/2017.
 */

public interface MessageExchangeListener extends ResponseListener {
    void onNewMessage(ChatMessage message);

    void onPreviousMessagesFetched(ArrayList<ChatMessage> previousMessages);

    void onTypingStatusChanged(boolean isTyping);

    void onImageUrlFetched(String imageUrl);

    void onOnlineStatusChanged(boolean online);
}
