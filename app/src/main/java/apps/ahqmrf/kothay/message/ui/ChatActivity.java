package apps.ahqmrf.kothay.message.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.TreeSet;

import apps.ahqmrf.kothay.BaseActivity;
import apps.ahqmrf.kothay.OnItemClickCallback;
import apps.ahqmrf.kothay.R;
import apps.ahqmrf.kothay.message.callback.MessageExchangeListener;
import apps.ahqmrf.kothay.message.callback.OnSmileyClickCallback;
import apps.ahqmrf.kothay.message.connector.MessageServiceConnector;
import apps.ahqmrf.kothay.message.model.ChatMessage;
import apps.ahqmrf.kothay.message.model.ItemChat;
import apps.ahqmrf.kothay.tracker.ui.RouteActivity;
import apps.ahqmrf.kothay.tracker.ui.TrackUserActivity;
import apps.ahqmrf.kothay.user.response.UserInfoResponse;
import apps.ahqmrf.kothay.util.AppUtils;
import apps.ahqmrf.kothay.util.Const;
import apps.ahqmrf.kothay.util.FullScreenImageFragment;
import apps.ahqmrf.kothay.util.GridSpacingItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class ChatActivity extends BaseActivity implements MessageExchangeListener, OnSmileyClickCallback, OnItemClickCallback {

    @BindView(R.id.emos)         RecyclerView emosView;
    @BindView(R.id.messages)     RecyclerView messagesView;
    @BindView(R.id.inputMessage) EditText     inputView;
    @BindView(R.id.ic_call)      ImageView    callView;
    @BindView(R.id.ic_track)     ImageView    trackView;
    @BindView(R.id.ic_route)     ImageView    routeView;
    @BindView(R.id.typing)       TextView     typingView;
    @BindView(R.id.active)       TextView     onlineView;
    @BindView(R.id.input_layout) View         inputLayout;

    private MessageServiceConnector connector;
    private ChatAdapter             adapter;
    private ArrayList<ItemChat>     messages;
    private UserInfoResponse        user;
    private LinearLayoutManager     manager;
    private Parcelable              chatState;
    private int           lastIdOfOther      = -1;
    private int           lastIdOfSelf       = -1;
    private int           otherCount         = 0;
    private int           selfCount          = 0;
    private TreeSet<Long> messagesSet        = new TreeSet<>();
    private boolean       isListenerAttached = false;
    private boolean       isDataLoaded       = false;
    private boolean       isFirstMessage     = true;

    long    delay        = 0;
    long    lastTextEdit = 0;
    Handler handler      = new Handler();

    private Runnable inputFinishChecker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (lastTextEdit + delay)) {
                if (connector != null) connector.updateTypingStatus(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
    }

    @Override
    public void onViewCreated() {
        showBackArrow();
        if (blankSpaceView != null) blankSpaceView.setVisibility(View.VISIBLE);
        ViewTreeObserver vto = emosView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                emosView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width = emosView.getMeasuredWidth();
                int colWidth = AppUtils.dpToPx(48);
                int columns = width / colWidth;
                setEmosView(columns);
            }
        });
        user = getIntent().getParcelableExtra(Const.USER_INFO);
        if (user != null) {
            setLabel(user.getName());
            if (user.isFriend()) {
                if (!user.sharesLocation()) {
                    trackView.setColorFilter(AppUtils.getColor(R.color.bottomIconUnfocused));
                    routeView.setColorFilter(AppUtils.getColor(R.color.bottomIconUnfocused));
                } else {
                    trackView.setColorFilter(AppUtils.getColor(R.color.colorPrimary));
                    routeView.setColorFilter(AppUtils.getColor(R.color.colorPrimary));
                }
                trackView.setVisibility(View.VISIBLE);
                callView.setVisibility(View.VISIBLE);
                routeView.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(user.getPhone())) {
                    callView.setColorFilter(AppUtils.getColor(R.color.colorPrimary));
                }
            }
            connector = new MessageServiceConnector(this, self, user);
            messages = new ArrayList<>();
            adapter = new ChatAdapter(messages, this, (user.getGender().equals(Const.FEMALE) ? R.drawable.woman_placeholder : R.drawable.man_placeholder));
            manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            manager.setStackFromEnd(true);
            messagesView.setLayoutManager(manager);
            messagesView.setAdapter(adapter);
            showLoader();
            connector.fetchPreviousMessages();

            inputView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    handler.removeCallbacks(inputFinishChecker);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        lastTextEdit = System.currentTimeMillis();
                        handler.postDelayed(inputFinishChecker, delay);
                    } else {
                        if (connector != null) connector.updateTypingStatus(false);
                    }
                }
            });
        }
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onViewCreated();
    }

    private void setEmosView(int columns) {
        SmileyAdapter smileyAdapter = new SmileyAdapter(this);
        int rows = smileyAdapter.getItemCount() / columns + (smileyAdapter.getItemCount() % columns > 0 ? 1 : 0);
        if (rows > Const.EMO_LIST_MAX_ROW) {
            ViewGroup.LayoutParams params = emosView.getLayoutParams();
            params.height = AppUtils.dpToPx(Const.EMO_LIST_MAX_ROW * 48 + 36);
            params.height = Math.max(AppUtils.dpToPx(48), params.height);
            emosView.setLayoutParams(params);
        }
        emosView.setLayoutManager(new GridLayoutManager(this, columns));
        emosView.addItemDecoration(new GridSpacingItemDecoration(columns, 3, false));
        emosView.setAdapter(smileyAdapter);
        emosView.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isListenerAttached && isDataLoaded) {
            connector.attachListener();
            isListenerAttached = true;
        }
        if (chatState != null) {
            manager.onRestoreInstanceState(chatState);
        }
        AppUtils.setChatOpen(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        connector.detachListener();
        connector.updateTypingStatus(false);
        isListenerAttached = false;
        isFirstMessage = true;
        AppUtils.setChatOpen(false);
    }

    @OnClick(R.id.ic_send)
    void onSendClick() {
        String text = inputView.getText().toString().trim();
        inputView.setText(null);
        if (TextUtils.isEmpty(text) || text.matches("[\\n\\r]+")) return;
        connector.sendMessage(Const.TYPE_TEXT, text);
    }

    @Override
    public void onNewMessage(ChatMessage message) {
        if (isFirstMessage) {
            isFirstMessage = false;
            if (messages.isEmpty()) {
                addMessage(message);
                messagesSet.add(message.getTime());
            } else return;
        }
        if (messagesSet.contains(message.getTime())) {
            return;
        }
        addMessage(message);
        messagesSet.add(message.getTime());
    }

    private void addMessage(ChatMessage message) {
        ItemChat item = new ItemChat(message);
        int id = messages.size();
        if (message.getFrom().equals(user.getUsername())) {
            ++otherCount;
            item.setIncoming(true);
            item.setFromDisplayImageUrl(user.getImageUrl());
            if (lastIdOfOther != -1) {
                ItemChat last = messages.get(lastIdOfOther);
                last.setLast(false);
                item.setBackground(R.drawable.other_end_bottom);
                if (otherCount == 2) {
                    last.setBackground(R.drawable.other_end_top);
                } else if (otherCount > 2) {
                    last.setBackground(R.drawable.other_end_mid);
                }
                messages.set(lastIdOfOther, last);
                adapter.notifyItemChanged(lastIdOfOther);
            } else {
                item.setBackground(R.drawable.message_other_end_bg);
            }
            lastIdOfOther = id;
            lastIdOfSelf = -1;
            selfCount = 0;

        } else {
            ++selfCount;
            if (lastIdOfSelf != -1) {
                ItemChat last = messages.get(lastIdOfSelf);
                last.setLast(false);
                item.setBackground(R.drawable.self_end_bottom);
                if (selfCount == 2) {
                    last.setBackground(R.drawable.self_end_top);
                } else if (selfCount > 2) {
                    last.setBackground(R.drawable.self_end_mid);
                }
                messages.set(lastIdOfSelf, last);
                adapter.notifyItemChanged(lastIdOfSelf);
            } else {
                item.setBackground(R.drawable.message_self_end_bg);
            }
            lastIdOfSelf = id;
            lastIdOfOther = -1;
            otherCount = 0;
        }
        item.setLast(true);
        messages.add(item);
        adapter.notifyItemInserted(messages.size() - 1);
        messagesView.smoothScrollToPosition(messages.size() - 1);
    }

    @OnClick(R.id.ic_emo)
    void onEmoClick() {
        emosView.setVisibility(View.VISIBLE);
        hideSoftKeyboard();
    }

    @OnClick(R.id.inputMessage)
    void onMessageTypeInputClick() {
        emosView.setVisibility(View.GONE);
    }

    @OnClick(R.id.shareImage)
    void checkReadExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            Const.READ_EXTERNAL_STORAGE);
                }
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Const.READ_EXTERNAL_STORAGE);
            }
        } else {
            openImageGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openImageGallery();

                } else {
                    AppUtils.toast(R.string.error_permission);
                }
                break;
            }
        }
    }

    private void openImageGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (getPackageManager().resolveActivity(intent, 0) != null) {
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), Const.REQUEST_BROWSE_GALLERY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.REQUEST_BROWSE_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (null != uri) {
                connector.sendImage(uri);
            }
        }
    }

    @Override
    public void onPreviousMessagesFetched(ArrayList<ChatMessage> previousMessages) {
        for (ChatMessage message : previousMessages) {
            addMessage(message);
        }
        isDataLoaded = true;
        connector.attachListener();
        hideLoader();
    }

    @Override
    public void onTypingStatusChanged(boolean isTyping) {
        if (isTyping) {
            typingView.setVisibility(View.VISIBLE);
            onlineView.setVisibility(View.GONE);
        } else {
            typingView.setVisibility(View.GONE);
            if (!onlineView.getText().toString().equals("Offline")) {
                onlineView.setVisibility(View.VISIBLE);
            } else {
                onlineView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onImageUrlFetched(String imageUrl) {
        connector.sendMessage(Const.PHOTO, imageUrl);
    }

    @Override
    public void onOnlineStatusChanged(boolean online) {
        if (online) {
            onlineView.setText(R.string.label_active);
            onlineView.setVisibility(View.VISIBLE);
        } else {
            onlineView.setText(R.string.label_offline);
            onlineView.setVisibility(View.GONE);
        }
    }

    @Optional
    @OnClick(R.id.ic_call)
    void onCallClick() {
        if (!TextUtils.isEmpty(user.getPhone())) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + user.getPhone()));
            startActivity(intent);
        }
    }

    @Optional
    @OnClick(R.id.ic_track)
    void onTrackClick() {
        if (user.sharesLocation()) {
            Intent intent = new Intent(this, TrackUserActivity.class);
            intent.putExtra(Const.USER_INFO, user);
            openPage(intent);
        }
    }

    @Optional
    @OnClick(R.id.ic_route)
    void onRouteClick() {
        if (user.sharesLocation()) {
            Intent intent = new Intent(this, RouteActivity.class);
            intent.putExtra(Const.USER_INFO, user);
            openPage(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        chatState = manager.onSaveInstanceState();
        outState.putParcelable(Const.CHAT_STATE, chatState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            chatState = savedInstanceState.getParcelable(Const.CHAT_STATE);
        }
    }

    @Override
    public void onIncomingMessage(ChatMessage message) {
        if (message.getFrom().equals(user.getUsername())) {
            AppUtils.cacheMessage(message);
        } else super.onIncomingMessage(message);
    }

    @Override
    public void onSmileyClick(int smileyId) {
        connector.sendMessage(Const.SMILEY, smileyId + "");
    }

    @Override
    public void onBackPressed() {
        if (emosView.getVisibility() == View.VISIBLE) emosView.setVisibility(View.GONE);
        else super.onBackPressed();
    }

    @Override
    public void onItemClick(Object item) {
        if (item instanceof String) {
            String imageUrl = (String) item;
            if (!TextUtils.isEmpty(self.getImageUrl())) {
                FullScreenImageFragment frag = FullScreenImageFragment.getInstance(imageUrl);
                frag.show(getSupportFragmentManager(), "Full screen image");
            }
        }
    }
}
